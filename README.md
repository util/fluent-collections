# fluent-collections

This is a library of light-weight collection implementations which strive to provide a fluent and easy to read API.

This library was created during the development of Search Guard FLX, where the following requirements stood in the foreground:

- The API should be clean and concise. Readability is paramount. The API should make it easy to perform set theoretic operations like intersection, union, etc.

- Immutability is more important than mutability, as the collections are mostly used for configuration information or for information which is never modified after its creation.

- Instances of the collections should be usable in very high throughput scenarios while creating only a small overhead in terms of heap footprint or GC strain.

- The implementation target collections with a relatively small number of elements (< 1000). For collections with more elements, the implementations usually fall back to the Java core collection implementations.

## License

This code is licensed under the Apache 2.0 License.

## Copyright

Copyright 2022 by floragunn GmbH
