/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import org.junit.Assert;
import org.junit.Test;

public class CheckListTest {
    @Test
    public void checkIf() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b", "xxx", "c", "d"));
        subject.checkIf((e) -> e.length() > 1);
        Assert.assertEquals(ImmutableSet.of("xxx"), subject.getCheckedElements());
        subject.checkIf((e) -> e.length() <= 1);
        Assert.assertEquals(ImmutableSet.of("a", "b", "xxx", "c", "d"), subject.getCheckedElements());
    }

    @Test
    public void uncheckIf() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b", "xxx", "c", "d"));
        subject.checkAll();
        subject.uncheckIf((e) -> e.length() > 1);

        Assert.assertEquals(ImmutableSet.of("a", "b", "c", "d"), subject.getCheckedElements());
    }

    @Test
    public void checkIf_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "xxx"));
        subject.checkIf((e) -> e.length() > 1);
        Assert.assertEquals(ImmutableSet.of("xxx"), subject.getCheckedElements());
        subject.checkIf((e) -> e.length() <= 1);
        Assert.assertEquals(ImmutableSet.of("a", "xxx"), subject.getCheckedElements());
    }

    @Test
    public void uncheckIf_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "xxx"));
        subject.checkAll();
        subject.uncheckIf((e) -> e.length() > 1);

        Assert.assertEquals(ImmutableSet.of("a"), subject.getCheckedElements());
    }

    @Test
    public void check_unknown() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b", "c", "d"));

        try {
            subject.check("xxx");
            Assert.fail();
        } catch (IllegalArgumentException e) {

        }
    }

    @Test
    public void check_unknown_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b"));

        try {
            subject.check("xxx");
            Assert.fail();
        } catch (IllegalArgumentException e) {

        }
    }

    @Test
    public void uncheck_unknown_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b"));

        try {
            subject.uncheck("xxx");
            Assert.fail();
        } catch (IllegalArgumentException e) {

        }
    }

    @Test
    public void uncheckIfPresent() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b", "c", "d"));
        subject.check("a");
        subject.check("b");
        subject.uncheckIfPresent("a");
        Assert.assertEquals(ImmutableSet.of("b"), subject.getCheckedElements());
        Assert.assertEquals(ImmutableSet.of("a", "c", "d"), subject.getUncheckedElements());
    }

    @Test
    public void uncheckIfPresent_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b"));
        subject.check("a");
        subject.check("b");
        subject.uncheckIfPresent("a");
        Assert.assertEquals(ImmutableSet.of("b"), subject.getCheckedElements());
        Assert.assertEquals(ImmutableSet.of("a"), subject.getUncheckedElements());
    }

    @Test
    public void uncheckIfPresent_unknown() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b", "c", "d"));
        subject.check("a");
        subject.uncheckIfPresent("xxx");
        Assert.assertEquals(ImmutableSet.of("a"), subject.getCheckedElements());
        Assert.assertEquals(ImmutableSet.of("b", "c", "d"), subject.getUncheckedElements());
    }

    @Test
    public void uncheckIfPresent_unknown_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b"));
        subject.check("a");
        subject.uncheckIfPresent("xxx");
        Assert.assertEquals(ImmutableSet.of("a"), subject.getCheckedElements());
        Assert.assertEquals(ImmutableSet.of("b"), subject.getUncheckedElements());
    }

    @Test
    public void isBlank_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b"));
        Assert.assertTrue(subject.isBlank());
        subject.check("a");
        Assert.assertFalse(subject.isBlank());
    }

    @Test
    public void isComplete_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b"));
        Assert.assertFalse(subject.isComplete());
        Assert.assertFalse(subject.check("a"));
        Assert.assertTrue(subject.check("b"));
        Assert.assertTrue(subject.isComplete());
    }

    @Test
    public void isChecked_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b"));
        Assert.assertFalse(subject.isChecked("a"));
        Assert.assertFalse(subject.isChecked("b"));
        subject.check("a");
        Assert.assertTrue(subject.isChecked("a"));
        Assert.assertFalse(subject.isChecked("b"));
        subject.check("b");
        Assert.assertTrue(subject.isChecked("a"));
        Assert.assertTrue(subject.isChecked("b"));
    }

    @Test
    public void isChecked_unknown_2e() {
        CheckList<String> subject = CheckList.create(ImmutableSet.of("a", "b"));
        try {
            subject.uncheck("xxx");
            Assert.fail();
        } catch (IllegalArgumentException e) {

        }
    }

    @Test
    public void view() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3"));
        Assert.assertFalse(root.isChecked("a1"));
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        Assert.assertFalse(view.check("a1"));
        Assert.assertTrue(root.isChecked("a1"));
        Assert.assertTrue(view.isChecked("a1"));
        Assert.assertFalse(root.isChecked("a2"));
        Assert.assertFalse(view.check("a2"));
        Assert.assertTrue(root.isChecked("a2"));
        Assert.assertTrue(view.isChecked("a2"));
        Assert.assertTrue(view.check("a3"));
        Assert.assertTrue(view.isChecked("a3"));
        Assert.assertTrue(root.isChecked("a3"));
        view = root.getView((k) -> k.startsWith("b"));
        Assert.assertFalse(view.check("b1"));
        Assert.assertTrue(root.isChecked("b1"));
        Assert.assertTrue(view.isChecked("b1"));
        view = root.getView((k) -> true);
        Assert.assertTrue(view.isChecked("a1"));
        Assert.assertTrue(view.isChecked("b1"));
    }

    @Test
    public void view_2e() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "b1"));
        Assert.assertFalse(root.isChecked("a1"));
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        Assert.assertTrue(view.isBlank());
        Assert.assertTrue(view.check("a1"));
        Assert.assertTrue(root.isChecked("a1"));
        Assert.assertTrue(view.isChecked("a1"));
        Assert.assertFalse(root.isChecked("b1"));
        Assert.assertFalse(view.isBlank());
        Assert.assertTrue(view.isComplete());
        view = root.getView((k) -> k.startsWith("b"));
        Assert.assertTrue(view.check("b1"));
        Assert.assertTrue(root.isChecked("b1"));
        Assert.assertTrue(view.isChecked("b1"));
        view = root.getView((k) -> true);
        Assert.assertTrue(view.isChecked("a1"));
        Assert.assertTrue(view.isChecked("b1"));
    }
    
    @Test
    public void view_checkIf() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3"));
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        view.checkIf((k) -> k.equals("b1"));
        Assert.assertFalse(root.isChecked("b1"));
        view.checkIf((k) -> k.equals("a1"));
        Assert.assertTrue(view.isChecked("a1"));
        Assert.assertTrue(root.isChecked("a1"));        
    }
    
    @Test
    public void view_checkIf_2e() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "b1"));
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        view.checkIf((k) -> k.equals("b1"));
        Assert.assertFalse(root.isChecked("b1"));
        view.checkIf((k) -> k.equals("a1"));
        Assert.assertTrue(view.isChecked("a1"));
        Assert.assertTrue(root.isChecked("a1"));        
    }
    
    @Test
    public void view_uncheckIfPresent() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3"));
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        root.check("a1");
        root.check("b1");
        view.uncheckIfPresent("b1");
        Assert.assertTrue(root.isChecked("b1"));
        view.uncheckIfPresent("a1");
        Assert.assertFalse(root.isChecked("a1"));
    }
    
    @Test
    public void view_uncheckIfPresent_2e() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "b1"));
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        root.check("a1");
        root.check("b1");
        view.uncheckIfPresent("b1");
        Assert.assertTrue(root.isChecked("b1"));
        view.uncheckIfPresent("a1");
        Assert.assertFalse(root.isChecked("a1"));
    }
    
    @Test
    public void view_uncheckIf() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3"));
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        root.check("a1");
        root.check("b1");
        view.uncheckIf((k) -> k.startsWith("b"));
        Assert.assertTrue(root.isChecked("b1"));
        view.uncheckIf((k) -> k.startsWith("a"));
        Assert.assertFalse(root.isChecked("a1"));
    }
    
    @Test
    public void view_uncheckIf_2e() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "b1"));
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        root.check("a1");
        root.check("b1");
        view.uncheckIf((k) -> k.startsWith("b"));
        Assert.assertTrue(root.isChecked("b1"));
        view.uncheckIf((k) -> k.startsWith("a"));
        Assert.assertFalse(root.isChecked("a1"));
    }
    
    @Test
    public void view_checkAll() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3"));
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        view.checkAll();
        Assert.assertTrue(root.isChecked("a1"));
        Assert.assertFalse(root.isChecked("b1"));
        Assert.assertTrue(view.isComplete());
        Assert.assertFalse(root.isComplete());
    }
    
    @Test
    public void view_uncheckAll() {
        CheckList<String> root = CheckList.create(ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3"));
        root.checkAll();
        Assert.assertTrue(root.isComplete());
        CheckList<String> view = root.getView((k) -> k.startsWith("a"));
        Assert.assertTrue(view.isComplete());
        view.uncheckAll();
        Assert.assertFalse(root.isChecked("a1"));
        Assert.assertTrue(root.isChecked("b1"));
        Assert.assertFalse(view.isComplete());
        Assert.assertFalse(root.isComplete());
    }
}
