package com.floragunn.fluent.collections;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class ImmutableMapBuilderTest {

    @Test
    public void containsAnyKey() {
        Map<String, String> reference = new HashMap<>();
        reference.put("a", "A");
        reference.put("b", "B");
        reference.put("c", "C");

        ImmutableMap.Builder<String, String> subject = new ImmutableMap.Builder<>(reference);
        Assert.assertTrue(subject.containsAnyKey(Collections.singleton("a")));
        Assert.assertTrue(subject.containsAnyKey(reference.keySet()));
        Assert.assertFalse(subject.containsAnyKey(Collections.singleton("x")));
        Assert.assertTrue(subject.containsAnyKey(ImmutableSet.of("a", "x")));

        subject.put("y", "Y");
        Assert.assertTrue(subject.containsAnyKey(Collections.singleton("a")));
        Assert.assertTrue(subject.containsAnyKey(reference.keySet()));
        Assert.assertFalse(subject.containsAnyKey(Collections.singleton("x")));
        Assert.assertTrue(subject.containsAnyKey(ImmutableSet.of("a", "x")));
        Assert.assertTrue(subject.containsAnyKey(Collections.singleton("y")));
    }

}
