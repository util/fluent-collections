/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class ImmutableSetTest {

    @Test
    public void toArray_arrayBacked() {
        ImmutableSet<String> subject = new ImmutableSetImpl.ArrayBackedSet<String>(new String[] { "a", "b", "c" });

        assertEquals(ImmutableSet.of("a", "b", "c"), ImmutableSet.ofArray(subject.toArray(new String[0])));
        assertEquals(ImmutableSet.of("a", "b", "c"), ImmutableSet.ofArray(subject.toArray()));
    }

    @Test
    public void only_arrayBacked() {
        ImmutableSet<String> subject = new ImmutableSetImpl.ArrayBackedSet<String>(new String[] { "a", "b", "c" });

        try {
            subject.only();
            Assert.fail("only() should fail for sets with size != 1");
        } catch (IllegalStateException e) {
        }
    }

    @Test
    public void forAnyApplies_arrayBacked() {
        ImmutableSet<Integer> subject = new ImmutableSetImpl.ArrayBackedSet<Integer>(new Integer[] { 10, 50, 100, 110 });

        Assert.assertTrue(subject.forAnyApplies((i) -> i > 50));
        Assert.assertFalse(subject.forAnyApplies((i) -> i > 150));
    }

    @Test
    public void forAllApplies_arrayBacked() {
        ImmutableSet<Integer> subject = new ImmutableSetImpl.ArrayBackedSet<Integer>(new Integer[] { 10, 50, 100, 1000 });

        Assert.assertTrue(subject.forAllApplies((i) -> i > 5));
        Assert.assertFalse(subject.forAllApplies((i) -> i > 50));
    }

    @Test
    public void iterateMatching_arrayBacked() {
        ImmutableSet<Integer> subject = new ImmutableSetImpl.ArrayBackedSet<Integer>(new Integer[] { 10, 50, 100, 1000 });

        Set<Integer> filtered = new HashSet<>();

        for (Integer e : subject.iterateMatching((i) -> i > 50)) {
            filtered.add(e);
        }

        Assert.assertEquals(ImmutableSet.of(100, 1000), filtered);
    }

    @Test
    public void containsAll_oneElement() {
        ImmutableSet<String> subject = new ImmutableSetImpl.OneElementSet<>("A");

        Assert.assertTrue(subject.containsAll(Arrays.asList("A")));
        Assert.assertFalse(subject.containsAll(Arrays.asList("A", "B")));
        Assert.assertFalse(subject.containsAll(Arrays.asList("B")));
    }

    @Test
    public void matching_oneElement() {
        ImmutableSet<Integer> subject = new ImmutableSetImpl.OneElementSet<>(100);

        Assert.assertEquals(ImmutableSet.of(100), subject.matching((i) -> i > 10));
        Assert.assertEquals(ImmutableSet.empty(), subject.matching((i) -> i < 10));
    }

    @Test
    public void of_setWithOneAdditionalEntry() {
        Set<String> base = new HashSet<>();
        base.add("a");

        ImmutableSet<String> subject = ImmutableSet.of(base, "d");
        Set<String> reference = new HashSet<>(base);
        reference.add("d");
        Assert.assertEquals(reference, subject);

        base.add("b");
        base.add("c");

        subject = ImmutableSet.of(base, "d");
        reference = new HashSet<>(base);
        reference.add("d");

        Assert.assertEquals(reference, subject);
    }

    @Test
    public void ofNonNull() {
        Assert.assertEquals(ImmutableSet.empty(), ImmutableSet.ofNonNull(null, null));
        Assert.assertEquals(ImmutableSet.of("a"), ImmutableSet.ofNonNull("a", null));
        Assert.assertEquals(ImmutableSet.of("b"), ImmutableSet.ofNonNull(null, "b"));
        Assert.assertEquals(ImmutableSet.of("a", "b"), ImmutableSet.ofNonNull("a", "b"));
    }

    @Test
    public void collector() {
        Set<Integer> reference = new HashSet<>(Arrays.asList(1, 2, 3));
        ImmutableSet<Integer> subject = reference.stream().collect(ImmutableSet.collector());
        Assert.assertEquals(reference, subject);
    }

    @Test
    public void collector_parallel() {
        Set<Integer> reference = new HashSet<>(Arrays.asList(1, 2, 3));
        ImmutableSet<Integer> subject = reference.stream().parallel().collect(ImmutableSet.collector());
        Assert.assertEquals(reference, subject);
    }

    private static <E> void assertEquals(Set<E> expected, ImmutableSet<E> actual) {

        for (E e : expected) {

            if (!actual.contains(e)) {
                Assert.fail("Not found in actual: " + e + ";\nexpected (" + expected.size() + "): " + expected + "\nactual (" + actual.size() + "): "
                        + actual);
            }
        }

        for (E e : actual) {
            if (!expected.contains(e)) {
                Assert.fail("Not found in expected: " + e + ";\nexpected (" + expected.size() + "): " + expected + "\nactual (" + actual.size()
                        + "): " + actual);
            }
        }

        if (expected.size() != actual.size()) {
            Assert.fail("Size does not match: " + expected.size() + " vs " + actual.size() + ";\nexpected: " + expected + "\nactual: " + actual);
        }
    }
}
