/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections.views;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.fluent.collections.UnmodifiableIterator;

public class IteratorViewTest {

    @Test
    public void map() {
        List<String> base = new ArrayList<>();
        base.add("a");
        base.add("b");
        base.add("c");
        IteratorView<String> subject = IteratorView.map(base.iterator(), String::toUpperCase);
        Iterator<String> reference = base.stream().map(String::toUpperCase).collect(Collectors.toList()).iterator();

        for (;;) {
            boolean subjectHasNext = subject.hasNext();
            boolean referenceHasNext = reference.hasNext();

            Assert.assertEquals(referenceHasNext, subjectHasNext);

            if (!subjectHasNext) {
                break;
            }

            Object subjectNext = subject.next();
            Object referenceNext = reference.next();

            Assert.assertEquals(referenceNext, subjectNext);
        }
    }

    @Test
    public void filter() {
        List<String> base = new ArrayList<>();
        base.add("x");
        base.add("xx");
        base.add("xxx");
        UnmodifiableIterator<String> subject = IteratorView.filter(base.iterator(), (s) -> s.length() <= 1);
        Iterator<String> reference = base.stream().filter((s) -> s.length() <= 1).collect(Collectors.toList()).iterator();

        for (;;) {
            boolean subjectHasNext = subject.hasNext();
            boolean referenceHasNext = reference.hasNext();

            Assert.assertEquals(referenceHasNext, subjectHasNext);

            if (!subjectHasNext) {
                break;
            }

            Object subjectNext = subject.next();
            Object referenceNext = reference.next();

            Assert.assertEquals(referenceNext, subjectNext);
        }
    }

}
