/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections.views;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.fluent.collections.UnmodifiableMap;

public class MapViewTest {

    @Test
    public void mapValues() {
        HashMap<String, String> base = new HashMap<>();
        base.put("a", "aa");
        base.put("b", "bb");
        base.put("c", "cc");

        MapView<String, String> subject = MapView.mapValues(base, String::toUpperCase);
        Map<String, String> reference = base.entrySet().stream()
                .map((Function<Map.Entry<String, String>, Map.Entry<String, String>>) (e) -> new AbstractMap.SimpleEntry<>(e.getKey(),
                        e.getValue().toUpperCase()))
                .collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue()));

        Assert.assertEquals(reference, subject);
        Assert.assertEquals(reference.size(), subject.size());
        Assert.assertEquals(reference.hashCode(), subject.hashCode());
        Assert.assertEquals(reference.keySet(), subject.keySet());
        Assert.assertEquals(new HashSet<>(reference.values()), new HashSet<>(subject.values()));
        Assert.assertEquals(reference.entrySet(), subject.entrySet());
    }

    @Test
    public void mapValues_containsValue() {
        HashMap<String, String> base = new HashMap<>();
        base.put("a", "aa");
        base.put("b", "bb");
        base.put("c", "cc");
        MapView<String, String> subject = MapView.mapValues(base, String::toUpperCase);

        Assert.assertTrue(subject.containsValue("AA"));
        Assert.assertFalse(subject.containsValue("aa"));
    }

    @Test
    public void mapValues_get() {
        HashMap<String, String> base = new HashMap<>();
        base.put("a", "aa");
        base.put("b", "bb");
        base.put("c", "cc");
        MapView<String, String> subject = MapView.mapValues(base, String::toUpperCase);

        Assert.assertEquals("AA", subject.get("a"));
    }

    @Test
    public void stringKeys() {
        HashMap<Object, String> base = new HashMap<>();
        base.put("a", "A");
        base.put("b", "B");
        base.put(1, "1");

        UnmodifiableMap<String, Object> subject = MapView.stringKeys(base);
        Map<String, Object> reference = base.entrySet().stream()
                .map((e) -> new AbstractMap.SimpleEntry<>(e.getKey() != null ? e.getKey().toString() : null, e.getValue()))
                .collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue()));

        Assert.assertEquals(reference, subject);
        Assert.assertEquals(reference.size(), subject.size());
        Assert.assertEquals(reference.hashCode(), subject.hashCode());
        Assert.assertEquals(reference.keySet(), subject.keySet());
        Assert.assertEquals(new HashSet<>(reference.values()), new HashSet<>(subject.values()));
        Assert.assertEquals(reference.entrySet(), subject.entrySet());
    }

    @Test
    public void stringKeys_unchanged_unmodifiable() {
        HashMap<Object, String> base = new HashMap<>();
        base.put("a", "A");
        base.put("b", "B");
        base.put("c", "C");
        UnmodifiableMap<?, ?> unmodifieableBase = UnmodifiableMap.of(base);
        UnmodifiableMap<?, ?> subject = MapView.stringKeys(unmodifieableBase);

        Assert.assertTrue(unmodifieableBase == subject);
    }

    @Test
    public void stringKeysUnsafe() {
        HashMap<Object, String> base = new HashMap<>();
        base.put("a", "A");
        base.put("b", "B");
        base.put(1, "1");

        Map<String, Object> subject = MapView.stringKeysUnsafe(base);
        Map<String, Object> reference = base.entrySet().stream()
                .map((e) -> new AbstractMap.SimpleEntry<>(e.getKey() != null ? e.getKey().toString() : null, e.getValue()))
                .collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue()));

        Assert.assertEquals(reference, subject);
        Assert.assertEquals(reference.size(), subject.size());
        Assert.assertEquals(reference.hashCode(), subject.hashCode());
        Assert.assertEquals(reference.keySet(), subject.keySet());
        Assert.assertEquals(new HashSet<>(reference.values()), new HashSet<>(subject.values()));
        Assert.assertEquals(reference.entrySet(), subject.entrySet());
    }

    @Test
    public void stringKeysUnsafe_unchanged() {
        HashMap<Object, String> base = new HashMap<>();
        base.put("a", "A");
        base.put("b", "B");
        base.put("c", "C");
        Map<?, ?> subject = MapView.stringKeysUnsafe(base);

        Assert.assertTrue(base == subject);
    }
}
