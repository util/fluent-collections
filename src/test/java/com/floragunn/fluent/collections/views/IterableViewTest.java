/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

import com.floragunn.fluent.collections.ImmutableList;

public class IterableViewTest {

    @Test
    public void map() {
        List<String> base = new ArrayList<>();
        base.add("a");
        base.add("b");
        base.add("c");
        IterableView<String> subjectIterable = IterableView.map(base, String::toUpperCase);
        Iterable<String> referenceIterable = base.stream().map(String::toUpperCase).collect(Collectors.toList());
        Iterator<String> subject = subjectIterable.iterator();
        Iterator<String> reference = referenceIterable.iterator();

        for (;;) {
            boolean subjectHasNext = subject.hasNext();
            boolean referenceHasNext = reference.hasNext();

            Assert.assertEquals(referenceHasNext, subjectHasNext);

            if (!subjectHasNext) {
                break;
            }

            Object subjectNext = subject.next();
            Object referenceNext = reference.next();

            Assert.assertEquals(referenceNext, subjectNext);
        }
    }

    @Test
    public void filter() {
        List<String> base = new ArrayList<>();
        base.add("x");
        base.add("xx");
        base.add("xxx");
        IterableView<String> subjectIterable = IterableView.filter(base, (s) -> s.length() <= 1);
        Iterator<String> subject = subjectIterable.iterator();
        Iterator<String> reference = base.stream().filter((s) -> s.length() <= 1).collect(Collectors.toList()).iterator();

        for (;;) {
            boolean subjectHasNext = subject.hasNext();
            boolean referenceHasNext = reference.hasNext();

            Assert.assertEquals(referenceHasNext, subjectHasNext);

            if (!subjectHasNext) {
                break;
            }

            Object subjectNext = subject.next();
            Object referenceNext = reference.next();

            Assert.assertEquals(referenceNext, subjectNext);
        }
    }
    
    @Test
    public void concat() {
        List<String> l1 = ImmutableList.of("a", "b", "c");
        List<String> l2 = ImmutableList.of("x", "y", "z");
        
        IterableView<String> subjectIterable = IterableView.concat(l1, l2);
        
        List<String> result = new ArrayList<>();
        
        for (String string : subjectIterable) {
            result.add(string);
        }
        
        Assert.assertEquals(Arrays.asList("a", "b", "c", "x", "y", "z"), result);
    }

}
