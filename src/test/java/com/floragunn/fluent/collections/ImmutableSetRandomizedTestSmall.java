/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ImmutableSetRandomizedTestSmall {
    @Parameter
    public Integer seed;

    @Parameters(name = "{0}")
    public static Collection<Integer> seeds() {
        ArrayList<Integer> result = new ArrayList<>(1000);

        for (int i = 100; i < 1000; i++) {
            result.add(i);
        }

        return result;
    }

    @Test
    public void iterateMatching() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        ImmutableSet<String> subject = ImmutableSet.of(initialContent);

        assertEquals(reference, subject);

        for (int i = 0; i < 10; i++) {
            HashSet<String> intersectionSourceReference = new HashSet<>();

            for (String string : subject) {
                if (random.nextFloat() < 0.5) {
                    intersectionSourceReference.add(string);
                }
            }

            Set<String> matchingSubject = new HashSet<String>();

            for (String string : subject.iterateMatching((e) -> intersectionSourceReference.contains(e))) {
                matchingSubject.add(string);
            }

            Assert.assertEquals(intersectionSourceReference, matchingSubject);
        }
    }

    @Test
    public void forAllApplies() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        ImmutableSet<String> subject = ImmutableSet.of(initialContent);

        assertEquals(reference, subject);

        Assert.assertTrue(subject.forAllApplies((e) -> reference.contains(e)));

        if (!subject.isEmpty()) {
            ImmutableSet<String> subjectLessOne = subject.without(subject.any());
            Assert.assertFalse(subject.forAllApplies((e) -> subjectLessOne.contains(e)));
        }
    }

    @Test
    public void forAnyApplies() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        ImmutableSet<String> subject = ImmutableSet.of(initialContent);

        assertEquals(reference, subject);

        if (!subject.isEmpty()) {
            String any = subject.any();

            Assert.assertTrue(reference.contains(any));
            Assert.assertTrue(subject.forAnyApplies((e) -> e.equals(any)));
            Assert.assertFalse(subject.forAnyApplies((e) -> e.equals("foobar")));
        } else {
            Assert.assertFalse(subject.forAnyApplies((e) -> false));
        }
    }

    @Test
    public void map() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        ImmutableSet<String> subject = ImmutableSet.of(initialContent);

        assertEquals(reference, subject);

        ImmutableSet<String> mappedSubject = subject.map((e) -> e.toUpperCase());
        Set<String> mappedReference = reference.stream().map((e) -> e.toUpperCase()).collect(Collectors.toSet());

        assertEquals(mappedReference, mappedSubject);
    }

    @Test
    public void mapFlat() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<List<String>> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            int c2 = random.nextInt(10);

            List<String> c = new ArrayList<>(c2);

            for (int k = 0; k < c2; k++) {
                c.add(randomString(random));
            }

            initialContent.add(c);
        }

        HashSet<List<String>> reference = new HashSet<>(initialContent);
        ImmutableSet<List<String>> subject = ImmutableSet.of(initialContent);

        assertEquals(reference, subject);

        ImmutableSet<String> mappedSubject = subject.mapFlat((e) -> e.stream().map((e2) -> e2.toUpperCase()).collect(Collectors.toSet()));
        Set<String> mappedReference = reference.stream().flatMap((e) -> e.stream().map((e2) -> e2.toUpperCase())).collect(Collectors.toSet());

        assertEquals(mappedReference, mappedSubject);
    }

    @Test
    public void mapStatic() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        Set<String> mappedReference = reference.stream().map((e) -> e.toUpperCase()).collect(Collectors.toSet());
        ImmutableSet<String> mappedSubject = ImmutableSet.map(initialContent, (e) -> e.toUpperCase());

        assertEquals(mappedReference, mappedSubject);
    }

    @Test
    public void toArray() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        ImmutableSet<String> subject = ImmutableSet.of(initialContent);

        assertEquals(reference, subject);

        Object[] subjectArray = subject.toArray();
        Assert.assertEquals(reference,
                new HashSet<String>(Arrays.asList(subjectArray).stream().map((e) -> e.toString()).collect(Collectors.toSet())));

        String[] subjectStringArray = subject.toArray(new String[0]);
        Assert.assertEquals(reference, new HashSet<String>(Arrays.asList(subjectStringArray)));

        String[] subjectStringArray2 = subject.toArray(new String[subject.size()]);
        Assert.assertEquals(reference, new HashSet<String>(Arrays.asList(subjectStringArray2)));
    }

    @Test
    public void toList() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        ImmutableSet<String> subject = ImmutableSet.of(initialContent);

        assertEquals(reference, subject);

        ImmutableList<String> subjectList = subject.toList();
        Assert.assertEquals(reference, new HashSet<String>(subjectList));
    }
    
    @Test
    public void toMap() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        ImmutableSet<String> subject = ImmutableSet.of(initialContent);

        assertEquals(reference, subject);

        ImmutableMap<String, String> subjectMap = subject.toMap((k) -> "mapped_" + k.toUpperCase());
        HashMap<String, String> referenceMap = new HashMap<>();
        
        for (String e : reference) {
            referenceMap.put(e, "mapped_" + e.toUpperCase());
        }
        
        Assert.assertEquals(referenceMap, subjectMap);
    }


    @Test
    public void hashCodeEquals() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        ImmutableSet<String> subject = ImmutableSet.of(initialContent);

        Assert.assertEquals(reference, subject);
        Assert.assertEquals(reference.hashCode(), subject.hashCode());
    }

    @Test
    public void ofIterable() {
        Random random = new Random(seed);

        int initialCount = initialCount(random);

        List<String> initialContent = new ArrayList<>();

        for (int i = 0; i < initialCount; i++) {
            initialContent.add(randomString(random));
        }

        HashSet<String> reference = new HashSet<>(initialContent);
        ImmutableSet<String> subject = ImmutableSet.of(new Iterable<String>() {

            @Override
            public Iterator<String> iterator() {
                return reference.iterator();
            }

        });

        Assert.assertEquals(reference, subject);

    }

    private static int initialCount(Random random) {
        float f = random.nextFloat();

        if (f < 0.3) {
            return random.nextInt(10);
        } else if (f < 0.7) {
            return random.nextInt(40);
        } else {
            return random.nextInt(300);
        }
    }

    private static <E> void assertEquals(Set<E> expected, ImmutableSet<E> actual) {

        for (E e : expected) {

            if (!actual.contains(e)) {
                Assert.fail("Not found in actual: " + e + ";\nexpected (" + expected.size() + "): " + expected + "\nactual (" + actual.size() + "): "
                        + actual);
            }
        }

        for (E e : actual) {
            if (!expected.contains(e)) {
                Assert.fail("Not found in expected: " + e + ";\nexpected (" + expected.size() + "): " + expected + "\nactual (" + actual.size()
                        + "): " + actual);
            }
        }

        if (expected.size() != actual.size()) {
            Assert.fail("Size does not match: " + expected.size() + " vs " + actual.size() + ";\nexpected: " + expected + "\nactual: " + actual);
        }
    }

    static String[] ipAddresses = createRandomIpAddresses(new Random(9));
    static String[] locationNames = createRandomLocationNames(new Random(2));

    private static String randomString(Random random) {
        if (random.nextFloat() < 0.5) {
            return randomIpAddress(random);
        } else {
            return randomLocationName(random);
        }
    }

    private static String randomIpAddress(Random random) {
        return ipAddresses[random.nextInt(ipAddresses.length)];
    }

    private static String randomLocationName(Random random) {
        int i = (int) Math.floor(random.nextGaussian() * locationNames.length * 0.333 + locationNames.length);

        if (i < 0 || i >= locationNames.length) {
            i = random.nextInt(locationNames.length);
        }

        return locationNames[i];
    }

    private static String[] createRandomIpAddresses(Random random) {
        String[] result = new String[2000];

        for (int i = 0; i < result.length; i++) {
            result[i] = (random.nextInt(10) + 100) + "." + (random.nextInt(5) + 100) + "." + random.nextInt(255) + "." + random.nextInt(255);
        }

        return result;
    }

    private static String[] createRandomLocationNames(Random random) {
        String[] p1 = new String[] { "Schön", "Schöner", "Tempel", "Friedens", "Friedrichs", "Blanken", "Rosen", "Charlotten", "Malch", "Lichten",
                "Lichter", "Hasel", "Kreuz", "Pank", "Marien", "Adlers", "Zehlen", "Haken", "Witten", "Jungfern", "Hellers", "Finster", "Birken",
                "Falken", "Freders", "Karls", "Grün", "Wilmers", "Heiners", "Lieben", "Marien", "Wiesen", "Biesen", "Schmachten", "Rahns", "Rangs",
                "Herms", "Rüders", "Wuster", "Hoppe", "Waidmanns", "Wolters", "Schmargen" };
        String[] p2 = new String[] { "au", "ow", "berg", "feld", "felde", "tal", "thal", "höhe", "burg", "horst", "hausen", "dorf", "hof", "heide",
                "weide", "hain", "walde", "linde", "hagen", "eiche", "witz", "rade", "werder", "see", "fließ", "krug", "mark", "lust" };

        ArrayList<String> result = new ArrayList<>(p1.length * p2.length);

        for (int i = 0; i < p1.length; i++) {
            for (int k = 0; k < p2.length; k++) {
                result.add(p1[i] + p2[k]);
            }
        }

        Collections.shuffle(result, random);

        return result.toArray(new String[result.size()]);
    }

}
