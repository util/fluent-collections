/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ImmutableListTest {

    @Test
    public void of_1e() {
        Assert.assertEquals(Arrays.asList(1), ImmutableList.of(1));
    }

    @Test
    public void of_2e() {
        Assert.assertEquals(Arrays.asList(1, 2), ImmutableList.of(1, 2));
    }

    @Test
    public void of_3e() {
        Assert.assertEquals(Arrays.asList(1, 2, 3), ImmutableList.of(1, 2, 3));
    }

    @Test
    public void of_4e() {
        Assert.assertEquals(Arrays.asList(1, 2, 3, 4), ImmutableList.of(1, 2, 3, 4));
    }

    @Test
    public void ofNonNull_1e() {
        Assert.assertEquals(Arrays.asList(), ImmutableList.ofNonNull(null));
        Assert.assertEquals(Arrays.asList(1), ImmutableList.ofNonNull(1));
    }

    @Test
    public void ofNonNull_2e() {
        Assert.assertEquals(Arrays.asList(), ImmutableList.ofNonNull(null, null));
        Assert.assertEquals(Arrays.asList(1), ImmutableList.ofNonNull(null, 1));
        Assert.assertEquals(Arrays.asList(1), ImmutableList.ofNonNull(1, null));
        Assert.assertEquals(Arrays.asList(1, 2), ImmutableList.ofNonNull(1, 2));
    }

    @Test
    public void ofNonNull_3e() {
        Assert.assertEquals(Arrays.asList(), ImmutableList.ofNonNull(null, null, (Integer) null));
        Assert.assertEquals(Arrays.asList(1, 2), ImmutableList.ofNonNull((Integer) null, 1, 2));
        Assert.assertEquals(Arrays.asList(1, 2), ImmutableList.ofNonNull(1, (Integer) null, 2));
        Assert.assertEquals(Arrays.asList(1, 2), ImmutableList.ofNonNull(1, 2, (Integer) null));
        Assert.assertEquals(Arrays.asList(1, 2, 3), ImmutableList.ofNonNull(1, 2, 3));
    }

    @Test
    public void ofNonNull_4e() {
        Assert.assertEquals(Arrays.asList(), ImmutableList.ofNonNull(null, null, (Integer) null, (Integer) null));
        Assert.assertEquals(Arrays.asList(1, 2, 3), ImmutableList.ofNonNull(1, 2, 3, null));
        Assert.assertEquals(Arrays.asList(1, 2, 3, 4), ImmutableList.ofNonNull(1, 2, 3, 4));
    }

    @Test
    public void collector() {
        List<Integer> reference = Arrays.asList(1, 2, 3);
        ImmutableList<Integer> subject = reference.stream().collect(ImmutableList.collector());
        Assert.assertEquals(reference, subject);
    }

    @Test
    public void collector_parallel() {
        List<Integer> reference = Arrays.asList(1, 2, 3);
        ImmutableList<Integer> subject = reference.stream().parallel().collect(ImmutableList.collector());
        Assert.assertEquals(reference, subject);
    }
}
