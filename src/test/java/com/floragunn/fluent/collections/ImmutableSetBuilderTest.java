/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class ImmutableSetBuilderTest {

    @Test
    public void constructor_array() {
        Set<String> reference = new HashSet<>(Arrays.asList("a", "b", "c", "d", "e", "xxx"));
        ImmutableSet.Builder<String> subject = new ImmutableSet.Builder<>(reference.toArray(new String[0]));
        Assert.assertEquals(reference, subject.build());
    }

    @Test
    public void clear() {
        ImmutableSet.Builder<String> subject = new ImmutableSet.Builder<String>(new String[] { "a", "b", "c", "d", "e", "xxx" });
        Assert.assertEquals(6, subject.size());
        subject.clear();
        Assert.assertEquals(0, subject.size());
    }

    @Test
    public void containsAll() {
        Set<String> reference = new HashSet<>(Arrays.asList("a", "b", "c", "d", "e", "xxx"));
        ImmutableSet.Builder<String> subject = new ImmutableSet.Builder<>(reference);
        Assert.assertTrue(subject.containsAll(Collections.singleton("a")));
        Assert.assertTrue(subject.containsAll(reference));
        Assert.assertFalse(subject.containsAll(ImmutableSet.of(reference).with("y")));
        Assert.assertFalse(subject.containsAll(Collections.singleton("X")));

        subject.add("y");
        Assert.assertTrue(subject.containsAll(Collections.singleton("a")));
        Assert.assertTrue(subject.containsAll(reference));
        Assert.assertTrue(subject.containsAll(ImmutableSet.of(reference).with("y")));
        Assert.assertFalse(subject.containsAll(Collections.singleton("X")));
    }

    @Test
    public void containsAny() {
        Set<String> reference = new HashSet<>(Arrays.asList("a", "b", "c", "d", "e", "xxx"));
        ImmutableSet.Builder<String> subject = new ImmutableSet.Builder<>(reference);
        Assert.assertTrue(subject.containsAny(Collections.singleton("a")));
        Assert.assertTrue(subject.containsAny(reference));
        Assert.assertFalse(subject.containsAny(Collections.singleton("X")));
        Assert.assertTrue(subject.containsAny(ImmutableSet.of("a", "X")));

        subject.add("y");
        Assert.assertTrue(subject.containsAny(Collections.singleton("a")));
        Assert.assertTrue(subject.containsAny(reference));
        Assert.assertFalse(subject.containsAny(Collections.singleton("X")));
        Assert.assertTrue(subject.containsAny(ImmutableSet.of("a", "X")));
        Assert.assertTrue(subject.containsAny(Collections.singleton("y")));
    }

    @Test
    public void addAll() {
        Set<String> reference = new HashSet<>(Arrays.asList("a", "b", "c", "d", "e", "xxx"));
        ImmutableSet.Builder<String> subject = new ImmutableSet.Builder<>(reference);

        List<String> toBeAdded = Arrays.asList("y", "z");
        reference.addAll(toBeAdded);
        subject.addAll(toBeAdded);
        Assert.assertEquals(reference, subject.build());
    }

    @Test
    public void addAll_array() {
        Set<String> reference = new HashSet<>(Arrays.asList("a", "b", "c", "d", "e", "xxx"));
        ImmutableSet.Builder<String> subject = new ImmutableSet.Builder<>(reference);

        List<String> toBeAdded = Arrays.asList("y", "z");
        reference.addAll(toBeAdded);
        subject.addAll(toBeAdded.toArray(new String[0]));
        Assert.assertEquals(reference, subject.build());
    }
}
