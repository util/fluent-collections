package com.floragunn.fluent.collections;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class CheckTableTest {

    @Test
    public void test() {
        Set<String> rows = ImmutableSet.of("esb-prod-1", "esb-prod-2", "esb-prod-3", "esb-prod-4", "esb-prod-5", ".monitoring-es-7-2021.12.30",
                ".async-search", "humanresources", "finance", "suggest", "nested", "logs", "auditlog-2021.12.30");

        Set<String> columns = ImmutableSet.of("indices:admin/settings/update");

        CheckTable<String, String> checkTable = CheckTable.create(rows, columns);

        checkTable.checkIf(rows, (action) -> true);

        Assert.assertTrue(checkTable.toString(), checkTable.isComplete());
    }

    @Test
    public void view_uncheckAll() {
        Set<String> rows = ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3");
        Set<Integer> columns = ImmutableSet.of(1, 2, 3, 4, 5, 6);
        CheckTable<String, Integer> root = CheckTable.create(rows, columns);
        root.checkIf(rows, (i) -> true);
        Assert.assertTrue(root.isComplete());
        CheckTable<String, Integer> view = root.viewRestrictedToRows((r) -> r.startsWith("a"));
        Assert.assertTrue(view.isComplete());
        view.uncheckAll();
        Assert.assertTrue(view.isBlank());
        Assert.assertFalse(root.isBlank());
        Assert.assertFalse(view.isComplete());
        Assert.assertTrue(root.isChecked("b1", 1));
        Assert.assertFalse(root.isChecked("a1", 1));
    }
    
    @Test
    public void view_checkIf() {
        Set<String> rows = ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3");
        Set<Integer> columns = ImmutableSet.of(1, 2, 3, 4, 5, 6);
        CheckTable<String, Integer> root = CheckTable.create(rows, columns);
        CheckTable<String, Integer> view = root.viewRestrictedToRows((r) -> r.startsWith("a"));
        view.checkIf("a1", (i) -> i >= 4);        
        Assert.assertTrue(view.isChecked("a1", 4));
        Assert.assertFalse(view.isChecked("a1", 3));
        Assert.assertFalse(view.isChecked("a2", 4));
        Assert.assertTrue(root.isChecked("a1", 4));
        Assert.assertFalse(root.isChecked("a1", 3));
        Assert.assertFalse(root.isChecked("a2", 4));
        Assert.assertFalse(root.isChecked("b2", 4));
                
        view.checkIf((r) -> r.equals("a1"), 3); 
        Assert.assertTrue(view.isChecked("a1", 3));
        Assert.assertTrue(root.isChecked("a1", 3));
        Assert.assertFalse(view.isChecked("a2", 3));
    }
    
    @Test
    public void view_uncheckIf() {
        Set<String> rows = ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3");
        Set<Integer> columns = ImmutableSet.of(1, 2, 3, 4, 5, 6);
        CheckTable<String, Integer> root = CheckTable.create(rows, columns);
        root.checkIf(rows, (i) -> true);
        CheckTable<String, Integer> view = root.viewRestrictedToRows((r) -> r.startsWith("a"));
        view.uncheckIf("a1", (i) -> i >= 4);        
        Assert.assertFalse(view.isChecked("a1", 4));
        Assert.assertTrue(view.isChecked("a1", 3));
        Assert.assertTrue(view.isChecked("a2", 4));
        Assert.assertFalse(root.isChecked("a1", 4));
        Assert.assertTrue(root.isChecked("a1", 3));
        Assert.assertTrue(root.isChecked("a2", 4));
        Assert.assertTrue(root.isChecked("b2", 4));
                
        view.uncheckIf((r) -> r.equals("a1"), 3); 
        Assert.assertFalse(view.isChecked("a1", 3));
        Assert.assertFalse(root.isChecked("a1", 3));
        Assert.assertTrue(view.isChecked("a2", 3));
    }
    
    @Test
    public void view_uncheckRowIf() {
        Set<String> rows = ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3");
        Set<Integer> columns = ImmutableSet.of(1, 2, 3, 4, 5, 6);
        CheckTable<String, Integer> root = CheckTable.create(rows, columns);
        root.checkIf(rows, (i) -> true);
        CheckTable<String, Integer> view = root.viewRestrictedToRows((r) -> r.startsWith("a"));
        view.uncheckRowIf((r) -> r.equals("a1"));       
        Assert.assertFalse(view.isChecked("a1", 1));
        Assert.assertTrue(view.isChecked("a2", 1));
    }
    
    @Test
    public void view_uncheckRow() {
        Set<String> rows = ImmutableSet.of("a1", "a2", "a3", "b1", "b2", "b3");
        Set<Integer> columns = ImmutableSet.of(1, 2, 3, 4, 5, 6);
        CheckTable<String, Integer> root = CheckTable.create(rows, columns);
        root.checkIf(rows, (i) -> true);
        CheckTable<String, Integer> view = root.viewRestrictedToRows((r) -> r.startsWith("a"));
        view.uncheckRow("a1");       
        Assert.assertFalse(view.isChecked("a1", 1));
        Assert.assertTrue(view.isChecked("a2", 1));
        view.uncheckRowIfPresent("a2");       
        Assert.assertFalse(view.isChecked("a2", 1));
        Assert.assertTrue(view.isChecked("a3", 1));


    }
}
