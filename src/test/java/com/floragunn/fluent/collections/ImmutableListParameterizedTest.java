/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ImmutableListParameterizedTest {
    @Parameter
    public List<String> base;

    @Parameters(name = "{0}")
    public static Collection<List<String>> bases() {
        List<List<String>> result = new ArrayList<>();

        result.add(java.util.Collections.emptyList());
        result.add(Arrays.asList("a"));
        result.add(Arrays.asList("a", "bb"));
        result.add(Arrays.asList("a", "bb", "ccc"));
        result.add(Arrays.asList("a", "bb", "x", "ccc", "x", "dddd", "x", "eeeee", "x", "fffff"));

        result.add(Arrays.asList("aa"));
        result.add(Arrays.asList("aa", "b"));
        result.add(Arrays.asList("aa", "b", "c"));
        result.add(Arrays.asList("aa", "b", "x", "ccc", "x", "dddd", "x", "eeeee", "x", "fffff"));

        result.add(Arrays.asList("aa"));
        result.add(Arrays.asList("aa", "bb"));
        result.add(Arrays.asList("aa", "bb", "ccc"));

        result.add(Arrays.asList("a"));
        result.add(Arrays.asList("a", "b"));
        result.add(Arrays.asList("a", "b", "c"));

        return result;
    }

    @Test
    public void matching() {
        ImmutableList<String> subject = ImmutableList.of(base).matching((s) -> s.length() > 1);
        List<String> reference = base.stream().filter((s) -> s.length() > 1).collect(Collectors.toList());
        Assert.assertEquals(reference, subject);
    }

    @Test
    public void containsAll() {
        ImmutableList<String> subject = ImmutableList.of(base);
        ImmutableList<String> subSet = ImmutableList.of(base).matching((s) -> s.length() > 1);
        Assert.assertTrue(subject.containsAll(subSet));

        ImmutableList<String> superSet = subject.with("zzz");
        Assert.assertFalse(subject.containsAll(superSet));
    }

    @Test
    public void map() {
        ImmutableList<String> subject = ImmutableList.of(base).map((s) -> s.toUpperCase());
        List<String> reference = base.stream().map((s) -> s.toUpperCase()).collect(Collectors.toList());
        Assert.assertEquals(reference, subject);
    }

    @Test
    public void map_withNull() {
        ImmutableList<String> subject = ImmutableList.of(base).map((s) -> s.length() > 1 ? s.toUpperCase() : null);
        List<String> reference = base.stream().filter((s) -> s.length() > 1).map((s) -> s.toUpperCase()).collect(Collectors.toList());
        Assert.assertEquals(reference, subject);
    }

    @Test
    public void toArray() {
        String[] subject = ImmutableList.of(base).toArray(new String[0]);
        String[] reference = base.toArray(new String[0]);
        Assert.assertArrayEquals(reference, subject);
    }

    @Test
    public void toArray_object() {
        Object[] subject = ImmutableList.of(base).toArray();
        Object[] reference = base.toArray();
        Assert.assertArrayEquals(reference, subject);
    }

    @Test
    public void indexOf() {
        ImmutableList<String> subject = ImmutableList.of(base);
        Assert.assertEquals(new ArrayList<>(base).indexOf("bb"), subject.indexOf("bb"));
        Assert.assertEquals(new ArrayList<>(base).indexOf("ccc"), subject.indexOf("ccc"));
        Assert.assertEquals(new ArrayList<>(base).indexOf("x"), subject.indexOf("x"));

        if (subject.size() > 0) {
            Assert.assertEquals(0, subject.indexOf(subject.get(0)));
        }

        if (subject.size() > 1) {
            Assert.assertEquals(1, subject.indexOf(subject.get(1)));
        }
    }

    @Test
    public void lastIndexOf() {
        ImmutableList<String> subject = ImmutableList.of(base);
        Assert.assertEquals(new ArrayList<>(base).lastIndexOf("bb"), subject.lastIndexOf("bb"));
        Assert.assertEquals(new ArrayList<>(base).lastIndexOf("ccc"), subject.lastIndexOf("ccc"));
        Assert.assertEquals(new ArrayList<>(base).lastIndexOf("x"), subject.lastIndexOf("x"));
    }

    @Test
    public void subList() {
        ImmutableList<String> subject = ImmutableList.of(base);
        
        Assert.assertEquals(base.subList(0, base.size()), subject.subList(0, base.size()));

        if (base.size() > 0) {
            Assert.assertEquals(base.subList(0, base.size() - 1), subject.subList(0, base.size() - 1));
            Assert.assertEquals(base.subList(0, 1), subject.subList(0, 1));
        }

        if (base.size() > 1) {
            Assert.assertEquals(base.subList(1, base.size()), subject.subList(1, base.size()));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void subList_indexOutOfBounds() {
        ImmutableList<String> subject = ImmutableList.of(base);

        subject.subList(0, subject.size() + 1);
    }
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void subList_indexOutOfBounds_negative() {
        ImmutableList<String> subject = ImmutableList.of(base);

        subject.subList(-1, -1);
    }

    @Test
    public void forAllApplies() {
        ImmutableList<String> subject = ImmutableList.of(base);
        Assert.assertEquals(base.stream().allMatch((s) -> s.length() > 1), subject.forAllApplies((s) -> s.length() > 1));
        Assert.assertEquals(base.stream().allMatch((s) -> s.length() <= 1), subject.forAllApplies((s) -> s.length() <= 1));
    }

    @Test
    public void forAnyApplies() {
        ImmutableList<String> subject = ImmutableList.of(base);
        Assert.assertEquals(base.stream().anyMatch((s) -> s.length() > 1), subject.forAnyApplies((s) -> s.length() > 1));
        Assert.assertEquals(base.stream().anyMatch((s) -> s.length() <= 1), subject.forAnyApplies((s) -> s.length() <= 1));
    }

    @Test
    public void get() {
        ImmutableList<String> subject = ImmutableList.of(base);

        for (int i = 0; i < base.size(); i++) {
            Assert.assertEquals(base.get(i), subject.get(i));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void get_indexOutOfBounds() {
        ImmutableList<String> subject = ImmutableList.of(base);
        subject.get(subject.size());
    }

    @Test
    public void hashCodeTest() {
        ImmutableList<String> subject = ImmutableList.of(base);
        Assert.assertEquals(base.hashCode(), subject.hashCode());
    }

    @Test
    public void with() {
        ImmutableList<String> subject = ImmutableList.of(base);
        ArrayList<String> reference = new ArrayList<>(base);
        reference.add("added");
        Assert.assertEquals(reference, subject.with("added"));
    }

    @Test
    public void with_array() {
        ImmutableList<String> subject = ImmutableList.of(base);
        ArrayList<String> reference = new ArrayList<>(base);
        reference.addAll(Arrays.asList("added1", "added2"));
        Assert.assertEquals(reference, subject.with("added1", "added2"));
    }

    @Test
    public void with_collection() {
        ImmutableList<String> subject = ImmutableList.of(base);
        ArrayList<String> reference = new ArrayList<>(base);
        reference.addAll(Arrays.asList("added1", "added2"));
        Assert.assertEquals(reference, subject.with(Arrays.asList("added1", "added2")));
    }

    @Test
    public void with_optional() {
        ImmutableList<String> subject = ImmutableList.of(base);
        Assert.assertEquals(subject, subject.with(Optional.empty()));
        ArrayList<String> reference = new ArrayList<>(base);
        reference.add("added");
        Assert.assertEquals(reference, subject.with(Optional.of("added")));
    }

    @Test
    public void without() {
        ImmutableList<String> subject = ImmutableList.of(base);
        Assert.assertEquals(subject, subject.without(Arrays.asList("not contained")));

        if (subject.size() > 0) {
            String e = base.get(0);
            ArrayList<String> reference = new ArrayList<>(base);
            reference.removeAll(Arrays.asList(e));
            Assert.assertEquals(reference, subject.without(Arrays.asList(e)));
        }
    }

    @Test
    public void toStringTest() {
        ImmutableList<String> subject = ImmutableList.of(base);
        Assert.assertEquals(base.toString(), subject.toString());
    }

    @Test
    public void concat2() {
        ArrayList<String> reference = new ArrayList<>(base);
        reference.add("added");
        Assert.assertEquals(reference, ImmutableList.concat(base, Arrays.asList("added")));
    }

    @Test
    public void concat3() {
        ArrayList<String> reference = new ArrayList<>(base);
        reference.add("added1");
        reference.add("added2");

        Assert.assertEquals(reference, ImmutableList.concat(base, Arrays.asList("added1"), Arrays.asList("added2")));
    }

    @Test
    public void first() {
        if (base.size() > 0) {
            Assert.assertEquals(base.get(0), ImmutableList.of(base).first());
        } else {
            try {
                ImmutableList.of(base).first();
                Assert.fail("first() should fail for empty lists");
            } catch (IndexOutOfBoundsException e) {

            }
        }
    }

    @Test
    public void last() {
        if (base.size() > 0) {
            Assert.assertEquals(base.get(base.size() - 1), ImmutableList.of(base).last());
        } else {
            try {
                ImmutableList.of(base).last();
                Assert.fail("last() should fail for empty lists");
            } catch (IndexOutOfBoundsException e) {

            }
        }
    }

}
