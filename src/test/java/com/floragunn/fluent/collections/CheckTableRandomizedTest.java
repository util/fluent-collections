package com.floragunn.fluent.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CheckTableRandomizedTest {
    public Integer seed;

    int rowCount;
    int columnCount;

    @Parameters(name = "{0}; {1}x{2}")
    public static Collection<Object[]> seeds() {
        ArrayList<Object[]> result = new ArrayList<>();
        Random random = new Random(1);

        result.add(new Object[] { 100, 1, 1 });
        result.add(new Object[] { 102, 1, 2 });
        result.add(new Object[] { 103, 2, 1 });
        result.add(new Object[] { 104, 2, 2 });

        for (int i = 105; i < 200; i++) {
            result.add(new Object[] { i, randomSize(random), randomSize(random) });
        }

        return result;
    }

    @Test
    public void basicTest() {
        Random random = new Random(seed);
        ImmutableSet<Integer> rows = createIntegerElements(random, rowCount);
        ImmutableSet<String> columns = createStringElements(random, columnCount);

        List<Integer> rowsList = new ArrayList<>(rows);
        Collections.shuffle(rowsList, random);
        List<String> columnsList = new ArrayList<>(columns);
        Collections.shuffle(columnsList, random);

        CheckTable<Integer, String> subject = CheckTable.create(rows, columns);

        Map<String, Set<Integer>> referenceCR = new HashMap<>();
        Map<Integer, Set<String>> referenceRC = new HashMap<>();

        int checkCount = 0;
        int totalCount = rows.size() * columns.size();

        for (int i = 0; i < Math.min(100, rowsList.size()); i++) {
            Integer row = rowsList.get(i);

            for (int k = 0; k < Math.min(100, columnsList.size()); k++) {
                String column = columnsList.get(k);

                boolean checkResult = subject.check(row, column);
                checkCount++;
                boolean checkResultReference = totalCount == checkCount;
                referenceCR.computeIfAbsent(column, (key) -> new HashSet<>()).add(row);
                referenceRC.computeIfAbsent(row, (key) -> new HashSet<>()).add(column);

                Assert.assertEquals(subject + " vs " + referenceCR, checkResultReference, checkResult);

                Assert.assertEquals(referenceCR.entrySet().stream().filter(entry -> entry.getValue().size() == rows.size())
                        .map(entry -> entry.getKey()).collect(Collectors.toSet()), subject.getCompleteColumns());

                Assert.assertEquals(referenceRC.entrySet().stream().filter(entry -> entry.getValue().size() == columns.size())
                        .map(entry -> entry.getKey()).collect(Collectors.toSet()), subject.getCompleteRows());

                Set<String> checkedColumns = new HashSet<>();
                subject.iterateCheckedColumns(row).forEach((c) -> checkedColumns.add(c));
                Assert.assertEquals(referenceRC.get(row), checkedColumns);

                Set<Integer> checkedRows = new HashSet<>();
                subject.iterateCheckedRows(column).forEach((r) -> checkedRows.add(r));
                Assert.assertEquals(referenceCR.get(column), checkedRows);

                Assert.assertTrue(subject.getIncompleteRows().toString(), !subject.getIncompleteRows().containsAny(subject.getCompleteRows()));
                Assert.assertTrue(subject.getIncompleteColumns().toString(),
                        !subject.getIncompleteColumns().containsAny(subject.getCompleteColumns()));

                Assert.assertEquals(referenceRC.get(row), subject.getCheckedColumns(row));
                Assert.assertEquals(referenceCR.get(column), subject.getCheckedRows(column));

                Set<String> uncheckedColumns = new HashSet<>();
                subject.iterateUncheckedColumns(row).forEach((c) -> uncheckedColumns.add(c));
                Assert.assertEquals(referenceRC.get(row), columns.without(uncheckedColumns));

                Set<Integer> uncheckedRows = new HashSet<>();
                subject.iterateUncheckedRows(column).forEach((r) -> uncheckedRows.add(r));
                Assert.assertEquals(uncheckedRows.toString(), referenceCR.get(column), rows.without(uncheckedRows));

                for (String c : checkedColumns) {
                    Assert.assertTrue(subject.isChecked(row, c));
                }

                for (Integer r : checkedRows) {
                    Assert.assertTrue(subject.isChecked(r, column));
                }
            }
        }

        int toUncheck = random.nextInt(checkCount);

        for (int i = 0; i < toUncheck; i++) {
            Integer rowToUncheck = rowsList.get(random.nextInt(rowsList.size()));
            List<String> checkedColumnsList = new ArrayList<String>();
            subject.iterateCheckedColumns(rowToUncheck).forEach(checkedColumnsList::add);
            ;

            if (checkedColumnsList.size() == 0) {
                continue;
            }

            String columnToUncheck = checkedColumnsList.get(random.nextInt(checkedColumnsList.size()));

            subject.uncheck(rowToUncheck, columnToUncheck);
            referenceCR.get(columnToUncheck).remove(rowToUncheck);
            referenceRC.get(rowToUncheck).remove(columnToUncheck);
            checkCount--;

            Set<String> checkedColumns = new HashSet<>();
            subject.iterateCheckedColumns(rowToUncheck).forEach((c) -> checkedColumns.add(c));
            Assert.assertEquals(referenceRC.get(rowToUncheck), checkedColumns);

            Set<Integer> checkedRows = new HashSet<>();
            subject.iterateCheckedRows(columnToUncheck).forEach((r) -> checkedRows.add(r));
            Assert.assertEquals(referenceCR.get(columnToUncheck), checkedRows);

            Assert.assertTrue(subject.getIncompleteRows().toString(), !subject.getIncompleteRows().containsAny(subject.getCompleteRows()));
            Assert.assertTrue(subject.getIncompleteColumns().toString(), !subject.getIncompleteColumns().containsAny(subject.getCompleteColumns()));

            Set<String> uncheckedColumns = new HashSet<>();
            subject.iterateUncheckedColumns(rowToUncheck).forEach((c) -> uncheckedColumns.add(c));
            Assert.assertEquals(referenceRC.get(rowToUncheck), columns.without(uncheckedColumns));

            Set<Integer> uncheckedRows = new HashSet<>();
            subject.iterateUncheckedRows(columnToUncheck).forEach((r) -> uncheckedRows.add(r));
            Assert.assertEquals(referenceCR.get(columnToUncheck), rows.without(uncheckedRows));

        }

        Collections.shuffle(columnsList, random);

        for (int i = 0; i < Math.min(50, columnsList.size()); i++) {
            String column = columnsList.get(i);

            Predicate<Integer> predicate = (r) -> r < 5000;
            Set<Integer> matchedRows = rows.without(referenceCR.computeIfAbsent(column, (key) -> new HashSet<>())).stream().filter(predicate)
                    .collect(Collectors.toSet());
            boolean complete = subject.checkIf(predicate, column);

            checkCount += matchedRows.size();
            Assert.assertEquals(checkCount == totalCount, complete);
            Assert.assertEquals(checkCount == totalCount, subject.isComplete());

            referenceCR.get(column).addAll(matchedRows);
            matchedRows.forEach((r) -> referenceRC.computeIfAbsent(r, (key) -> new HashSet<>()).add(column));

            Assert.assertEquals(referenceCR.get(column), subject.getCheckedRows(column));
        }

        Collections.shuffle(rowsList, random);

        for (int i = 0; i < Math.min(50, rowsList.size()); i++) {
            Integer row = rowsList.get(i);

            Predicate<String> predicate = (c) -> c.hashCode() < 0;
            Set<String> matchedColumns = columns.without(referenceRC.computeIfAbsent(row, (key) -> new HashSet<>())).stream().filter(predicate)
                    .collect(Collectors.toSet());
            boolean complete = subject.checkIf(row, predicate);

            checkCount += matchedColumns.size();
            Assert.assertEquals(checkCount == totalCount, complete);
            Assert.assertEquals(checkCount == totalCount, subject.isComplete());

            referenceRC.get(row).addAll(matchedColumns);
            matchedColumns.forEach((c) -> referenceCR.computeIfAbsent(c, (key) -> new HashSet<>()).add(row));

            Assert.assertEquals(referenceRC.get(row), subject.getCheckedColumns(row));
        }

        Collections.shuffle(columnsList, random);

        for (int i = 0; i < Math.min(50, columnsList.size()); i++) {
            String column = columnsList.get(i);

            Predicate<Integer> predicate = (r) -> r < 7000;
            Set<Integer> matchedRows = referenceCR.computeIfAbsent(column, (key) -> new HashSet<>()).stream().filter(predicate)
                    .collect(Collectors.toSet());
            subject.uncheckIf(predicate, column);

            checkCount -= matchedRows.size();
            referenceCR.get(column).removeAll(matchedRows);
            matchedRows.forEach((r) -> referenceRC.get(r).remove(column));

            Assert.assertEquals(referenceCR.get(column), subject.getCheckedRows(column));
        }

        Collections.shuffle(rowsList, random);

        for (int i = 0; i < Math.min(50, rowsList.size()); i++) {
            Integer row = rowsList.get(i);

            Predicate<String> predicate = (c) -> c.hashCode() < 1000;
            Set<String> matchedColumns = referenceRC.computeIfAbsent(row, (key) -> new HashSet<>()).stream().filter(predicate)
                    .collect(Collectors.toSet());
            subject.uncheckIf(row, predicate);

            checkCount -= matchedColumns.size();
            referenceRC.get(row).removeAll(matchedColumns);
            matchedColumns.forEach((c) -> referenceCR.get(c).remove(row));

            Assert.assertEquals(referenceRC.get(row), subject.getCheckedColumns(row));
        }
    }

    @Test
    public void view() {
        Random random = new Random(seed);
        ImmutableSet<Integer> rows = createIntegerElements(random, rowCount);
        ImmutableSet<String> columns = createStringElements(random, columnCount);

        CheckTable<Integer, String> subject = CheckTable.create(rows, columns);

        Map<String, Set<Integer>> referenceCR = new HashMap<>();
        Map<Integer, Set<String>> referenceRC = new HashMap<>();
        Map<String, Set<Integer>> viewReferenceCR = new HashMap<>();
        Map<Integer, Set<String>> viewReferenceRC = new HashMap<>();

        ImmutableSet<Integer> viewRows = rows.matching((r) -> random.nextFloat() < 0.3).with(rows.any());
        ImmutableSet<String> viewColumns = columns; // columns.matching((r) -> random.nextFloat() < 0.3).with(columns.any());

        CheckTable<Integer, String> view = subject.viewRestrictedToRows((r) -> viewRows.contains(r));

        List<Integer> rowsList = new ArrayList<>(rows);
        Collections.shuffle(rowsList, random);
        List<String> columnsList = new ArrayList<>(columns);
        Collections.shuffle(columnsList, random);
        List<Integer> viewRowsList = new ArrayList<>(viewRows);
        Collections.shuffle(viewRowsList, random);
        List<String> viewColumnsList = new ArrayList<>(viewColumns);
        Collections.shuffle(viewColumnsList, random);

        int checkCount = 0;
        int viewCheckCount = 0;
        int totalCount = rows.size() * columns.size();
        int totalViewCount = viewRows.size() * viewColumns.size();

        for (int i = 0; i < Math.min(20, rowsList.size()); i++) {
            Integer row = rowsList.get(i);

            for (int k = 0; k < Math.min(20, columnsList.size()); k++) {
                String column = columnsList.get(k);

                subject.check(row, column);
                checkCount++;
                referenceCR.computeIfAbsent(column, (key) -> new HashSet<>()).add(row);
                referenceRC.computeIfAbsent(row, (key) -> new HashSet<>()).add(column);

                if (viewRows.contains(row) && viewColumns.contains(column)) {
                    Assert.assertTrue(view.isChecked(row, column));
                    viewReferenceCR.computeIfAbsent(column, (key) -> new HashSet<>()).add(row);
                    viewReferenceRC.computeIfAbsent(row, (key) -> new HashSet<>()).add(column);
                    viewCheckCount++;
                }
            }
        }

        for (int i = 0; i < Math.min(50, viewRowsList.size()); i++) {
            Integer row = viewRowsList.get(i);

            for (int k = 0; k < Math.min(50, viewColumnsList.size()); k++) {
                String column = viewColumnsList.get(k);

                if (subject.isChecked(row, column)) {
                    Assert.assertTrue(view.isChecked(row, column));
                } else {
                    view.check(row, column);
                    Assert.assertTrue(subject.isChecked(row, column));
                    checkCount++;
                    viewCheckCount++;
                    referenceCR.computeIfAbsent(column, (key) -> new HashSet<>()).add(row);
                    referenceRC.computeIfAbsent(row, (key) -> new HashSet<>()).add(column);
                    viewReferenceCR.computeIfAbsent(column, (key) -> new HashSet<>()).add(row);
                    viewReferenceRC.computeIfAbsent(row, (key) -> new HashSet<>()).add(column);
                }

                Set<String> checkedColumns = new HashSet<>();
                view.iterateCheckedColumns(row).forEach((c) -> checkedColumns.add(c));
                Assert.assertEquals(viewReferenceRC.get(row), checkedColumns);

                Set<Integer> checkedRows = new HashSet<>();
                view.iterateCheckedRows(column).forEach((r) -> checkedRows.add(r));
                Assert.assertEquals(viewReferenceCR.get(column), checkedRows);

                Assert.assertTrue(view.getIncompleteRows().toString(), !view.getIncompleteRows().containsAny(view.getCompleteRows()));
                Assert.assertTrue(view.getIncompleteColumns().toString(), !view.getIncompleteColumns().containsAny(view.getCompleteColumns()));

                Assert.assertEquals(viewReferenceRC.get(row), view.getCheckedColumns(row));
                Assert.assertEquals(viewReferenceCR.get(column), view.getCheckedRows(column));

                Set<String> uncheckedColumns = new HashSet<>();
                view.iterateUncheckedColumns(row).forEach((c) -> uncheckedColumns.add(c));
                Assert.assertEquals(viewReferenceRC.get(row), viewColumns.without(uncheckedColumns));

                Set<Integer> uncheckedRows = new HashSet<>();
                subject.iterateUncheckedRows(column).forEach((r) -> uncheckedRows.add(r));
                Assert.assertEquals(uncheckedRows.toString(), viewReferenceCR.get(column), viewRows.without(uncheckedRows));

                for (String c : checkedColumns) {
                    Assert.assertTrue(view.isChecked(row, c));
                }

                for (Integer r : checkedRows) {
                    Assert.assertTrue(view.isChecked(r, column));
                }
            }
        }

        int toUncheck = random.nextInt(viewCheckCount);

        for (int i = 0; i < toUncheck; i++) {
            Integer rowToUncheck = viewRowsList.get(random.nextInt(viewRowsList.size()));
            List<String> checkedColumnsList = new ArrayList<String>();
            view.iterateCheckedColumns(rowToUncheck).forEach(checkedColumnsList::add);

            if (checkedColumnsList.size() == 0) {
                continue;
            }

            String columnToUncheck = checkedColumnsList.get(random.nextInt(checkedColumnsList.size()));

            view.uncheck(rowToUncheck, columnToUncheck);
            referenceCR.get(columnToUncheck).remove(rowToUncheck);
            referenceRC.get(rowToUncheck).remove(columnToUncheck);
            viewReferenceCR.get(columnToUncheck).remove(rowToUncheck);
            viewReferenceRC.get(rowToUncheck).remove(columnToUncheck);
            checkCount--;
            viewCheckCount--;

            Set<String> checkedColumns = new HashSet<>();
            view.iterateCheckedColumns(rowToUncheck).forEach((c) -> checkedColumns.add(c));
            Assert.assertEquals(viewReferenceRC.get(rowToUncheck), checkedColumns);

            Set<Integer> checkedRows = new HashSet<>();
            view.iterateCheckedRows(columnToUncheck).forEach((r) -> checkedRows.add(r));
            Assert.assertEquals(viewReferenceCR.get(columnToUncheck), checkedRows);

            Set<Integer> uncheckedRows = new HashSet<>();
            view.iterateUncheckedRows(columnToUncheck).forEach((r) -> uncheckedRows.add(r));
            Set<Integer> referenceUncheckedRows = new HashSet<>(viewRows);
            referenceUncheckedRows.removeAll(viewReferenceCR.get(columnToUncheck));
            Assert.assertEquals(referenceUncheckedRows, uncheckedRows);

            
            Assert.assertTrue(view.getIncompleteRows().toString(), !view.getIncompleteRows().containsAny(view.getCompleteRows()));
            Assert.assertTrue(view.getIncompleteColumns().toString(), !view.getIncompleteColumns().containsAny(view.getCompleteColumns()));
            
        }

    }

    public CheckTableRandomizedTest(Integer seed, int rowCount, int columnCount) {
        this.seed = seed;
        this.rowCount = rowCount;
        this.columnCount = columnCount;
    }

    private static ImmutableSet<String> createStringElements(Random random, int size) {

        ImmutableSet.Builder<String> result = new ImmutableSet.Builder<>(size);

        for (int i = 0; i < size; i++) {
            result.add("e_" + i + "_" + random.nextInt(10));
        }

        return result.build();
    }

    private static ImmutableSet<Integer> createIntegerElements(Random random, int size) {

        ImmutableSet.Builder<Integer> result = new ImmutableSet.Builder<>(size);

        for (int i = 0; i < size; i++) {
            result.add(random.nextInt(10000));
        }

        return result.build();
    }

    private static int randomSize(Random random) {
        float f = random.nextFloat();

        if (f < 0.02) {
            return 1;
        } else if (f < 0.1) {
            return 2;
        } else if (f < 0.2) {
            return random.nextInt(10) + 1;
        } else if (f < 0.7) {
            return random.nextInt(20) + 11;
        } else if (f < 0.95) {
            return random.nextInt(200) + 21;
        } else {
            return random.nextInt(400) + 201;
        }
    }

}
