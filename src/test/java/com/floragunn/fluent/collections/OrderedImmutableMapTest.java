/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class OrderedImmutableMapTest {
    @Test
    public void assertType_oneElement() {
        OrderedImmutableMap<Object, Object> subject = new OrderedImmutableMapImpl.SingleElementMap<>(1, 1);
        OrderedImmutableMap<Integer, Integer> asserted = subject.assertElementType(Integer.class, Integer.class);
        Assert.assertEquals(subject, asserted);
        OrderedImmutableMap<Integer, Object> asserted2 = subject.assertElementType(Integer.class, Object.class);
        Assert.assertEquals(subject, asserted2);

        try {
            subject.assertElementType(String.class, Integer.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Key 1 is not compatible"));
        }

        try {
            subject.assertElementType(Integer.class, String.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Value 1 is not compatible"));
        }
    }

    @Test
    public void assertType_twoElement() {
        OrderedImmutableMap<Object, Object> subject = new OrderedImmutableMapImpl.TwoElementMap<>(1, 1, 2, 2);
        OrderedImmutableMap<Integer, Integer> asserted = subject.assertElementType(Integer.class, Integer.class);
        Assert.assertEquals(subject, asserted);
        OrderedImmutableMap<Integer, Object> asserted2 = subject.assertElementType(Integer.class, Object.class);
        Assert.assertEquals(subject, asserted2);

        try {
            subject.assertElementType(String.class, Integer.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Key 1 is not compatible"));
        }

        try {
            subject.assertElementType(Integer.class, String.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Value 1 is not compatible"));
        }
    }

    @Test
    public void assertType_array() {
        OrderedImmutableMap<Object, Object> subject = new OrderedImmutableMapImpl.ArrayBackedMap<>(1, 1, 2, 2, 3, 3);
        OrderedImmutableMap<Integer, Integer> asserted = subject.assertElementType(Integer.class, Integer.class);
        Assert.assertEquals(subject, asserted);
        OrderedImmutableMap<Integer, Object> asserted2 = subject.assertElementType(Integer.class, Object.class);
        Assert.assertEquals(subject, asserted2);

        try {
            subject.assertElementType(String.class, Integer.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Key 1 is not compatible"));
        }

        try {
            subject.assertElementType(Integer.class, String.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Value 1 is not compatible"));
        }
    }

    @Test
    public void assertType_hash() {
        OrderedImmutableMap<Object, Object> subject = new OrderedImmutableMap.Builder<>(15).with(1, 1).with(2, 2).with(3, 3).with(4, 4).build();
        Assert.assertTrue(subject.getClass().getName(), subject instanceof OrderedImmutableMapImpl.ArrayBackedMap);
        OrderedImmutableMap<Integer, Integer> asserted = subject.assertElementType(Integer.class, Integer.class);
        Assert.assertEquals(subject, asserted);
        OrderedImmutableMap<Integer, Object> asserted2 = subject.assertElementType(Integer.class, Object.class);
        Assert.assertEquals(subject, asserted2);

        try {
            subject.assertElementType(String.class, Integer.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Key 1 is not compatible"));
        }

        try {
            subject.assertElementType(Integer.class, String.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Value 1 is not compatible"));
        }
    }

    @Test
    public void assertType_mapBacked() {
        Map<Object, Object> delegate = new HashMap<>();
        delegate.put(1, 1);
        OrderedImmutableMap<Object, Object> subject = new OrderedImmutableMapImpl.MapBackedMap<>(delegate);
        OrderedImmutableMap<Integer, Integer> asserted = subject.assertElementType(Integer.class, Integer.class);
        Assert.assertEquals(subject, asserted);
        OrderedImmutableMap<Integer, Object> asserted2 = subject.assertElementType(Integer.class, Object.class);
        Assert.assertEquals(subject, asserted2);

        try {
            subject.assertElementType(String.class, Integer.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Key 1 is not compatible"));
        }

        try {
            subject.assertElementType(Integer.class, String.class);
            Assert.fail();
        } catch (ClassCastException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().contains("Value 1 is not compatible"));
        }
    }

    @Test
    public void ensureKeyType_oneElement() {
        OrderedImmutableMap<Object, ?> subject = new OrderedImmutableMapImpl.SingleElementMap<>(1, 1);
        OrderedImmutableMap<Integer, ?> ensured = subject.ensureKeyType(Integer.class, (x) -> 1);
        Assert.assertTrue((OrderedImmutableMap<?, ?>) subject == (OrderedImmutableMap<?, ?>) ensured);

        OrderedImmutableMap<String, ?> ensured2 = subject.ensureKeyType(String.class, (x) -> x.toString());
        Assert.assertFalse((OrderedImmutableMap<?, ?>) subject == (OrderedImmutableMap<?, ?>) ensured2);
        Assert.assertEquals("1", ensured2.keySet().any());
    }

    @Test
    public void ensureKeyType_twoElement() {
        OrderedImmutableMap<Object, ?> subject = new OrderedImmutableMapImpl.TwoElementMap<>(1, 1, 2, 2);
        OrderedImmutableMap<Integer, ?> ensured = subject.ensureKeyType(Integer.class, (x) -> 1);
        Assert.assertTrue((OrderedImmutableMap<?, ?>) subject == (OrderedImmutableMap<?, ?>) ensured);

        OrderedImmutableMap<String, ?> ensured2 = subject.ensureKeyType(String.class, (x) -> x.toString());
        Assert.assertFalse((OrderedImmutableMap<?, ?>) subject == (OrderedImmutableMap<?, ?>) ensured2);
        Assert.assertEquals(ImmutableSet.of("1", "2"), ensured2.keySet());
    }

    @Test
    public void ensureKeyType_array() {
        OrderedImmutableMap<Object, ?> subject = new OrderedImmutableMapImpl.ArrayBackedMap<>(1, 1, 2, 2, 3, 3);
        OrderedImmutableMap<Integer, ?> ensured = subject.ensureKeyType(Integer.class, (x) -> 1);
        Assert.assertTrue((OrderedImmutableMap<?, ?>) subject == (OrderedImmutableMap<?, ?>) ensured);

        OrderedImmutableMap<String, ?> ensured2 = subject.ensureKeyType(String.class, (x) -> x.toString());
        Assert.assertFalse((OrderedImmutableMap<?, ?>) subject == (OrderedImmutableMap<?, ?>) ensured2);
        Assert.assertEquals(ImmutableSet.of("1", "2", "3"), ensured2.keySet());
    }

    @Test
    public void of_mapWithOneAdditionalEntry() {
        Map<String, Integer> base = new HashMap<>();
        base.put("a", 1);

        OrderedImmutableMap<String, Integer> subject = OrderedImmutableMap.of(base, "d", 4);
        Map<String, Integer> reference = new HashMap<>(base);
        reference.put("d", 4);
        Assert.assertEquals(reference, subject);

        base.put("b", 2);
        base.put("c", 3);

        subject = OrderedImmutableMap.of(base, "d", 4);
        reference = new HashMap<>(base);
        reference.put("d", 4);

        Assert.assertEquals(reference, subject);
    }
    
    @Test
    public void of_mapWithTwoAdditionalEntries() {
        Map<String, Integer> base = new HashMap<>();
        base.put("a", 1);

        OrderedImmutableMap<String, Integer> subject = OrderedImmutableMap.of(base, "d", 4, "e", 5);
        Map<String, Integer> reference = new HashMap<>(base);
        reference.put("d", 4);
        reference.put("e", 5);
        Assert.assertEquals(reference, subject);

        base.put("b", 2);
        base.put("c", 3);

        subject = OrderedImmutableMap.of(base, "d", 4, "e", 5);
        reference = new HashMap<>(base);
        reference.put("d", 4);
        reference.put("e", 5);

        Assert.assertEquals(reference, subject);
    }
    
    @Test
    public void map_collection() {
        List<String> base = Arrays.asList("a/1", "b/2", "c/3", "d/4");
        
        OrderedImmutableMap<String, Integer> subject = OrderedImmutableMap.map(base, (s) -> {
           String [] p = s.split("/"); 
           
           return OrderedImmutableMap.entry(p[0], Integer.parseInt(p[1]));
        });
        
        Assert.assertEquals(OrderedImmutableMap.of("a", 1, "b", 2, "c", 3, "d", 4), subject);
    }
    
    @Test
    public void matching_oneElement() {
        OrderedImmutableMap<Integer, Integer> subject = new OrderedImmutableMapImpl.SingleElementMap<>(1, 1);
        Assert.assertEquals(subject, subject.matching((k, v) -> v == 1));
        Assert.assertEquals(Collections.emptyMap(), subject.matching((k, v) -> v == 0));
    }

    @Test
    public void matching_twoElements() {
        OrderedImmutableMap<Integer, Integer> subject = new OrderedImmutableMapImpl.TwoElementMap<>(1, 1, 2, 2);
        Assert.assertEquals(subject, subject.matching((k, v) -> v == 1 || v == 2));
        Assert.assertEquals(Collections.emptyMap(), subject.matching((k, v) -> v == 0));
        Assert.assertEquals(Collections.singletonMap(1, 1), subject.matching((k, v) -> v == 1));
        Assert.assertEquals(Collections.singletonMap(2, 2), subject.matching((k, v) -> v == 2));
    }

    @Test
    public void matching_without() {
        OrderedImmutableMap<Integer, Integer> subject = OrderedImmutableMap.of(1, 1, 2, 2, 9, 9).without(9);

        Assert.assertTrue(subject.getClass().toString(), subject instanceof OrderedImmutableMapImpl.WithoutMap);

        Assert.assertEquals(new HashMap<>(subject), subject.matching((k) -> k == 1 || k == 2));
        Assert.assertEquals(new HashMap<>(subject), subject.matching((k) -> k == 1 || k == 2 || k == 3));
        Assert.assertEquals(Collections.emptyMap(), subject.matching((k) -> k == 0));
        Assert.assertEquals(Collections.singletonMap(1, 1), subject.matching((k) -> k == 1));
        Assert.assertEquals(Collections.singletonMap(2, 2), subject.matching((k) -> k == 2));
    }

    @Test
    public void matching_without_BiPredicate() {
        OrderedImmutableMap<String, Integer> subject = OrderedImmutableMap.of("a", 1, "b", 2, "x", 3).without("x");

        Assert.assertTrue(subject.getClass().toString(), subject instanceof OrderedImmutableMapImpl.WithoutMap);

        Assert.assertEquals(new HashMap<>(subject), subject.matching((k, v) -> v == 1 || v == 2));
        Assert.assertEquals(new HashMap<>(subject), subject.matching((k, v) -> v == 1 || v == 2 || v == 3));
        Assert.assertEquals(Collections.emptyMap(), subject.matching((k, v) -> v == 0));
        Assert.assertEquals(Collections.singletonMap("a", 1), subject.matching((k, v) -> v == 1));
        Assert.assertEquals(Collections.singletonMap("b", 2), subject.matching((k, v) -> v == 2));
    }
}
