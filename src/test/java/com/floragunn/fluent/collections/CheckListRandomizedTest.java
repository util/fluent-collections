/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CheckListRandomizedTest {
    @Parameter
    public Integer seed;

    @Parameters(name = "{0}")
    public static Collection<Integer> seeds() {
        ArrayList<Integer> result = new ArrayList<>(10000);

        for (int i = 100; i < 1000; i++) {
            result.add(i);
        }

        return result;
    }

    @Test
    public void basicTest() {
        Random random = new Random(seed);
        ImmutableSet<String> elements = createElements(random);
        List<String> elementsList = new ArrayList<>(elements);
        Collections.shuffle(elementsList, random);

        CheckList<String> subject = CheckList.create(elements);
        Set<String> reference = new HashSet<>();
        List<ImmutableSet<String>> history = new ArrayList<>();

        for (int i = 0; i < Math.min(100, elementsList.size()); i++) {
            history.add(subject.getCheckedElements());
            String element = elementsList.get(i);

            boolean checkResult = subject.check(element);
            reference.add(element);

            boolean checkResultReference = reference.size() == elements.size();

            Assert.assertEquals(subject + " vs " + reference, checkResultReference, checkResult);
            Assert.assertEquals(reference, subject.getCheckedElements());

            Set<String> checked = new HashSet<>();
            subject.iterateCheckedElements().forEach((e) -> checked.add(e));
            Assert.assertEquals(reference, checked);

            Assert.assertTrue(subject.getUncheckedElements().toString(), !subject.getUncheckedElements().containsAny(subject.getCheckedElements()));
            Assert.assertEquals(elements.size(), subject.getCheckedElements().size() + subject.getUncheckedElements().size());

            Set<String> unchecked = new HashSet<>();
            subject.iterateUncheckedElements().forEach((e) -> unchecked.add(e));
            Assert.assertEquals(subject.getUncheckedElements(), unchecked);
            
            for (String e : checked) {
                Assert.assertTrue(subject.isChecked(e));
            }

            for (String e : unchecked) {
                Assert.assertFalse(subject.isChecked(e));
            }
        }

        List<String> checkedElements = new ArrayList<>(reference);
        Collections.shuffle(checkedElements, random);

        int toUncheck = random.nextInt(checkedElements.size());

        for (int i = 0; i < toUncheck; i++) {
            history.add(subject.getCheckedElements());
            String element = checkedElements.get(i);

            subject.uncheck(element);
            reference.remove(element);

            Assert.assertEquals(subject.getCheckedElements(), reference);

            Set<String> checked = new HashSet<>();
            subject.iterateCheckedElements().forEach((e) -> checked.add(e));
            Assert.assertEquals(reference, checked);

            Assert.assertTrue(subject.getUncheckedElements().toString(), !subject.getUncheckedElements().containsAny(subject.getCheckedElements()));
            Assert.assertEquals(elements.size(), subject.getCheckedElements().size() + subject.getUncheckedElements().size());
        }

        for (int i = 90; i < Math.min(200, elementsList.size()); i++) {
            history.add(subject.getCheckedElements());
            String element = elementsList.get(i);

            boolean checkResult = subject.check(element);
            reference.add(element);

            boolean checkResultReference = reference.size() == elements.size();

            Assert.assertEquals(subject + " vs " + reference, checkResultReference, checkResult);
            Assert.assertEquals(subject.getCheckedElements(), reference);

            Set<String> checked = new HashSet<>();
            subject.iterateCheckedElements().forEach((e) -> checked.add(e));
            Assert.assertEquals(reference, checked);

            Assert.assertTrue(subject.getUncheckedElements().toString(), !subject.getUncheckedElements().containsAny(subject.getCheckedElements()));
            Assert.assertEquals(elements.size(), subject.getCheckedElements().size() + subject.getUncheckedElements().size());
        }

        checkedElements = new ArrayList<>(reference);
        Collections.shuffle(checkedElements, random);

        toUncheck = random.nextInt(checkedElements.size());

        for (int i = 0; i < toUncheck; i++) {
            history.add(subject.getCheckedElements());
            String element = checkedElements.get(i);

            subject.uncheck(element);
            reference.remove(element);

            Assert.assertEquals(reference, subject.getCheckedElements());

            Set<String> checked = new HashSet<>();
            subject.iterateCheckedElements().forEach((e) -> checked.add(e));
            Assert.assertEquals(reference, checked);
        }

        for (int i = 0; i < elementsList.size(); i++) {
            history.add(subject.getCheckedElements());
            String element = elementsList.get(i);

            boolean checkResult = subject.check(element);
            reference.add(element);

            boolean checkResultReference = reference.size() == elements.size();

            Assert.assertEquals(subject + " vs " + reference, checkResultReference, checkResult);
            Assert.assertEquals(subject.getCheckedElements(), reference);
        }
    }

    private static ImmutableSet<String> createElements(Random random) {
        float f = random.nextFloat();
        int size;

        if (f < 0.02) {
            size = 1;
        } else if (f < 0.1) {
            size = 2;
        } else if (f < 0.2) {
            size = random.nextInt(10) + 1;
        } else if (f < 0.4) {
            size = random.nextInt(20) + 11;
        } else if (f < 0.9) {
            size = random.nextInt(200) + 21;
        } else {
            size = random.nextInt(800) + 201;
        }

        ImmutableSet.Builder<String> result = new ImmutableSet.Builder<>(size);

        for (int i = 0; i < size; i++) {
            result.add("e_" + i + "_" + random.nextInt(10));
        }

        return result.build();
    }
}
