/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * A map which cannot be changed by public methods. In contrast to immutable maps, background processes might still change its contents.
 */
public interface UnmodifiableMap<K, V> extends Map<K, V> {

    public static <K, V> UnmodifiableMap<K, V> of(Map<K, V> map) {
        if (map instanceof UnmodifiableMap) {
            return (UnmodifiableMap<K, V>) map;
        }

        return new UnmodifiableMap<K, V>() {

            @Override
            public int size() {
                return map.size();
            }

            @Override
            public boolean isEmpty() {
                return map.isEmpty();
            }

            @Override
            public boolean containsKey(Object key) {
                return map.containsKey(key);
            }

            @Override
            public boolean containsValue(Object value) {
                return map.containsValue(value);
            }

            @Override
            public V get(Object key) {
                return map.get(key);
            }

            @Override
            public UnmodifiableSet<K> keySet() {
                return UnmodifiableSet.of(map.keySet());
            }

            @Override
            public UnmodifiableCollection<V> values() {
                return UnmodifiableCollection.of(map.values());
            }

            @Override
            public UnmodifiableSet<Entry<K, V>> entrySet() {
                return UnmodifiableSet.of(map.entrySet());
            }
        };
    }

    UnmodifiableSet<K> keySet();

    UnmodifiableSet<Entry<K, V>> entrySet();
    
    UnmodifiableCollection<V> values();

    @Override
    @Deprecated
    default V put(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default V remove(Object key) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default void putAll(Map<? extends K, ? extends V> m) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default V putIfAbsent(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default boolean remove(Object key, Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default boolean replace(K key, V oldValue, V newValue) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default V replace(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Deprecated
    default V merge(K key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        throw new UnsupportedOperationException();
    }
}
