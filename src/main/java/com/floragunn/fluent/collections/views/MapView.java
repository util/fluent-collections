/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections.views;

import java.util.Map;
import java.util.function.Function;

import com.floragunn.fluent.collections.UnmodifiableMap;

public interface MapView<K, V> extends UnmodifiableMap<K, V> {
    static <K, V, VT> MapView<K, VT> mapValues(Map<K, V> map, Function<V, VT> valueMappingFunction) {
        return MapViewImpl.mapValues(map, valueMappingFunction);
    }

    static UnmodifiableMap<String, Object> stringKeys(Map<?, ?> map) {
        return MapViewImpl.stringKeys(map);
    }
    
    static Map<String, Object> stringKeysUnsafe(Map<?, ?> map) {
        return MapViewImpl.stringKeysUnsafe(map);
    }
}
