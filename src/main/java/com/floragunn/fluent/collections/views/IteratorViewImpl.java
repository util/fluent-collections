/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections.views;

import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Predicate;

import com.floragunn.fluent.collections.UnmodifiableIterator;

public class IteratorViewImpl {
    static <E> UnmodifiableIterator<E> filter(Iterator<E> iter, Predicate<E> predicate) {
        E first = firstMatching(iter, predicate);

        if (first == null) {
            return UnmodifiableIterator.empty();
        }

        return new UnmodifiableIterator<E>() {

            E e = first;

            @Override
            public boolean hasNext() {
                return this.e != null;
            }

            @Override
            public E next() {
                E result = this.e;
                E next = null;

                while (iter.hasNext() && next == null) {
                    E candidate = iter.next();

                    if (predicate.test(candidate)) {
                        next = candidate;
                        break;
                    }
                }

                this.e = next;

                return result;
            }

        };
    }

    static <ES, ET> IteratorView<ET> map(Iterator<ES> iterator, Function<ES, ET> valueMappingFunction) {
        return new ValueRewritingIteratorView<>(iterator, valueMappingFunction);
    }

    static <E> E firstMatching(Iterator<E> iter, Predicate<E> predicate) {
        while (iter.hasNext()) {
            E e = iter.next();

            if (predicate.test(e)) {
                return e;
            }
        }

        return null;
    }
    
    static <E> IteratorView<E> concat(Iterator<E> e1, Iterator<E> e2) {
        return new IteratorView<E>() {

            @Override
            public boolean hasNext() {
                return e1.hasNext() || e2.hasNext();
            }

            @Override
            public E next() {
                if (e1.hasNext()) {
                    return e1.next();
                } else {
                    return e2.next();
                }
            }
            
        };
    }

    static class ValueRewritingIteratorView<ES, ET> implements IteratorView<ET> {

        private final Iterator<ES> source;
        private final Function<ES, ET> valueMappingFunction;

        public ValueRewritingIteratorView(Iterator<ES> source, Function<ES, ET> valueMappingFunction) {
            this.source = source;
            this.valueMappingFunction = valueMappingFunction;
        }

        @Override
        public boolean hasNext() {
            return source.hasNext();
        }

        @Override
        public ET next() {
            return valueMappingFunction.apply(source.next());
        }
    }
}
