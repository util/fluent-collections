/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections.views;

import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Predicate;

public interface IterableView<E> extends Iterable<E> {
    static <E> IterableView<E> filter(Iterable<E> iterable, Predicate<E> predicate) {

        return new IterableView<E>() {

            @Override
            public Iterator<E> iterator() {
                return IteratorView.filter(iterable.iterator(), predicate);
            }
        };
    }

    static <ES, ET> IterableView<ET> map(Iterable<ES> iterable, Function<ES, ET> valueMappingFunction) {
        return new IterableView<ET>() {

            @Override
            public Iterator<ET> iterator() {
                return IteratorView.map(iterable.iterator(), valueMappingFunction);
            }
        };
    }

    static <E> IterableView<E> concat(Iterable<E> i1, Iterable<E> i2) {
        return new IterableView<E>() {

            @Override
            public Iterator<E> iterator() {
                return IteratorView.concat(i1.iterator(), i2.iterator());
            }
        };
    }
}
