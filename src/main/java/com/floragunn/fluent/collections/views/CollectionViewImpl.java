/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections.views;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.function.Function;

import com.floragunn.fluent.collections.UnmodifiableIterator;

class CollectionViewImpl {
    static <ES, ET> CollectionView<ET> map(Collection<ES> set, Function<ES, ET> valueMappingFunction) {
        return new ValueRewritingCollectionView<>(set, valueMappingFunction);
    }

    static class ValueRewritingCollectionView<ES, ET> extends AbstractCollection<ET> implements CollectionView<ET> {

        private final Collection<ES> source;
        private final Function<ES, ET> valueMappingFunction;

        public ValueRewritingCollectionView(Collection<ES> source, Function<ES, ET> valueMappingFunction) {
            this.source = source;
            this.valueMappingFunction = valueMappingFunction;
        }

        @Override
        public int size() {
            return source.size();
        }

        @Override
        public UnmodifiableIterator<ET> iterator() {
            return IteratorViewImpl.map(source.iterator(), valueMappingFunction);
        }

        @Override
        public boolean isEmpty() {
            return source.isEmpty();
        }

    };

}
