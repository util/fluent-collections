/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections.views;

import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Predicate;

import com.floragunn.fluent.collections.UnmodifiableIterator;

public interface IteratorView<E> extends UnmodifiableIterator<E> {
    static <E> UnmodifiableIterator<E> filter(Iterator<E> iter, Predicate<E> predicate) {
        return IteratorViewImpl.filter(iter, predicate);
    }
    
    static <ES, ET> IteratorView<ET> map(Iterator<ES> iterator, Function<ES, ET> valueMappingFunction) {
        return IteratorViewImpl.map(iterator, valueMappingFunction);
    }
    
    static <E> IteratorView<E> concat(Iterator<E> e1, Iterator<E> e2) {
        return IteratorViewImpl.concat(e1, e2);
    }
}
