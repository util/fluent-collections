/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;

public interface ImmutableList<E> extends UnmodifiableList<E> {

    static <E> ImmutableList<E> empty() {
        return ImmutableListImpl.empty();
    }

    static <E> ImmutableList<E> of(E e) {
        return new ImmutableListImpl.OneElementList<E>(e);
    }

    static <E> ImmutableList<E> of(E e1, E e2) {
        return new ImmutableListImpl.TwoElementList<E>(e1, e2);
    }

    @SafeVarargs
    static <E> ImmutableList<E> of(E e1, E e2, E e3, E... more) {
        return ImmutableListImpl.of(e1, e2, e3, more);
    }

    @SafeVarargs
    static <E> ImmutableList<E> ofArray(E... more) {
        return ImmutableListImpl.ofArray(more);
    }

    static <E> ImmutableList<E> of(Collection<? extends E> collection) {
        return ImmutableListImpl.of(collection);
    }

    static <E> ImmutableList<E> ofNonNull(E e) {
        return ImmutableListImpl.ofNonNull(e);
    }

    static <E> ImmutableList<E> ofNonNull(E e1, E e2) {
        return ImmutableListImpl.ofNonNull(e1, e2);
    }

    @SafeVarargs
    static <E> ImmutableList<E> ofNonNull(E e1, E e2, E... more) {
        return ImmutableListImpl.ofNonNull(e1, e2, more);
    }

    static <E> ImmutableList<E> concat(Collection<? extends E> c1, Collection<? extends E> c2) {
        return ImmutableListImpl.concat(c1, c2);
    }

    static <E> ImmutableList<E> concat(Collection<? extends E> c1, Collection<? extends E> c2, Collection<? extends E> c3) {
        return ImmutableListImpl.concat(c1, c2, c3);
    }

    static <C, E> ImmutableList<E> map(Collection<C> collection, Function<C, E> mappingFunction) {
        return ImmutableListImpl.map(collection, mappingFunction);
    }

    static <E> Collector<E, ?, ImmutableList<E>> collector() {
        return ImmutableListImpl.collector();
    }

    ImmutableList<E> with(E other);

    ImmutableList<E> with(Collection<E> other);

    ImmutableList<E> with(Optional<E> other);

    ImmutableList<E> with(@SuppressWarnings("unchecked") E... other);

    ImmutableList<E> matching(Predicate<E> predicate);

    ImmutableList<E> without(Collection<E> other);

    <O> ImmutableList<O> map(Function<E, O> mappingFunction);

    ImmutableList<E> subList(int fromIndex, int toIndex);

    boolean forAllApplies(Predicate<E> predicate);

    boolean forAnyApplies(Predicate<E> predicate);

    E only();

    E any();

    E first();

    E last();

    String toShortString();

    public static class Builder<E> {
        private ArrayList<E> list;

        public Builder() {
            this(10);
        }

        public Builder(int expectedNumberOfElements) {
            this.list = new ArrayList<>(expectedNumberOfElements);
        }

        public Builder(Collection<E> initialContent) {
            this.list = new ArrayList<>(initialContent);
        }

        public Builder<E> add(E e) {
            list.add(e);
            return this;
        }

        public Builder<E> with(E e) {
            list.add(e);
            return this;
        }

        public Builder<E> add(Optional<E> optional) {
            if (optional.isPresent()) {
                list.add(optional.get());
            }
            return this;
        }

        public boolean addAll(Collection<E> collection) {
            return this.list.addAll(collection);
        }
        
        public Builder<E> with(Optional<E> optional) {
            if (optional.isPresent()) {
                list.add(optional.get());
            }
            return this;
        }

        public Builder<E> with(Collection<E> collection) {
            list.addAll(collection);
            return this;
        }

        public ImmutableList<E> build() {
            return of(list);
        }

        public ImmutableList<E> build(Comparator<E> sortedBy) {
            list.sort(sortedBy);
            return of(list);
        }

    }

}
