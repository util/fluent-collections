/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import com.floragunn.fluent.collections.OrderedImmutableMapImpl.InternalBuilder;

public interface OrderedImmutableMap<K, V> extends ImmutableMap<K, V> {
    static <K, V> OrderedImmutableMap<K, V> of(K k1, V v1) {
        return OrderedImmutableMapImpl.of(k1, v1);
    }

    static <K, V> OrderedImmutableMap<K, V> of(K k1, V v1, K k2, V v2) {
        return OrderedImmutableMapImpl.of(k1, v1, k2, v2);
    }

    static <K, V> OrderedImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3) {
        return OrderedImmutableMapImpl.of(k1, v1, k2, v2, k3, v3);
    }

    static <K, V> OrderedImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        return OrderedImmutableMapImpl.of(k1, v1, k2, v2, k3, v3, k4, v4);
    }

    static <K, V> OrderedImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return OrderedImmutableMapImpl.of(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5);
    }

    static <K, V> OrderedImmutableMap<K, V> ofNonNull(K k1, V v1) {
        return OrderedImmutableMapImpl.ofNonNull(k1, v1);
    }

    static <K, V> OrderedImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2) {
        return OrderedImmutableMapImpl.ofNonNull(k1, v1, k2, v2);
    }

    static <K, V> OrderedImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2, K k3, V v3) {
        return OrderedImmutableMapImpl.ofNonNull(k1, v1, k2, v2, k3, v3);
    }

    static <K, V> OrderedImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        return OrderedImmutableMapImpl.ofNonNull(k1, v1, k2, v2, k3, v3, k4, v4);
    }

    static <K, V> OrderedImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return OrderedImmutableMapImpl.ofNonNull(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5);
    }

    static <K, V> OrderedImmutableMap<K, V> of(Map<K, V> map) {
        return OrderedImmutableMapImpl.of(map);
    }

    static <K, V> OrderedImmutableMap<K, V> of(Map<K, V> map, K k1, V v1) {
        return OrderedImmutableMapImpl.of(map, k1, v1);
    }

    static <K, V> OrderedImmutableMap<K, V> of(Map<K, V> map, K k1, V v1, K k2, V v2) {
        return OrderedImmutableMapImpl.of(map, k1, v1, k2, v2);
    }

    static <K, V> OrderedImmutableMap<K, V> without(Map<K, V> map, K key) {
        return OrderedImmutableMapImpl.without(map, key);
    }

    static <C, K, V> OrderedImmutableMap<K, V> map(Collection<C> collection, Function<C, Map.Entry<K, V>> mappingFunction) {
        return OrderedImmutableMapImpl.map(collection, mappingFunction);
    }

    static <KS, VS, KT, VT> OrderedImmutableMap<KT, VT> map(Map<KS, VS> source, Function<KS, KT> keyMappingFunction,
            Function<VS, VT> valueMappingFunction) {
        return OrderedImmutableMapImpl.map(source, keyMappingFunction, valueMappingFunction);
    }

    static <K, V> Map.Entry<K, V> entry(K k1, V v1) {
        return new AbstractMap.SimpleImmutableEntry<>(k1, v1);
    }

    public static <K, V> OrderedImmutableMap<K, V> empty() {
        return OrderedImmutableMapImpl.empty();
    }

    public static <K, V> OrderedImmutableMap<K, V> ordered() {
        return OrderedImmutableMapImpl.empty();
    }

    OrderedImmutableMap<K, V> without(K key);

    OrderedImmutableMap<K, V> with(K key, V value);

    <K2 extends K, V2 extends V> OrderedImmutableMap<K, V> with(ImmutableMap<K2, V2> other);

    OrderedImmutableMap<K, V> withComputed(K key, Function<V, V> f);

    OrderedImmutableMap<K, V> matching(Predicate<K> predicate);

    OrderedImmutableMap<K, V> matching(BiPredicate<K, V> predicate);

    OrderedImmutableMap<K, V> intersection(ImmutableSet<? extends K> keys);

    <KT, VT> OrderedImmutableMap<KT, VT> map(Function<K, KT> keyMappingFunction, Function<V, VT> valueMappingFunction);

    <VT> OrderedImmutableMap<K, VT> mapValues(Function<V, VT> valueMappingFunction);

    <KT, VT> OrderedImmutableMap<KT, VT> assertElementType(Class<KT> keyType, Class<VT> valueType);

    <KT> OrderedImmutableMap<KT, V> ensureKeyType(Class<KT> keyType, Function<Object, KT> conversionFunction);
    
    static class Builder<K, V> {
        private InternalBuilder<K, V> internalBuilder;
        private Function<K, V> defaultSupplier;

        public Builder() {
            internalBuilder = new OrderedImmutableMapImpl.HashArrayBackedMap.Builder<K, V>(16);
        }

        public Builder(int expectedNumberOfElements) {
            if (expectedNumberOfElements <= 25) {
                internalBuilder = new OrderedImmutableMapImpl.HashArrayBackedMap.Builder<K, V>(16);
            } else if (expectedNumberOfElements <= 100) {
                internalBuilder = new OrderedImmutableMapImpl.HashArrayBackedMap.Builder<K, V>(64);
            } else if (expectedNumberOfElements <= 400) {
                internalBuilder = new OrderedImmutableMapImpl.HashArrayBackedMap.Builder<K, V>(256);
            } else {
                internalBuilder = new OrderedImmutableMapImpl.MapBackedMap.Builder<K, V>(expectedNumberOfElements);
            }
        }

        public Builder(Map<K, V> initialContent) {
            if (initialContent instanceof OrderedImmutableMapImpl.HashArrayBackedMap) {
                internalBuilder = new OrderedImmutableMapImpl.HashArrayBackedMap.Builder<K, V>(
                        (OrderedImmutableMapImpl.HashArrayBackedMap<K, V>) initialContent);
            } else {
                if (initialContent.size() <= 25) {
                    internalBuilder = new OrderedImmutableMapImpl.HashArrayBackedMap.Builder<K, V>(16);

                    for (Map.Entry<K, V> entry : initialContent.entrySet()) {
                        internalBuilder = internalBuilder.with(entry.getKey(), entry.getValue());
                    }
                } else if (initialContent.size() <= 100) {
                    internalBuilder = new OrderedImmutableMapImpl.HashArrayBackedMap.Builder<K, V>(64);

                    for (Map.Entry<K, V> entry : initialContent.entrySet()) {
                        internalBuilder = internalBuilder.with(entry.getKey(), entry.getValue());
                    }
                } else if (initialContent.size() <= 400) {
                    internalBuilder = new OrderedImmutableMapImpl.HashArrayBackedMap.Builder<K, V>(256);

                    for (Map.Entry<K, V> entry : initialContent.entrySet()) {
                        internalBuilder = internalBuilder.with(entry.getKey(), entry.getValue());
                    }
                } else {
                    internalBuilder = new OrderedImmutableMapImpl.MapBackedMap.Builder<K, V>(initialContent);
                }
            }
        }

        public Builder<K, V> defaultValue(Function<K, V> defaultSupplier) {
            this.defaultSupplier = defaultSupplier;
            return this;
        }

        public Builder<K, V> with(K key, V value) {
            if (key == null) {
                throw new IllegalArgumentException("null keys are not allowed");
            }
            internalBuilder = internalBuilder.with(key, value);
            return this;
        }

        public Builder<K, V> with(Map<K, V> map) {
            if (!map.isEmpty()) {
                internalBuilder = internalBuilder.with(map);
            }

            return this;
        }

        public void put(K key, V value) {
            if (key == null) {
                throw new IllegalArgumentException("null keys are not allowed");
            }

            internalBuilder = internalBuilder.with(key, value);
        }

        public void putAll(Map<K, V> map) {
            if (!map.isEmpty()) {
                internalBuilder = internalBuilder.with(map);
            }
        }

        public boolean remove(K e) {
            return internalBuilder.remove(e);
        }

        public boolean contains(K e) {
            return internalBuilder.contains(e);
        }

        public V get(K k) {
            V result = internalBuilder.get(k);

            if (result == null && defaultSupplier != null) {
                result = defaultSupplier.apply(k);
                if (result != null) {
                    put(k, result);
                }
            }

            return result;
        }

        public OrderedImmutableMap<K, V> build() {
            return (OrderedImmutableMap<K, V>) internalBuilder.build();
        }

        public <V2> OrderedImmutableMap<K, V2> build(Function<V, V2> valueMappingFunction) {
            return (OrderedImmutableMap<K, V2>) internalBuilder.build(valueMappingFunction);
        }

        public int size() {
            return internalBuilder.size();
        }

        public Set<K> keySet() {
            return internalBuilder.keySet();
        }
    }

}
