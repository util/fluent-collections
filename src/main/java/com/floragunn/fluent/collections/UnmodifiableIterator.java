/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

public interface UnmodifiableIterator<E> extends Iterator<E> {

    static <E> UnmodifiableIterator<E> of(Iterator<E> iterator) {
        if (iterator instanceof UnmodifiableIterator) {
            return (UnmodifiableIterator<E>) iterator;
        }

        return new UnmodifiableIterator<E>() {

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public E next() {
                return iterator.next();
            }
        };
    }

    static <E> UnmodifiableIterator<E> of(E element) {
        return new UnmodifiableIterator<E>() {

            private int i = 0;

            @Override
            public boolean hasNext() {
                return i < 1;
            }

            @Override
            public E next() {
                if (i == 0) {
                    i++;
                    return element;
                } else {
                    throw new NoSuchElementException();
                }
            }
        };
    }
    
    @SafeVarargs
    static <E> UnmodifiableIterator<E> of(E... elements) {
        return new UnmodifiableIterator<E>() {

            private int i = 0;

            @Override
            public boolean hasNext() {
                return i < elements.length;
            }

            @Override
            public E next() {
                if (i >= elements.length) {
                    throw new NoSuchElementException();
                }

                E element = (E) elements[i];
                i++;
                return element;
            }
        };
    }

    static <E> UnmodifiableIterator<E> empty() {
        @SuppressWarnings("unchecked")
        UnmodifiableIterator<E> result = (UnmodifiableIterator<E>) EMPTY;
        return result;
    }

    @Override
    @Deprecated
    default void remove() {
        throw new UnsupportedOperationException();
    }

    static final UnmodifiableIterator<?> EMPTY = new UnmodifiableIterator<Object>() {

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Object next() {
            throw new NoSuchElementException();
        }
    };
}
