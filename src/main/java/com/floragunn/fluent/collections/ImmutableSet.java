/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;

import com.floragunn.fluent.collections.ImmutableSetImpl.HashArrayBackedSet;
import com.floragunn.fluent.collections.ImmutableSetImpl.InternalBuilder;
import com.floragunn.fluent.collections.ImmutableSetImpl.SetBackedSet;

public interface ImmutableSet<E> extends UnmodifiableSet<E> {

    static <E> ImmutableSet<E> empty() {
        return ImmutableSetImpl.empty();
    }

    static <E> ImmutableSet<E> of(E e) {
        return ImmutableSetImpl.of(e);
    }

    static <E> ImmutableSet<E> of(E e1, E e2) {
        return ImmutableSetImpl.of(e1, e2);
    }

    static <E> ImmutableSet<E> ofNonNull(E e1, E e2) {
        return ImmutableSetImpl.ofNonNull(e1, e2);
    }

    @SafeVarargs
    static <E> ImmutableSet<E> of(E e, E... more) {
        return ImmutableSetImpl.of(e, more);
    }

    @SafeVarargs
    static <E> ImmutableSet<E> ofArray(E... array) {
        return ImmutableSetImpl.ofArray(array);
    }

    static <E> ImmutableSet<E> of(Iterable<? extends E> iterable) {
        return ImmutableSetImpl.of(iterable);
    }
    
    static <E> ImmutableSet<E> of(Collection<? extends E> collection) {
        return ImmutableSetImpl.of(collection);
    }

    static <E> ImmutableSet<E> of(Set<E> set, E other) {
        return ImmutableSetImpl.of(set, other);
    }

    static <E> ImmutableSet<E> flattenDeep(Collection<?> collection, Function<Object, E> mappingFunction) {
        return ImmutableSetImpl.flattenDeep(collection, mappingFunction);
    }

    static <C, E> ImmutableSet<E> map(Collection<C> collection, Function<C, E> mappingFunction) {
        return ImmutableSetImpl.map(collection, mappingFunction);
    }

    static <E> Collector<E, ?, ImmutableSet<E>> collector() {
        return ImmutableSetImpl.collector();
    }

    ImmutableSet<E> with(E other);

    ImmutableSet<E> with(Collection<? extends E> other);

    ImmutableSet<E> with(@SuppressWarnings("unchecked") E... other);

    ImmutableSet<E> matching(Predicate<E> predicate);

    Iterable<E> iterateMatching(Predicate<E> predicate);

    default ImmutableSet<E> withoutMatching(Predicate<E> predicate) {
        return matching(predicate.negate());
    }

    ImmutableSet<E> intersection(Set<? extends E> other);

    ImmutableSet<E> without(Collection<? extends E> other);

    ImmutableSet<E> without(E other);

    <O> ImmutableSet<O> map(Function<E, O> mappingFunction);

    <O> ImmutableSet<O> mapFlat(Function<E, Collection<O>> mappingFunction);

    ImmutableList<E> toList();

    <V> ImmutableMap<E, V> toMap(Function<E, V> mappingFunction);

    boolean forAllApplies(Predicate<E> predicate);

    boolean forAnyApplies(Predicate<E> predicate);

    boolean containsAny(Collection<? extends E> e);

    E only();

    E any();

    String toShortString();

    static class Builder<E> implements Iterable<E> {
        private InternalBuilder<E> internalBuilder;
        private final ImmutableSet<E> initialImmutableSet;

        public Builder() {
            this.initialImmutableSet = null;

            internalBuilder = new HashArrayBackedSet.Builder<E>(16);
        }

        public Builder(int expectedNumberOfElements) {
            this.initialImmutableSet = null;

            if (expectedNumberOfElements <= 16) {
                internalBuilder = new HashArrayBackedSet.Builder<E>(16);
            } else if (expectedNumberOfElements <= 64) {
                internalBuilder = new HashArrayBackedSet.Builder<E>(64);
            } else if (expectedNumberOfElements <= 256) {
                internalBuilder = new HashArrayBackedSet.Builder<E>(256);
            } else {
                internalBuilder = new SetBackedSet.Builder<E>(expectedNumberOfElements);
            }
        }

        public Builder(Collection<E> initialContent) {
            if (initialContent instanceof ImmutableSet) {
                this.initialImmutableSet = (ImmutableSet<E>) initialContent;
            } else {
                this.initialImmutableSet = null;

                initFrom(initialContent);
            }
        }

        public Builder(E[] initialContent) {
            this.initialImmutableSet = null;

            if (initialContent.length <= 16) {
                internalBuilder = new HashArrayBackedSet.Builder<E>(16);

                for (int i = 0; i < initialContent.length; i++) {
                    internalBuilder = internalBuilder.with(initialContent[i]);
                }
            } else if (initialContent.length <= 64) {
                internalBuilder = new HashArrayBackedSet.Builder<E>(64);

                for (int i = 0; i < initialContent.length; i++) {
                    internalBuilder = internalBuilder.with(initialContent[i]);
                }
            } else if (initialContent.length <= 256) {
                internalBuilder = new HashArrayBackedSet.Builder<E>(256);

                for (int i = 0; i < initialContent.length; i++) {
                    internalBuilder = internalBuilder.with(initialContent[i]);
                }
            } else {
                internalBuilder = new SetBackedSet.Builder<E>(Arrays.asList(initialContent));
            }
        }

        public Builder<E> with(E e) {
            if (internalBuilder == null) {
                initFrom(initialImmutableSet);
            }

            internalBuilder = internalBuilder.with(e);
            return this;
        }

        public Builder<E> with(Collection<E> collection) {
            if (collection.isEmpty()) {
                return this;
            }

            if (internalBuilder == null) {
                initFrom(initialImmutableSet);
            }

            internalBuilder = internalBuilder.with(collection);
            return this;
        }

        public boolean add(E e) {
            if (internalBuilder == null) {
                initFrom(initialImmutableSet);
            }

            int size = internalBuilder.size();
            internalBuilder = internalBuilder.with(e);
            return size != internalBuilder.size();
        }

        public boolean addAll(Collection<E> collection) {
            if (collection.isEmpty()) {
                return false;
            }

            if (internalBuilder == null) {
                initFrom(initialImmutableSet);
            }

            int size = internalBuilder.size();

            for (E e : collection) {
                internalBuilder = internalBuilder.with(e);
            }

            return size != internalBuilder.size();
        }

        public boolean addAll(@SuppressWarnings("unchecked") E... array) {
            if (array.length == 0) {
                return false;
            }

            if (internalBuilder == null) {
                initFrom(initialImmutableSet);
            }

            int size = internalBuilder.size();

            for (int i = 0; i < array.length; i++) {
                internalBuilder = internalBuilder.with(array[i]);
            }

            return size != internalBuilder.size();
        }

        public boolean remove(E e) {
            if (internalBuilder == null) {
                initFrom(initialImmutableSet);
            }

            return internalBuilder.remove(e);
        }

        public void clear() {
            if (internalBuilder == null) {
                internalBuilder = new HashArrayBackedSet.Builder<E>(16);
            } else {
                internalBuilder.clear();
            }
        }

        public boolean contains(E e) {
            if (internalBuilder == null) {
                return initialImmutableSet.contains(e);
            } else {
                return internalBuilder.contains(e);
            }
        }

        public boolean containsAny(Set<E> set) {
            if (internalBuilder == null) {
                return initialImmutableSet.containsAny(set);
            } else {
                return internalBuilder.containsAny(set);
            }
        }

        public boolean containsAll(Set<E> set) {
            if (internalBuilder == null) {
                return initialImmutableSet.containsAll(set);
            } else {
                return internalBuilder.containsAll(set);
            }
        }

        public ImmutableSet<E> build() {
            if (internalBuilder == null) {
                return initialImmutableSet;
            } else {
                return internalBuilder.build();
            }
        }

        public Iterator<E> iterator() {
            if (internalBuilder == null) {
                initFrom(initialImmutableSet);
            }

            return internalBuilder.iterator();
        }

        public int size() {
            if (internalBuilder == null) {
                return initialImmutableSet.size();
            } else {
                return internalBuilder.size();
            }
        }

        public E any() {
            if (internalBuilder == null) {
                return initialImmutableSet.any();
            } else {
                return internalBuilder.any();
            }
        }

        @Override
        public String toString() {
            if (internalBuilder == null) {
                return initialImmutableSet.toString();
            } else {
                return internalBuilder.toString();
            }
        }

        public String toDebugString() {
            return internalBuilder != null ? (internalBuilder.getClass() + " " + internalBuilder.toDebugString()) : "initial state";
        }

        private void initFrom(Collection<E> initialContent) {
            if (initialContent instanceof HashArrayBackedSet) {
                internalBuilder = new HashArrayBackedSet.Builder<E>((HashArrayBackedSet<E>) initialContent);
            } else {
                if (initialContent.size() <= 16) {
                    internalBuilder = new HashArrayBackedSet.Builder<E>(16);

                    for (E e : initialContent) {
                        internalBuilder = internalBuilder.with(e);
                    }
                } else if (initialContent.size() <= 64) {
                    internalBuilder = new HashArrayBackedSet.Builder<E>(64);

                    for (E e : initialContent) {
                        internalBuilder = internalBuilder.with(e);
                    }
                } else if (initialContent.size() <= 256) {
                    internalBuilder = new HashArrayBackedSet.Builder<E>(256);

                    for (E e : initialContent) {
                        internalBuilder = internalBuilder.with(e);
                    }
                } else {
                    internalBuilder = new SetBackedSet.Builder<E>(initialContent);
                }
            }
        }
    }

}
