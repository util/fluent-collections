/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.floragunn.fluent.collections.ImmutableSetImpl.HashArrayBackedSet;

class ImmutableMapImpl {

    static <K, V> ImmutableMap<K, V> of(Map<? extends K, ? extends V> map) {
        
        @SuppressWarnings("unchecked") // This is safe because we are never going to modify the map
        Map<K, V> upcastedMap = (Map<K, V>) map; 
        int size = map.size();

        if (map instanceof ImmutableMap) {
            return (ImmutableMap<K, V>) upcastedMap;
        } else if (size == 0) {
            return empty();
        } else if (size == 1) {
            Map.Entry<K, V> entry = upcastedMap.entrySet().iterator().next();
            return new SingleElementMap<>(entry.getKey(), entry.getValue());
        } else if (size == 2) {
            Iterator<Map.Entry<K, V>> iter = upcastedMap.entrySet().iterator();
            Map.Entry<K, V> entry1 = iter.next();
            Map.Entry<K, V> entry2 = iter.next();
            return new TwoElementMap<>(entry1.getKey(), entry1.getValue(), entry2.getKey(), entry2.getValue());
        } else if (size < 8) {
            return new ArrayBackedMap<>(upcastedMap);
        } else if (size < 400) {
            return new ImmutableMap.Builder<>(upcastedMap).build();
        } else {
            return new MapBackedMap<K, V>(new LinkedHashMap<K, V>(map));
        }
    }

    static <K, V> ImmutableMap<K, V> of(K k1, V v1) {
        return new SingleElementMap<>(k1, v1);
    }

    static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2) {
        if (k1.equals(k2)) {
            return new SingleElementMap<>(k1, v1);
        } else {
            return new TwoElementMap<>(k1, v1, k2, v2);
        }
    }

    static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3) {
        if (k1.equals(k2)) {
            if (k2.equals(k3)) {
                return new SingleElementMap<>(k1, v1);
            } else {
                return new TwoElementMap<>(k1, v1, k3, v3);
            }
        } else if (k2.equals(k3)) {
            // k1 != k2
            return new TwoElementMap<>(k1, v1, k2, v2);
        } else if (k1.equals(k3)) {
            // k1 != k2
            return new TwoElementMap<>(k1, v1, k2, v2);
        } else {
            return new ArrayBackedMap<>(k1, v1, k2, v2, k3, v3);
        }
    }

    static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        if (k4.equals(k3)) {
            return of(k1, v1, k2, v2, k4, v4);
        } else if (k4.equals(k2)) {
            return of(k1, v1, k3, v3, k4, v4);
        } else if (k4.equals(k1)) {
            return of(k2, v2, k3, v3, k4, v4);
        }

        if (k3.equals(k2)) {
            return of(k1, v1, k3, v3, k4, v4);
        } else if (k3.equals(k1)) {
            return of(k2, v2, k3, v3, k4, v4);
        }

        if (k2.equals(k1)) {
            return of(k2, v2, k3, v3, k4, v4);
        }

        return new ArrayBackedMap<>(k1, v1, k2, v2, k3, v3, k4, v4);
    }

    static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        if (k5.equals(k4)) {
            return of(k1, v1, k2, v2, k3, v3, k5, v5);
        } else if (k5.equals(k3)) {
            return of(k1, v1, k2, v2, k4, v4, k5, v5);
        } else if (k5.equals(k2)) {
            return of(k1, v1, k3, v3, k4, v4, k5, v5);
        } else if (k5.equals(k1)) {
            return of(k2, v2, k3, v3, k4, v4, k5, v5);
        }

        if (k4.equals(k3)) {
            return of(k1, v1, k2, v2, k4, v4, k5, v5);
        } else if (k4.equals(k2)) {
            return of(k1, v1, k3, v3, k4, v4, k5, v5);
        } else if (k4.equals(k1)) {
            return of(k2, v2, k3, v3, k4, v4, k5, v5);
        }

        if (k3.equals(k2)) {
            return of(k1, v1, k3, v3, k4, v4, k5, v5);
        } else if (k3.equals(k1)) {
            return of(k2, v2, k3, v3, k4, v4, k5, v5);
        }

        if (k2.equals(k1)) {
            return of(k2, v2, k3, v3, k4, v4, k5, v5);
        }

        return new ArrayBackedMap<>(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5);
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1) {
        if (k1 != null && v1 != null) {
            return of(k1, v1);
        } else {
            return empty();
        }
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2) {
        if (v1 != null && k1 != null && v2 != null && k2 != null) {
            return of(k1, v1, k2, v2);
        } else if (k1 != null && v1 != null) {
            return of(k1, v1);
        } else if (k2 != null && v2 != null) {
            return of(k2, v2);
        } else {
            return empty();
        }
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2, K k3, V v3) {
        if (k3 == null || v3 == null) {
            return ofNonNull(k1, v1, k2, v2);
        }

        if (k1 != null && v1 != null) {
            if (k2 != null && v2 != null) {
                return of(k1, v1, k2, v2, k3, v3);
            } else {
                return of(k1, v1, k3, v3);
            }
        } else {
            if (k2 != null && v2 != null) {
                return of(k2, v2, k3, v3);
            } else {
                return of(k3, v3);

            }
        }
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        if (k4 == null || v4 == null) {
            return ofNonNull(k1, v1, k2, v2, k3, v3);
        }

        if (k1 != null && v1 != null) {
            if (k2 != null && v2 != null) {
                if (k3 != null && v3 != null) {
                    return of(k1, v1, k2, v2, k3, v3, k4, v4);
                } else {
                    return of(k1, v1, k2, v2, k4, v4);
                }
            } else {
                if (k3 != null && v3 != null) {
                    return of(k1, v1, k3, v3, k4, v4);
                } else {
                    return of(k1, v1, k4, v4);
                }
            }
        } else {
            if (k2 != null && v2 != null) {
                if (k3 != null && v3 != null) {
                    return of(k2, v2, k3, v3, k4, v4);
                } else {
                    return of(k2, v2, k4, v4);
                }
            } else {
                if (k3 != null && v3 != null) {
                    return of(k3, v3, k4, v4);
                } else {
                    return of(k4, v4);
                }
            }
        }
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        if (k5 == null || v5 == null) {
            return ofNonNull(k1, v1, k2, v2, k3, v3, k4, v4);
        }

        if (k1 != null && v1 != null) {
            if (k2 != null && v2 != null) {
                if (k3 != null && v3 != null) {
                    if (k4 != null && v4 != null) {
                        return of(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5);
                    } else {
                        return of(k1, v1, k2, v2, k3, v3, k5, v5);
                    }
                } else {
                    if (k4 != null && v4 != null) {
                        return of(k1, v1, k2, v2, k4, v4, k5, v5);
                    } else {
                        return of(k1, v1, k2, v2, k5, v5);
                    }
                }
            } else {
                if (k3 != null && v3 != null) {
                    if (k4 != null && v4 != null) {
                        return of(k1, v1, k3, v3, k4, v4, k5, v5);
                    } else {
                        return of(k1, v1, k3, v3, k5, v5);
                    }
                } else {
                    if (k4 != null && v4 != null) {
                        return of(k1, v1, k4, v4, k5, v5);
                    } else {
                        return of(k1, v1, k5, v5);
                    }
                }
            }
        } else {
            if (k2 != null && v2 != null) {
                if (k3 != null && v3 != null) {
                    if (k4 != null && v4 != null) {
                        return of(k2, v2, k3, v3, k4, v4, k5, v5);
                    } else {
                        return of(k2, v2, k3, v3, k5, v5);
                    }
                } else {
                    if (k4 != null && v4 != null) {
                        return of(k2, v2, k4, v4, k5, v5);
                    } else {
                        return of(k2, v2, k5, v5);
                    }
                }
            } else {
                if (k3 != null && v3 != null) {
                    if (k4 != null && v4 != null) {
                        return of(k3, v3, k4, v4, k5, v5);
                    } else {
                        return of(k3, v3, k5, v5);
                    }
                } else {
                    if (k4 != null && v4 != null) {
                        return of(k4, v4, k5, v5);
                    } else {
                        return of(k5, v5);
                    }
                }
            }
        }
    }

    static <K, V> ImmutableMap<K, V> of(Map<K, V> map, K k1, V v1) {
        if (map == null || map.isEmpty()) {
            return of(k1, v1);
        } else if (map.size() == 1) {
            Map.Entry<K, V> entry = map.entrySet().iterator().next();
            return of(entry.getKey(), entry.getValue(), k1, v1);
        } else {
            ImmutableMap.Builder<K, V> builder = new ImmutableMap.Builder<K, V>(map).with(k1, v1);
            return builder.build();
        }
    }

    static <K, V> ImmutableMap<K, V> of(Map<K, V> map, K k1, V v1, K k2, V v2) {
        if (map == null || map.isEmpty()) {
            return of(k1, v1, k2, v2);
        } else {
            ImmutableMap.Builder<K, V> builder = new ImmutableMap.Builder<K, V>(map).with(k1, v1).with(k2, v2);
            return builder.build();
        }
    }

    static <K, V> ImmutableMap<K, V> without(Map<K, V> map, K key) {
        if (map.containsKey(key)) {
            if (map.size() == 1) {
                return empty();
            } else {
                return new WithoutMap<K, V>(map, key);
            }
        } else {
            return ImmutableMap.of(map);
        }
    }

    static <C, K, V> ImmutableMap<K, V> map(Collection<C> collection, Function<C, Map.Entry<K, V>> mappingFunction) {
        ImmutableMap.Builder<K, V> builder = new ImmutableMap.Builder<>(collection.size());

        for (C c : collection) {
            Map.Entry<K, V> entry = mappingFunction.apply(c);

            if (entry != null) {
                builder.put(entry.getKey(), entry.getValue());
            }
        }

        return builder.build();
    }

    static <KS, VS, KT, VT> ImmutableMap<KT, VT> map(Map<KS, VS> source, Function<KS, KT> keyMappingFunction, Function<VS, VT> valueMappingFunction) {
        ImmutableMap.Builder<KT, VT> builder = new ImmutableMap.Builder<>(source.size());

        for (Map.Entry<KS, VS> entry : source.entrySet()) {
            KT newKey = keyMappingFunction.apply(entry.getKey());
            VT newValue = valueMappingFunction.apply(entry.getValue());

            if (newKey != null && newValue != null) {
                builder.put(newKey, newValue);
            }
        }

        return builder.build();
    }

    static <K, V> Map.Entry<K, V> entry(K k1, V v1) {
        return new AbstractMap.SimpleImmutableEntry<>(k1, v1);
    }

    @SuppressWarnings("unchecked")
    static <K, V> ImmutableMap<K, V> empty() {
        return (ImmutableMap<K, V>) EMPTY_MAP;
    }

    static abstract class InternalBuilder<K, V> {
        abstract InternalBuilder<K, V> with(K key, V value);

        abstract <K2 extends K, V2 extends V> InternalBuilder<K, V> with(Map<K2, V2> map);

        abstract boolean remove(K e);

        abstract boolean contains(K e);
        
        abstract V get(K k);

        abstract ImmutableMap<K, V> build();

        abstract <V2> ImmutableMap<K, V2> build(Function<V, V2> valueMappingFunction);

        abstract int size();

        abstract Set<K> keySet();
        
        boolean containsAnyKey(Iterable<K> e) {
            for (K key : e) {
                if (this.contains(key)) {
                    return true;
                }
            }
            
            return false;
        }
    }

    static class SingleElementMap<K, V> extends AbstractImmutableMap<K, V> {
        private final K key;
        private final V value;
        private ImmutableSet<K> keySet;
        private ImmutableList<V> values;
        private ImmutableSet<Entry<K, V>> entrySet;

        SingleElementMap(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public int size() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean containsValue(Object value) {
            return Objects.equals(this.value, value);
        }

        @Override
        public boolean containsKey(Object key) {
            return Objects.equals(this.key, key);

        }

        @Override
        public V get(Object key) {
            if (Objects.equals(this.key, key)) {
                return this.value;
            } else {
                return null;
            }
        }

        @Override
        public ImmutableSet<K> keySet() {
            if (keySet == null) {
                keySet = ImmutableSet.of(this.key);
            }

            return keySet;
        }

        @Override
        public ImmutableList<V> values() {
            if (values == null) {
                values = ImmutableList.of(this.value);
            }

            return values;
        }

        @Override
        public ImmutableSet<Entry<K, V>> entrySet() {
            if (entrySet == null) {
                entrySet = ImmutableSet.of(new AbstractMap.SimpleEntry<>(key, value));
            }
            return entrySet;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Map)) {
                return false;
            }

            Map<?, ?> otherMap = (Map<?, ?>) o;

            if (otherMap.size() != 1) {
                return false;
            }

            Entry<?, ?> entry = otherMap.entrySet().iterator().next();

            return Objects.equals(key, entry.getKey()) && Objects.equals(value, entry.getValue());
        }

        @Override
        public  ImmutableMap<K, V> with(K key, V value) {
            if (this.key.equals(key)) {
                if (Objects.equals(this.value, value)) {
                    return this;
                } else {
                    return new SingleElementMap<>(key, value);
                }
            } else {
                return new TwoElementMap<>(this.key, this.value, key, value);
            }
        }

        @Override
        public ImmutableMap<K, V> intersection(ImmutableSet<? extends K> keys) {
            if (keys.contains(this.key)) {
                return this;
            } else {
                return empty();
            }
        }

        @Override
        public ImmutableMap<K, V> matching(Predicate<K> predicate) {
            if (predicate.test(this.key)) {
                return this;
            } else {
                return empty();
            }
        }

        @Override
        public ImmutableMap<K, V> matching(BiPredicate<K, V> predicate) {
            if (predicate.test(this.key, this.value)) {
                return this;
            } else {
                return empty();
            }
        }

        @Override
        public void forEach(BiConsumer<? super K, ? super V> action) {
            action.accept(key, value);
        }

        @Override
        public boolean containsAny(ImmutableSet<? extends K> keys) {
            if (keys.contains(this.key)) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public ImmutableList<V> values(BiPredicate<K, V> predicate) {
            if (predicate.test(key, value)) {
                return values();
            } else {
                return ImmutableList.empty();
            }
        }

        @Override
        public ImmutableList<V> valuesForKeys(ImmutableSet<? extends K> keys) {
            if (keys.contains(key)) {
                return values();
            } else {
                return ImmutableList.empty();
            }
        }

        @Override
        public <KT, VT> ImmutableMap<KT, VT> map(Function<K, KT> keyMappingFunction, Function<V, VT> valueMappingFunction) {
            return new SingleElementMap<KT, VT>(keyMappingFunction.apply(key), valueMappingFunction.apply(value));
        }

        @Override
        public <VT> ImmutableMap<K, VT> mapValues(Function<V, VT> valueMappingFunction) {
            return new SingleElementMap<K, VT>(key, valueMappingFunction.apply(value));
        }

        @Override
        public boolean forAllKeysApplies(Predicate<K> predicate) {
            return predicate.test(key);
        }

        @Override
        public boolean forAnyKeyApplies(Predicate<K> predicate) {
            return predicate.test(key);
        }

        @Override
        public <KT, VT> ImmutableMap<KT, VT> assertElementType(Class<KT> keyType, Class<VT> valueType) {
            if (key != null && !keyType.isAssignableFrom(key.getClass())) {
                throw new ClassCastException("Key " + key + " is not compatible with expected type " + keyType);
            }

            if (value != null && !valueType.isAssignableFrom(value.getClass())) {
                throw new ClassCastException("Value " + value + " is not compatible with expected type " + valueType);
            }

            @SuppressWarnings("unchecked")
            ImmutableMap<KT, VT> result = (ImmutableMap<KT, VT>) this;
            return result;
        }

        @Override
        public <KT> ImmutableMap<KT, V> ensureKeyType(Class<KT> keyType, Function<Object, KT> conversionFunction) {
            if (keyType.isAssignableFrom(key.getClass())) {
                @SuppressWarnings("unchecked")
                ImmutableMap<KT, V> result = (ImmutableMap<KT, V>) this;
                return result;
            } else {
                return new SingleElementMap<>(conversionFunction.apply(key), value);
            }
        }
    }

    static class TwoElementMap<K, V> extends AbstractImmutableMap<K, V> {
        private final K key1;
        private final V value1;
        private final K key2;
        private final V value2;
        private ImmutableSet<K> keySet;
        private ImmutableList<V> values;
        private ImmutableSet<Entry<K, V>> entrySet;

        TwoElementMap(K key1, V value1, K key2, V value2) {
            this.key1 = key1;
            this.value1 = value1;
            this.key2 = key2;
            this.value2 = value2;
        }

        @Override
        public int size() {
            return 2;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean containsValue(Object value) {
            return Objects.equals(this.value1, value) || Objects.equals(this.value2, value);
        }

        @Override
        public boolean containsKey(Object key) {
            return Objects.equals(this.key1, key) || Objects.equals(this.key2, key);

        }

        @Override
        public V get(Object key) {
            if (Objects.equals(this.key1, key)) {
                return this.value1;
            } else if (Objects.equals(this.key2, key)) {
                return this.value2;
            } else {
                return null;
            }
        }

        @Override
        public ImmutableSet<K> keySet() {
            if (keySet == null) {
                keySet = ImmutableSet.of(this.key1, this.key2);
            }

            return keySet;
        }

        @Override
        public ImmutableList<V> values() {
            if (values == null) {
                values = ImmutableList.of(this.value1, this.value2);
            }

            return values;
        }

        @Override
        public ImmutableSet<Entry<K, V>> entrySet() {
            if (entrySet == null) {
                entrySet = ImmutableSet.of(new AbstractMap.SimpleEntry<>(key1, value1), new AbstractMap.SimpleEntry<>(key2, value2));
            }
            return entrySet;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Map)) {
                return false;
            }

            Map<?, ?> otherMap = (Map<?, ?>) o;

            if (otherMap.size() != 2) {
                return false;
            }

            return Objects.equals(value1, otherMap.get(key1)) && Objects.equals(value2, otherMap.get(key2));
        }

        @Override
        public ImmutableMap<K, V> with(K key, V value) {
            if (Objects.equals(this.key1, key)) {
                if (Objects.equals(this.value1, value)) {
                    return this;
                } else {
                    return new TwoElementMap<>(key1, value, key2, value2);
                }
            } else if (Objects.equals(this.key2, key)) {
                if (Objects.equals(this.value2, value)) {
                    return this;
                } else {
                    return new TwoElementMap<>(key1, value1, key2, value);
                }
            } else {
                return new ArrayBackedMap<>(key1, value1, key2, value2, key, value);
            }
        }

        @Override
        public ImmutableMap<K, V> intersection(ImmutableSet<? extends K> keys) {
            if (keys.contains(key1)) {
                if (keys.contains(key2)) {
                    return this;
                } else {
                    return new SingleElementMap<>(key1, value1);
                }
            } else if (keys.contains(key2)) {
                return new SingleElementMap<>(key2, value2);
            } else {
                return empty();
            }
        }

        @Override
        public ImmutableMap<K, V> matching(Predicate<K> predicate) {
            if (predicate.test(key1)) {
                if (predicate.test(key2)) {
                    return this;
                } else {
                    return new SingleElementMap<>(key1, value1);
                }
            } else if (predicate.test(key2)) {
                return new SingleElementMap<>(key2, value2);
            } else {
                return empty();
            }
        }

        @Override
        public ImmutableMap<K, V> matching(BiPredicate<K, V> predicate) {
            if (predicate.test(key1, value1)) {
                if (predicate.test(key2, value2)) {
                    return this;
                } else {
                    return new SingleElementMap<>(key1, value1);
                }
            } else if (predicate.test(key2, value2)) {
                return new SingleElementMap<>(key2, value2);
            } else {
                return empty();
            }
        }
        @Override
        public void forEach(BiConsumer<? super K, ? super V> action) {
            action.accept(key1, value1);
            action.accept(key2, value2);
        }

        @Override
        public boolean containsAny(ImmutableSet<? extends K> keys) {
            if (keys.contains(key1) || keys.contains(key2)) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public ImmutableList<V> values(BiPredicate<K, V> predicate) {
            if (predicate.test(key1, value1)) {
                if (predicate.test(key2, value2)) {
                    return values();
                } else {
                    return ImmutableList.of(value1);
                }
            } else {
                if (predicate.test(key2, value2)) {
                    return ImmutableList.of(value2);
                } else {
                    return ImmutableList.empty();
                }
            }
        }

        @Override
        public ImmutableList<V> valuesForKeys(ImmutableSet<? extends K> keys) {
            if (keys.contains(key1)) {
                if (keys.contains(key2)) {
                    return values();
                } else {
                    return ImmutableList.of(value1);
                }
            } else {
                if (keys.contains(key2)) {
                    return ImmutableList.of(value2);
                } else {
                    return ImmutableList.empty();
                }

            }
        }

        @Override
        public <VT> ImmutableMap<K, VT> mapValues(Function<V, VT> valueMappingFunction) {
            return new TwoElementMap<K, VT>(key1, valueMappingFunction.apply(value1), key2, valueMappingFunction.apply(value2));
        }

        @Override
        public boolean forAllKeysApplies(Predicate<K> predicate) {
            return predicate.test(key1) && predicate.test(key2);
        }

        @Override
        public boolean forAnyKeyApplies(Predicate<K> predicate) {
            return predicate.test(key1) || predicate.test(key2);
        }

        @Override
        public <KT, VT> ImmutableMap<KT, VT> assertElementType(Class<KT> keyType, Class<VT> valueType) {
            if (!keyType.isAssignableFrom(key1.getClass())) {
                throw new ClassCastException("Key " + key1 + " is not compatible with expected type " + keyType);
            }

            if (!keyType.isAssignableFrom(key2.getClass())) {
                throw new ClassCastException("Key " + key2 + " is not compatible with expected type " + keyType);
            }

            if (value1 != null && !valueType.isAssignableFrom(value1.getClass())) {
                throw new ClassCastException("Value " + value1 + " is not compatible with expected type " + valueType);
            }

            if (value2 != null && !valueType.isAssignableFrom(value2.getClass())) {
                throw new ClassCastException("Value " + value2 + " is not compatible with expected type " + valueType);
            }

            @SuppressWarnings("unchecked")
            ImmutableMap<KT, VT> result = (ImmutableMap<KT, VT>) this;
            return result;
        }

        @Override
        public <KT> ImmutableMap<KT, V> ensureKeyType(Class<KT> keyType, Function<Object, KT> conversionFunction) {
            if (keyType.isAssignableFrom(key1.getClass()) && keyType.isAssignableFrom(key2.getClass())) {
                @SuppressWarnings("unchecked")
                ImmutableMap<KT, V> result = (ImmutableMap<KT, V>) this;
                return result;
            } else {
                return new TwoElementMap<>(conversionFunction.apply(key1), value1, conversionFunction.apply(key2), value2);
            }
        }
    }

    static class HashArrayBackedMap<K, V> extends AbstractImmutableMap<K, V> {

        private static final int COLLISION_HEAD_ROOM = 4;
        private static final int NO_SPACE = Integer.MAX_VALUE;

        final int tableSize;
        final int size;

        private final K[] table1;
        private final V[] values1;
        private final K[] table2;
        private final V[] values2;

        private Entry<K, V>[] flatEntries;
        private ImmutableSet<K> keySet;
        private ImmutableList<V> values;
        private ImmutableSet<Entry<K, V>> entrySet;

        HashArrayBackedMap(int tableSize, int size, K[] table1, V[] values1, K[] table2, V[] values2) {
            this.tableSize = tableSize;
            this.size = size;
            this.table1 = table1;
            this.values1 = values1;
            this.table2 = table2;
            this.values2 = values2;
        }

        HashArrayBackedMap(int tableSize, int size, K[] table1, V[] values1, K[] table2, V[] values2, Entry<K, V>[] flatEntries) {
            this.tableSize = tableSize;
            this.size = size;
            this.table1 = table1;
            this.values1 = values1;
            this.table2 = table2;
            this.values2 = values2;
            this.flatEntries = flatEntries;
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean isEmpty() {
            return size == 0;
        }

        @Override
        public boolean containsKey(Object key) {
            return containsKey(key, hashPosition(key));
        }

        boolean containsKey(Object key, int pos) {
            if (key.equals(this.table1[pos])) {
                return true;
            } else if (this.table2 != null && checkTable2(key, pos) < 0) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public V get(Object key) {
            int pos = hashPosition(key);

            if (key.equals(this.table1[pos])) {
                return this.values1[pos];
            } else if (this.table2 != null) {
                int check = checkTable2(key, pos);

                if (check < 0) {
                    int actualPos = -check - 1;
                    return this.values2[actualPos];
                } else {
                    return null;
                }

            } else {
                return null;
            }
        }

        @Override
        public ImmutableSet<K> keySet() {
            ImmutableSet<K> result = this.keySet;

            if (result == null) {
                this.keySet = result = new ImmutableSetImpl.HashArrayBackedSet<K>(tableSize, size, (K[]) table1, (K[]) table2);
            }

            return result;
        }

        @Override
        public ImmutableList<V> values() {
            ImmutableList<V> result = this.values;

            if (result == null) {
                V[] array = createVArray(size);
                int count = 0;

                for (int i = 0; i < tableSize; i++) {
                    if (this.table1[i] != null) {
                        array[count] = this.values1[i];
                        count++;
                    }
                }

                if (this.table2 != null) {
                    for (int i = 0; i < this.table2.length; i++) {
                        if (this.table2[i] != null) {
                            array[count] = this.values2[i];
                            count++;
                        }
                    }
                }

                this.values = result = new ImmutableListImpl.ArrayBackedList<>(array);
            }

            return result;
        }

        @Override
        public ImmutableSet<Entry<K, V>> entrySet() {
            ImmutableSet<Entry<K, V>> result = this.entrySet;

            if (result == null) {
                this.entrySet = result = new ImmutableSetImpl.ArrayBackedSet<Entry<K, V>>(getFlatEntries());
            }

            return result;
        }

        @Override
        public void forEach(BiConsumer<? super K, ? super V> action) {
            for (int i = 0; i < table1.length; i++) {
                K key = table1[i];

                if (key != null) {
                    action.accept(key, values1[i]);
                }
            }

            if (table2 != null) {
                for (int i = 0; i < table2.length; i++) {
                    K key = table2[i];

                    if (key != null) {
                        action.accept(key, values2[i]);
                    }
                }
            }
        }

        @Override
        public ImmutableMap<K, V> with(K otherKey, V otherValue) {

            int pos = hashPosition(otherKey);

            if (this.table1[pos] != null) {
                if (otherKey.equals(this.table1[pos])) {
                    // already contained

                    if (Objects.equals(this.values1[pos], otherValue)) {
                        return this;
                    } else {
                        V[] newValues1 = this.values1.clone();
                        newValues1[pos] = otherValue;
                        return new HashArrayBackedMap<K, V>(tableSize, size, this.table1, newValues1, this.table2, this.values2);
                    }
                } else if (this.table2 != null) {
                    if (this.table2[pos] != null) {
                        int check = checkTable2(otherKey, pos);

                        if (check < 0) {
                            // already contained

                            int actualPos = -check - 1;

                            if (Objects.equals(this.values2[actualPos], otherValue)) {
                                return this;
                            } else {
                                V[] newValues2 = this.values2.clone();
                                newValues2[actualPos] = otherValue;
                                return new HashArrayBackedMap<K, V>(tableSize, size, this.table1, this.values1, this.table2, newValues2);
                            }
                        } else if (check == NO_SPACE) {
                            return new WithMap<>(this, otherKey, otherValue);
                        } else {
                            K[] newTable2 = this.table2.clone();
                            V[] newValues2 = this.values2.clone();
                            newTable2[check] = otherKey;
                            newValues2[check] = otherValue;
                            return new HashArrayBackedMap<K, V>(tableSize, size + 1, this.table1, this.values1, newTable2, newValues2);
                        }
                    } else {
                        K[] newTable2 = this.table2.clone();
                        V[] newValues2 = this.values2.clone();
                        newTable2[pos] = otherKey;
                        newValues2[pos] = otherValue;
                        return new HashArrayBackedMap<K, V>(tableSize, size + 1, this.table1, this.values1, newTable2, newValues2);
                    }
                } else {
                    K[] newTable2 = createTable2();
                    V[] newValues2 = createValues2();

                    newTable2[pos] = otherKey;
                    newValues2[pos] = otherValue;
                    return new HashArrayBackedMap<>(tableSize, size + 1, this.table1, this.values1, newTable2, newValues2);
                }
            } else {
                K[] newTable1 = this.table1.clone();
                V[] newValues1 = this.values1.clone();
                newTable1[pos] = otherKey;
                newValues1[pos] = otherValue;
                return new HashArrayBackedMap<K, V>(tableSize, size + 1, newTable1, newValues1, this.table2, this.values2);
            }
        }

        @Override
        public <K2 extends K, V2 extends V> ImmutableMap<K, V> with(ImmutableMap<K2, V2> other) {
            int otherSize = other.size();

            if (otherSize == 0) {
                return this;
            } else if (otherSize == 1) {
                Map.Entry<K2, V2> otherOnly = other.entrySet().iterator().next();
                return this.with(otherOnly.getKey(), otherOnly.getValue());
            } else {
                return new HashArrayBackedMap.Builder<K, V>(this).with(other).build();
            }
        }

        @Override
        public ImmutableMap<K, V> matching(Predicate<K> predicate) {
           return matching((k, v) -> predicate.test(k));
        }
        
        @Override
        public ImmutableMap<K, V> matching(BiPredicate<K, V> predicate) {
            int table1count = 0;
            int table2count = 0;

            K[] newTable1 = createTable1();
            V[] newValues1 = createValues1();
            K[] newTable2 = table2 != null ? createTable2() : null;
            V[] newValues2 = values2 != null ? createValues2() : null;

            for (int i = 0; i < tableSize; i++) {
                K k = this.table1[i];

                if (k != null) {
                    if (predicate.test(k, this.values1[i])) {
                        newTable1[i] = k;
                        newValues1[i] = this.values1[i];
                        table1count++;
                    }
                }
            }

            int count = table1count;

            if (this.table2 != null) {
                for (int i = 0; i < this.table2.length; i++) {
                    K key = this.table2[i];

                    if (key != null) {
                        if (predicate.test(key, this.values2[i])) {
                            int pos = i == 0 ? 0 : hashPosition(key);

                            if (newTable1[pos] == null) {
                                newTable1[pos] = key;
                                newValues1[pos] = this.values2[i];
                                table1count++;
                            } else {
                                for (int k = pos;; k++) {
                                    if (newTable2[k] == null) {
                                        newTable2[k] = key;
                                        newValues2[k] = this.values2[i];
                                        table2count++;
                                        break;
                                    }
                                }
                            }

                            count++;
                        }
                    }
                }
            }

            if (count == 0) {
                return empty();
            } else if (count == 1) {
                int pos1 = findIndexOfNextNonNull(newTable1, 0);
                return new SingleElementMap<K, V>(newTable1[pos1], newValues1[pos1]);
            } else if (count == 2) {
                int pos1 = findIndexOfNextNonNull(newTable1, 0);
                int pos2 = findIndexOfNextNonNull(newTable1, pos1 + 1);

                if (pos2 != -1) {
                    return new TwoElementMap<K, V>(newTable1[pos1], newValues1[pos1], newTable1[pos2], newValues1[pos2]);
                } else {
                    pos2 = findIndexOfNextNonNull(newTable2, 0);
                    return new TwoElementMap<K, V>(newTable1[pos1], newValues1[pos1], newTable2[pos2], newValues2[pos2]);
                }
            } else if (count < size) {
                if (table2count == 0) {
                    return new HashArrayBackedMap<K, V>(tableSize, count, newTable1, newValues1, null, null);
                } else {
                    return new HashArrayBackedMap<K, V>(tableSize, count, newTable1, newValues1, newTable2, newValues2);
                }
            } else {
                return this;
            }
        }

        @Override
        public ImmutableMap<K, V> intersection(ImmutableSet<? extends K> keys) {
            int otherSize = keys.size();

            if (otherSize == 0) {
                return empty();
            } else if (otherSize == 1) {
                K key = keys.only();

                if (containsKey(key)) {
                    return new SingleElementMap<>(key, get(key));
                } else {
                    return empty();
                }
            }

            if (keys instanceof ImmutableSetImpl.HashArrayBackedSet && ((ImmutableSetImpl.HashArrayBackedSet<?>) keys).tableSize == this.tableSize) {
                return intersection((ImmutableSetImpl.HashArrayBackedSet<? extends K>) keys);
            } else {
                return matching((k) -> keys.contains(k));
            }
        }

        private ImmutableMap<K, V> intersection(ImmutableSetImpl.HashArrayBackedSet<? extends K> keys) {
            int table1count = 0;
            int table2count = 0;

            K[] newTable1 = createTable1();
            V[] newValues1 = createValues1();
            K[] newTable2 = table2 != null ? createTable2() : null;
            V[] newValues2 = createValues2();

            for (int i = 0; i < tableSize; i++) {
                K key = this.table1[i];

                if (key != null) {
                    if (keys.contains(key, i)) {
                        newTable1[i] = key;
                        newValues1[i] = this.values1[i];
                        table1count++;
                    }
                }
            }

            if (this.table2 != null) {
                for (int i = 0; i < this.table2.length; i++) {
                    K key = this.table2[i];

                    if (key != null) {
                        int pos = i == 0 ? 0 : hashPosition(key);

                        if (keys.contains(key, pos)) {
                            if (newTable1[pos] == null) {
                                newTable1[pos] = key;
                                newValues1[pos] = this.values2[i];
                                table1count++;
                            } else {
                                for (int k = pos;; k++) {
                                    if (newTable2[k] == null) {
                                        newTable2[k] = key;
                                        newValues2[k] = this.values2[i];
                                        table2count++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            int count = table1count + table2count;

            if (count == 0) {
                return empty();
            } else if (count == 1) {
                int pos1 = findIndexOfNextNonNull(newTable1, 0);
                return new SingleElementMap<K, V>(newTable1[pos1], newValues1[pos1]);
            } else if (count == 2) {
                int pos1 = findIndexOfNextNonNull(newTable1, 0);
                int pos2 = findIndexOfNextNonNull(newTable1, pos1 + 1);

                if (pos2 != -1) {
                    return new TwoElementMap<K, V>(newTable1[pos1], newValues1[pos1], newTable1[pos2], newValues1[pos2]);
                } else {
                    pos2 = findIndexOfNextNonNull(newTable2, 0);
                    return new TwoElementMap<K, V>(newTable1[pos1], newValues1[pos1], newTable2[pos2], newValues2[pos2]);
                }
            } else if (count < size) {
                if (table2count == 0) {
                    return new HashArrayBackedMap<K, V>(tableSize, count, newTable1, newValues1, null, null);
                } else {
                    return new HashArrayBackedMap<K, V>(tableSize, count, newTable1, newValues1, newTable2, newValues2);
                }
            } else {
                return this;
            }
        }

        @Override
        public ImmutableMap<K, V> without(K other) {
            int pos = hashPosition(other);

            if (this.table1[pos] != null && other.equals(this.table1[pos])) {
                if (size == 1) {
                    return empty();
                }

                K[] newTable1 = this.table1.clone();
                V[] newValues1 = this.values1.clone();
                newTable1[pos] = null;
                newValues1[pos] = null;

                if (this.table2 == null || this.table2[pos] == null) {
                    return new HashArrayBackedMap<>(tableSize, size - 1, newTable1, newValues1, this.table2, this.values2);
                } else {
                    for (int i = pos; i < this.table2.length && this.table2[i] != null; i++) {
                        int otherPos = hashPosition(this.table2[i]);
                        if (otherPos == pos) {
                            newTable1[pos] = this.table2[i];
                            newValues1[pos] = this.values2[i];
                            K[] newTable2 = this.table2.clone();
                            V[] newValues2 = this.values2.clone();
                            newTable2[i] = null;
                            newValues2[i] = null;
                            repositionCollisions(tableSize, newTable2, newValues2, i);
                            return new HashArrayBackedMap<>(tableSize, size - 1, newTable1, newValues1, newTable2, newValues2);
                        }
                    }

                    return new HashArrayBackedMap<>(tableSize, size - 1, newTable1, newValues1, this.table2, this.values2);
                }

            } else if (this.table2 != null && this.table2[pos] != null) {
                int check = checkTable2(other, pos);

                if (check < 0) {
                    // Contained

                    if (size == 1) {
                        return empty();
                    }

                    int actualPos = -check - 1;

                    K[] newTable2 = this.table2.clone();
                    V[] newValues2 = this.values2.clone();

                    newTable2[actualPos] = null;
                    newValues2[actualPos] = null;

                    repositionCollisions(tableSize, newTable2, newValues2, actualPos);
                    return new HashArrayBackedMap<>(tableSize, size - 1, this.table1, this.values1, newTable2, newValues2);
                } else {
                    // Not contained
                    return this;
                }

            } else {
                return this;
            }
        }

        private static <K, V> void repositionCollisions(int tableSize, K[] table2, V[] values2, int start) {
            assert table2[start] == null;

            int firstGapAt = -1;
            int lastGapAt = -1;

            for (int i = start + 1; i < table2.length; i++) {
                if (table2[i] == null) {
                    // done
                    return;
                }

                int pos = hashPosition(tableSize, table2[i]);

                if (firstGapAt == -1) {
                    if (pos != i) {
                        table2[i - 1] = table2[i];
                        values2[i - 1] = values2[i];
                        table2[i] = null;
                        values2[i] = null;
                    } else {
                        firstGapAt = i - 1;
                        lastGapAt = i - 1;
                    }
                } else {
                    if (pos == i) {
                        if (table2[i - 1] == null) {
                            lastGapAt = i - 1;
                        }
                    } else if (pos == lastGapAt) {
                        assert table2[lastGapAt] == null;

                        table2[lastGapAt] = table2[i];
                        values2[lastGapAt] = values2[i];
                        table2[i] = null;
                        values2[i] = null;

                        lastGapAt = -1;

                    } else {
                        for (int k = i - 1; k >= firstGapAt; k--) {
                            if (k < pos) {
                                break;
                            }

                            if (table2[k] == null) {
                                table2[k] = table2[i];
                                values2[k] = values2[i];
                                table2[i] = null;
                                values2[i] = null;
                                lastGapAt = -1;
                                break;
                            }
                        }

                    }
                }
            }
        }

        private Map.Entry<K, V>[] getFlatEntries() {
            Map.Entry<K, V>[] result = this.flatEntries;

            if (result == null) {

                @SuppressWarnings("unchecked")
                Entry<K, V>[] flatEntries = (Entry<K, V>[]) new Entry[size];

                int k = 0;

                for (int i = 0; i < table1.length; i++) {
                    K key = table1[i];

                    if (key != null) {
                        flatEntries[k] = new AbstractMap.SimpleEntry<K, V>(key, values1[i]);
                        k++;
                    }
                }

                if (table2 != null) {
                    for (int i = 0; i < table2.length; i++) {
                        K key = table2[i];

                        if (key != null) {
                            flatEntries[k] = new AbstractMap.SimpleEntry<K, V>(key, values2[i]);
                            k++;
                        }
                    }
                }

                assert size == k;

                this.flatEntries = result = flatEntries;
            }

            return result;
        }

        @SuppressWarnings("unchecked")
        private K[] createTable1() {
            return (K[]) new Object[tableSize];
        }

        @SuppressWarnings("unchecked")
        private K[] createTable2() {
            return (K[]) new Object[tableSize + COLLISION_HEAD_ROOM];
        }

        @SuppressWarnings("unchecked")
        private V[] createValues1() {
            return (V[]) new Object[tableSize];
        }

        @SuppressWarnings("unchecked")
        private V[] createValues2() {
            return (V[]) new Object[tableSize + COLLISION_HEAD_ROOM];
        }

        @SuppressWarnings("unchecked")
        private static <E> E[] createTable1(int tableSize) {
            return (E[]) new Object[tableSize];
        }

        @SuppressWarnings("unchecked")
        private static <E> E[] createTable2(int tableSize) {
            return (E[]) new Object[tableSize + COLLISION_HEAD_ROOM];
        }

        @SuppressWarnings("unchecked")
        private V[] createVArray(int size) {
            return (V[]) new Object[size];
        }

        static int findIndexOfNextNonNull(Object[] array, int start) {
            for (int i = start; i < array.length; i++) {
                if (array[i] != null) {
                    return i;
                }
            }

            return -1;
        }

        int hashPosition(Object e) {
            return hashPosition(tableSize, e);
        }

        static int hashPosition(int tableSize, Object e) {
            if (e == null) {
                throw new IllegalArgumentException("ImmutableSet does not support null values");
            }

            int hash = e.hashCode();

            switch (tableSize) {
            case 16:
                return (hash & 0xf) ^ (hash >> 4 & 0xf) ^ (hash >> 8 & 0xf) ^ (hash >> 12 & 0xf) ^ (hash >> 16 & 0xf) ^ (hash >> 20 & 0xf)
                        ^ (hash >> 24 & 0xf) ^ (hash >> 28 & 0xf);
            case 64:
                return (hash & 0x3f) ^ (hash >> 6 & 0x3f) ^ (hash >> 12 & 0x3f) ^ (hash >> 18 & 0x3f) ^ (hash >> 24 & 0xf) ^ (hash >> 28 & 0xf);
            case 256:
                return (hash & 0xff) ^ (hash >> 8 & 0xff) ^ (hash >> 16 & 0xff) ^ (hash >> 24 & 0xff);
            default:
                throw new RuntimeException("Invalid tableSize " + tableSize);
            }

        }

        /** 
         * If e is contained in table2: returns the position as negative value calculated by -1 - position.
         * If e is not contained in table2: returns a possible free slot in table2 as positive value. If no slot is free, NO_SPACE is returned.
         */
        int checkTable2(Object e, int hashPosition) {
            return checkTable2(table2, e, hashPosition);
        }

        /** 
         * If e is contained in table2: returns the position as negative value calculated by -1 - position.
         * If e is not contained in table2: returns a possible free slot in table2 as positive value. If no slot is free, NO_SPACE is returned.
         */
        static <E> int checkTable2(E[] table2, Object e, int hashPosition) {
            if (table2[hashPosition] == null) {
                return hashPosition;
            } else if (table2[hashPosition].equals(e)) {
                return -1 - hashPosition;
            }

            int max = hashPosition + COLLISION_HEAD_ROOM;

            for (int i = hashPosition + 1; i <= max; i++) {
                if (table2[i] == null) {
                    return i;
                } else if (table2[i].equals(e)) {
                    return -1 - i;
                }
            }

            return NO_SPACE;
        }

        static class Builder<K, V> extends InternalBuilder<K, V> {
            private final static Object T = new Object();
            private K[] table1;
            private V[] values1;
            private K[] table2;
            private V[] values2;

            private int size = 0;
            private final int tableSize;
            private boolean containsTombstones = false;
            @SuppressWarnings("unchecked")
            private final K tombstone = (K) T;

            public Builder(int tableSize) {
                this.tableSize = tableSize;
            }

            public Builder(HashArrayBackedMap<K, V> initialContent) {
                this.table1 = initialContent.table1.clone();
                this.values1 = initialContent.values1.clone();
                this.table2 = initialContent.table2 != null ? initialContent.table2.clone() : null;
                this.values2 = initialContent.values2 != null ? initialContent.values2.clone() : null;
                this.size = initialContent.size;
                this.tableSize = initialContent.tableSize;
            }

            public InternalBuilder<K, V> with(K key, V value) {
                if (key == null) {
                    throw new IllegalArgumentException("Null keys are not supported");
                }

                return with(key, value, hashPosition(key));
            }

            private InternalBuilder<K, V> with(K key, V value, int pos) {

                if (table1 == null) {
                    table1 = createTable1(tableSize);
                    values1 = createTable1(tableSize);

                    table1[pos] = key;
                    values1[pos] = value;
                    size++;
                    return this;
                } else {
                    if (table1[pos] == null) {
                        table1[pos] = key;
                        values1[pos] = value;
                        size++;
                        return this;
                    } else if (table1[pos].equals(key)) {
                        // already contained
                        values1[pos] = value;
                        return this;
                    } else {
                        // collision

                        if (table2 == null) {
                            table2 = createTable2(tableSize);
                            values2 = createTable2(tableSize);
                            table2[pos] = key;
                            values2[pos] = value;
                            size++;
                            return this;
                        } else if (table2[pos] == null) {
                            table2[pos] = key;
                            values2[pos] = value;
                            size++;
                            return this;
                        } else {
                            int check = checkTable2(key, pos);

                            if (check < 0) {
                                // contained
                                int actualPos = -check - 1;
                                values2[actualPos] = value;
                                return this;
                            } else if (check == NO_SPACE) {
                                // collision
                                if (tableSize < 64) {
                                    return new HashArrayBackedMap.Builder<K, V>(64).with(build()).with(key, value);
                                } else if (tableSize < 256) {
                                    return new HashArrayBackedMap.Builder<K, V>(256).with(build()).with(key, value);
                                } else {
                                    return new MapBackedMap.Builder<>(build()).with(key, value);
                                }

                            } else {
                                table2[check] = key;
                                values2[check] = value;
                                size++;
                                return this;
                            }
                        }
                    }
                }
            }

            @Override
            <K2 extends K, V2 extends V> InternalBuilder<K, V> with(Map<K2, V2> map) {
                InternalBuilder<K, V> builder = this;

                for (Map.Entry<K2, V2> entry : map.entrySet()) {
                    builder = builder.with(entry.getKey(), entry.getValue());
                }

                return builder;
            }

            public ImmutableMap<K, V> build() {
                if (size == 0) {
                    return ImmutableMap.empty();
                } else if (size == 1) {
                    int i = findIndexOfNextNonNull(this.table1, 0);
                    return new SingleElementMap<>(this.table1[i], this.values1[i]);
                } else if (size == 2) {
                    int i1 = findIndexOfNextNonNull(this.table1, 0);
                    K key1 = this.table1[i1];
                    V value1 = this.values1[i1];
                    int i2 = findIndexOfNextNonNull(this.table1, i1 + 1);
                    K key2;
                    V value2;

                    if (i2 != -1) {
                        key2 = this.table1[i2];
                        value2 = this.values1[i2];
                    } else {
                        i2 = findIndexOfNextNonNullNonTombstone(this.table2, 0);
                        key2 = this.table2[i2];
                        value2 = this.values2[i2];
                    }

                    return new TwoElementMap<>(key1, value1, key2, value2);
                } else {
                    clearTombstones();
                    return new HashArrayBackedMap<>(tableSize, size, table1, values1, table2, values2);
                }
            }

            @Override
            <V2> ImmutableMap<K, V2> build(Function<V, V2> valueMappingFunction) {
                if (size == 0) {
                    return ImmutableMap.empty();
                } else if (size == 1) {
                    int i = findIndexOfNextNonNull(this.table1, 0);
                    return new SingleElementMap<>(this.table1[i], valueMappingFunction.apply(this.values1[i]));
                } else if (size == 2) {
                    int i1 = findIndexOfNextNonNull(this.table1, 0);
                    K key1 = this.table1[i1];
                    V value1 = this.values1[i1];
                    int i2 = findIndexOfNextNonNull(this.table1, i1 + 1);
                    K key2;
                    V value2;

                    if (i2 != -1) {
                        key2 = this.table1[i2];
                        value2 = this.values1[i2];
                    } else {
                        i2 = findIndexOfNextNonNullNonTombstone(this.table2, 0);
                        key2 = this.table2[i2];
                        value2 = this.values2[i2];
                    }

                    return new TwoElementMap<>(key1, valueMappingFunction.apply(value1), key2, valueMappingFunction.apply(value2));
                } else {
                    clearTombstones();
                    return new HashArrayBackedMap<>(tableSize, size, table1, mapArray(values1, valueMappingFunction), table2,
                            mapArray(values2, valueMappingFunction));
                }
            }

            static <V, V2> V2[] mapArray(V[] array, Function<V, V2> valueMappingFunction) {
                if (array == null) {
                    return null;
                }

                @SuppressWarnings("unchecked")
                V2[] result = (V2[]) array;
                for (int i = 0; i < array.length; i++) {
                    if (array[i] != null) {
                        result[i] = valueMappingFunction.apply(array[i]);
                    }
                }

                return result;
            }

            private void clearTombstones() {
                if (!containsTombstones) {
                    return;
                }

                int max = tableSize + COLLISION_HEAD_ROOM - 1;
                int table2count = 0;

                for (int i = 0; i <= max; i++) {
                    if (table2[i] == tombstone) {
                        table2[i] = null;
                        values2[i] = null;

                        int kMax = i >= tableSize ? max : i + COLLISION_HEAD_ROOM;

                        for (int k = kMax; k > i; k--) {
                            if (table2[k] != null && table2[k] != tombstone && hashPosition(table2[k]) <= i) {
                                table2[i] = table2[k];
                                values2[i] = values2[k];
                                table2[k] = tombstone;
                                values2[k] = null;
                                table2count++;
                                break;
                            }
                        }

                    } else if (table2[i] != null) {
                        table2count++;
                    }
                }

                if (table2count == 0) {
                    table2 = null;
                }
            }

            @Override
            int size() {
                return size;
            }

            @Override
            boolean remove(K key) {
                if (table1 == null) {
                    return false;
                }

                int position = hashPosition(key);

                if (table1[position] != null && table1[position].equals(key)) {
                    table1[position] = null;
                    values1[position] = null;
                    size--;

                    if (table2 != null && table2[position] != null) {
                        pullElementWithHashPositionFromTable2(position);
                    }

                    return true;
                } else if (table2 != null && table2[position] != null) {

                    int check = checkTable2(key, position);

                    if (check < 0) {
                        // Contained
                        int actualPos = -check - 1;
                        table2[actualPos] = tombstone;
                        containsTombstones = true;
                        size--;

                        return true;
                    }
                }

                // Not contained
                return false;
            }

            @Override
            boolean contains(K e) {
                if (table1 == null) {
                    return false;
                }

                int position = hashPosition(e);

                if (table1[position] != null && table1[position].equals(e)) {
                    return true;
                } else if (table2 != null && table2[position] != null) {
                    int check = checkTable2(e, position);

                    return check < 0;
                }

                return false;
            }

            @Override
            V get(K k) {
                if (table1 == null) {
                    return null;
                }

                int position = hashPosition(k);

                if (table1[position] != null && table1[position].equals(k)) {
                    return values1[position];
                } else if (table2 != null && table2[position] != null) {
                    int check = checkTable2(k, position);

                    if (check < 0) {
                        // Contained
                        int actualPos = -check - 1;
                        return values2[actualPos];
                    }
                }

                return null;
            }

            @Override
            Set<K> keySet() {
                if (size == 0) {
                    return ImmutableSet.empty();
                } else {
                    ImmutableSet.Builder<K> result = new ImmutableSet.Builder<>();

                    if (this.table1 != null) {
                        for (int i = 0; i < this.table1.length; i++) {
                            if (this.table1[i] != null) {
                                result.add(this.table1[i]);
                            }
                        }
                    }

                    if (this.table2 != null) {
                        for (int i = 0; i < this.table1.length; i++) {
                            if (this.table2[i] != null && this.table2[i] != tombstone) {
                                result.add(this.table2[i]);
                            }
                        }
                    }

                    return result.build();
                }
            }

            private int hashPosition(Object e) {
                return HashArrayBackedSet.hashPosition(tableSize, e);
            }

            /** 
             * If e is contained in table2: returns the position as negative value calculated by -1 - position.
             * If e is not contained in table2: returns a possible free slot in table2 as positive value. If no slot is free, NO_SPACE is returned.
             */
            int checkTable2(Object e, int hashPosition) {
                int insertionPositionCandidate = NO_SPACE;

                if (table2[hashPosition] == null) {
                    return hashPosition;
                } else if (table2[hashPosition] == tombstone) {
                    insertionPositionCandidate = hashPosition;
                } else if (table2[hashPosition].equals(e)) {
                    return -1 - hashPosition;
                }

                int max = hashPosition + COLLISION_HEAD_ROOM;

                for (int i = hashPosition + 1; i <= max; i++) {
                    if (table2[i] == null) {
                        if (insertionPositionCandidate != NO_SPACE) {
                            return insertionPositionCandidate;
                        } else {
                            return i;
                        }
                    } else if (table2[i] == tombstone) {
                        if (insertionPositionCandidate == NO_SPACE) {
                            insertionPositionCandidate = i;
                        }
                    } else if (table2[i].equals(e)) {
                        return -1 - i;
                    }
                }

                return insertionPositionCandidate;
            }

            void pullElementWithHashPositionFromTable2(int hashPosition) {
                int max = hashPosition + COLLISION_HEAD_ROOM;

                for (int i = hashPosition; i <= max; i++) {
                    if (table2[i] == null) {
                        table1[hashPosition] = null;
                        values1[hashPosition] = null;
                        return;
                    } else if (table2[i] != tombstone && hashPosition(table2[i]) == hashPosition) {
                        table1[hashPosition] = table2[i];
                        values1[hashPosition] = values2[i];
                        table2[i] = tombstone;
                        values2[i] = null;
                        containsTombstones = true;
                        return;
                    }
                }

                table1[hashPosition] = null;
                values1[hashPosition] = null;
            }

            int findIndexOfNextNonNullNonTombstone(Object[] array, int start) {
                for (int i = start; i < array.length; i++) {
                    if (array[i] != null && array[i] != tombstone) {
                        return i;
                    }
                }

                return -1;
            }

        }

        @Override
        public <VT> ImmutableMap<K, VT> mapValues(Function<V, VT> valueMappingFunction) {
            VT[] newValues1 = createTable1(this.tableSize);
            VT[] newValues2 = values2 != null ? createTable2(this.tableSize) : null;

            for (int i = 0; i < tableSize; i++) {
                V v = this.values1[i];

                if (v != null) {
                    newValues1[i] = valueMappingFunction.apply(v);
                }
            }

            if (this.values2 != null) {
                for (int i = 0; i < this.values2.length; i++) {
                    V v = this.values2[i];

                    if (v != null) {
                        newValues2[i] = valueMappingFunction.apply(v);
                    }
                }
            }

            return new HashArrayBackedMap<K, VT>(tableSize, this.size, this.table1, newValues1, this.table2, newValues2);
        }

        @Override
        public boolean forAllKeysApplies(Predicate<K> predicate) {
            for (int i = 0; i < tableSize; i++) {
                K key = this.table1[i];

                if (key != null && !predicate.test(key)) {
                    return false;
                }
            }

            if (this.table2 != null) {
                for (int i = 0; i < this.table2.length; i++) {
                    K key = this.table2[i];

                    if (key != null && !predicate.test(key)) {
                        return false;
                    }
                }
            }

            return true;
        }

        @Override
        public boolean forAnyKeyApplies(Predicate<K> predicate) {
            for (int i = 0; i < tableSize; i++) {
                K key = this.table1[i];

                if (key != null && predicate.test(key)) {
                    return true;
                }
            }

            if (this.table2 != null) {
                for (int i = 0; i < this.table2.length; i++) {
                    K key = this.table2[i];

                    if (key != null && predicate.test(key)) {
                        return true;
                    }
                }
            }

            return false;
        }

        @Override
        public <KT, VT> ImmutableMap<KT, VT> assertElementType(Class<KT> keyType, Class<VT> valueType) {
            @SuppressWarnings("unchecked")
            ImmutableMap<KT, VT> result = (ImmutableMap<KT, VT>) this;
            
            if (keyType.equals(Object.class) && valueType.equals(Object.class)) {
                return result;
            }
            
            for (int i = 0; i < tableSize; i++) {
                K key = this.table1[i];

                if (key != null) {
                    if (!keyType.isAssignableFrom(key.getClass())) {
                        throw new ClassCastException("Key " + key + " is not compatible with expected type " + keyType);
                    }

                    V value = this.values1[i];

                    if (value != null && !valueType.isAssignableFrom(value.getClass())) {
                        throw new ClassCastException("Value " + value + " is not compatible with expected type " + valueType);
                    }
                }
            }

            if (this.table2 != null) {
                for (int i = 0; i < this.table2.length; i++) {
                    K key = this.table2[i];

                    if (key != null) {
                        if (!keyType.isAssignableFrom(key.getClass())) {
                            throw new ClassCastException("Key " + key + " is not compatible with expected type " + keyType);
                        }

                        V value = this.values2[i];

                        if (value != null && !valueType.isAssignableFrom(value.getClass())) {
                            throw new ClassCastException("Value " + value + " is not compatible with expected type " + valueType);
                        }
                    }
                }
            }
          
            return result;
        }
    }

    static class ArrayBackedMap<K, V> extends AbstractImmutableMap<K, V> {
        private final K[] keys;
        private final V[] values;
        private ImmutableSet<K> keySet;
        private ImmutableSet<V> valueSet;
        private ImmutableSet<Entry<K, V>> entrySet;

        @SuppressWarnings("unchecked")
        ArrayBackedMap(K k1, V v1, K k2, V v2, K k3, V v3) {
            this.keys = (K[]) new Object[] { k1, k2, k3 };
            this.values = (V[]) new Object[] { v1, v2, v3 };
        }

        @SuppressWarnings("unchecked")
        ArrayBackedMap(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
            this.keys = (K[]) new Object[] { k1, k2, k3, k4 };
            this.values = (V[]) new Object[] { v1, v2, v3, v4 };
        }

        @SuppressWarnings("unchecked")
        ArrayBackedMap(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
            this.keys = (K[]) new Object[] { k1, k2, k3, k4, k5 };
            this.values = (V[]) new Object[] { v1, v2, v3, v4, v5 };
        }

        @SuppressWarnings("unchecked")
        ArrayBackedMap(Map<K, V> map) {
            this.keys = (K[]) new Object[map.size()];
            this.values = (V[]) new Object[map.size()];

            int i = 0;

            for (Map.Entry<K, V> entry : map.entrySet()) {
                this.keys[i] = entry.getKey();
                this.values[i] = entry.getValue();
                i++;
            }
        }

        ArrayBackedMap(K[] keys, V[] values) {
            this.keys = keys;
            this.values = values;
        }

        @Override
        public int size() {
            return keys.length;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean containsValue(Object value) {
            for (int i = 0; i < values.length; i++) {
                if (Objects.equals(values[i], value)) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public boolean containsKey(Object key) {
            for (int i = 0; i < keys.length; i++) {
                if (Objects.equals(keys[i], key)) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public V get(Object key) {
            for (int i = 0; i < keys.length; i++) {
                if (Objects.equals(keys[i], key)) {
                    return (V) values[i];
                }
            }
            return null;

        }

        @Override
        public void forEach(BiConsumer<? super K, ? super V> action) {
            for (int i = 0; i < keys.length; i++) {
                action.accept((K) keys[i], (V) values[i]);

            }
        }

        @Override
        public ImmutableSet<K> keySet() {
            if (keySet == null) {
                keySet = new ImmutableSetImpl.ArrayBackedSet<K>(keys);
            }

            return keySet;
        }

        @Override
        public UnmodifiableCollection<V> values() {
            if (valueSet == null) {
                valueSet = ImmutableSet.of(new HashSet<>(Arrays.asList(values)));
            }

            return valueSet;
        }

        @Override
        public ImmutableSet<Entry<K, V>> entrySet() {
            if (entrySet == null) {
                entrySet = new ImmutableSetImpl.SetBackedSet<>(new AbstractSet<Entry<K, V>>() {

                    @Override
                    public int size() {
                        return ArrayBackedMap.this.size();
                    }

                    @Override
                    public boolean isEmpty() {
                        return ArrayBackedMap.this.isEmpty();
                    }

                    @Override
                    public boolean contains(Object o) {
                        return ArrayBackedMap.this.containsKey(o);
                    }

                    @Override
                    public Iterator<Entry<K, V>> iterator() {

                        return new Iterator<Entry<K, V>>() {

                            private int i = 0;

                            @Override
                            public boolean hasNext() {
                                return i < keys.length;
                            }

                            @Override
                            public Entry<K, V> next() {
                                if (i < keys.length) {
                                    Entry<K, V> result = new AbstractMap.SimpleEntry<K, V>((K) keys[i], (V) values[i]);
                                    i++;
                                    return result;
                                } else {
                                    throw new NoSuchElementException();
                                }
                            }

                        };
                    }

                    @Override
                    public boolean add(Entry<K, V> e) {
                        throw new UnsupportedOperationException();
                    }

                    @Override
                    public boolean remove(Object o) {
                        throw new UnsupportedOperationException();
                    }

                    @Override
                    public boolean addAll(Collection<? extends Entry<K, V>> c) {
                        throw new UnsupportedOperationException();
                    }

                    @Override
                    public boolean retainAll(Collection<?> c) {
                        throw new UnsupportedOperationException();
                    }

                    @Override
                    public boolean removeAll(Collection<?> c) {
                        throw new UnsupportedOperationException();
                    }

                    @Override
                    public void clear() {
                        throw new UnsupportedOperationException();
                    }

                });

            }
            return entrySet;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Map)) {
                return false;
            }

            Map<?, ?> otherMap = (Map<?, ?>) o;

            if (otherMap.size() != size()) {
                return false;
            }

            for (int i = 0; i < keys.length; i++) {
                if (!Objects.equals(values[i], otherMap.get(keys[i]))) {
                    return false;
                }
            }

            return true;
        }

        @Override
        public ImmutableMap<K, V> with(K key, V value) {

            for (int i = 0; i < keys.length; i++) {
                if (Objects.equals(keys[i], key)) {
                    if (Objects.equals(values[i], value)) {
                        return this;
                    } else {
                        V[] values = this.values.clone();
                        values[i] = value;
                        return new ArrayBackedMap<>(keys, values);
                    }
                }
            }

            int l = this.keys.length;

            if (l < 5) {
                @SuppressWarnings("unchecked")
                K[] keys = (K[]) new Object[l + 1];
                @SuppressWarnings("unchecked")
                V[] values = (V[]) new Object[l + 1];
                System.arraycopy(this.keys, 0, keys, 0, l);
                System.arraycopy(this.values, 0, values, 0, l);

                keys[l] = key;
                values[l] = value;

                return new ArrayBackedMap<>(keys, values);
            } else {
                return new ImmutableMap.Builder<>(this).with(key, value).build();
            }
        }

        @Override
        public ImmutableMap<K, V> matching(Predicate<K> predicate) {
            int[] matchingIndices = new int[keys.length];
            int matchingCount = 0;

            for (int i = 0; i < this.keys.length; i++) {
                if (predicate.test(this.keys[i])) {
                    matchingIndices[matchingCount] = i;
                    matchingCount++;
                }
            }

            if (matchingCount == 0) {
                return empty();
            } else if (matchingCount == 1) {
                return new SingleElementMap<>(this.keys[matchingIndices[0]], this.values[matchingIndices[0]]);
            } else if (matchingCount == keys.length) {
                return this;
            } else {
                @SuppressWarnings("unchecked")
                K[] keys = (K[]) new Object[matchingCount];
                @SuppressWarnings("unchecked")
                V[] values = (V[]) new Object[matchingCount];

                for (int i = 0; i < matchingCount; i++) {
                    keys[i] = this.keys[matchingIndices[i]];
                    values[i] = this.values[matchingIndices[i]];
                }

                return new ArrayBackedMap<>(keys, values);
            }
        }
        
        @Override
        public ImmutableMap<K, V> matching(BiPredicate<K, V> predicate) {
            int[] matchingIndices = new int[keys.length];
            int matchingCount = 0;

            for (int i = 0; i < this.keys.length; i++) {
                if (predicate.test(this.keys[i], this.values[i])) {
                    matchingIndices[matchingCount] = i;
                    matchingCount++;
                }
            }

            if (matchingCount == 0) {
                return empty();
            } else if (matchingCount == 1) {
                return new SingleElementMap<>(this.keys[matchingIndices[0]], this.values[matchingIndices[0]]);
            } else if (matchingCount == keys.length) {
                return this;
            } else {
                @SuppressWarnings("unchecked")
                K[] keys = (K[]) new Object[matchingCount];
                @SuppressWarnings("unchecked")
                V[] values = (V[]) new Object[matchingCount];

                for (int i = 0; i < matchingCount; i++) {
                    keys[i] = this.keys[matchingIndices[i]];
                    values[i] = this.values[matchingIndices[i]];
                }

                return new ArrayBackedMap<>(keys, values);
            }
        }

        @Override
        public boolean forAllKeysApplies(Predicate<K> predicate) {
            for (int i = 0; i < this.keys.length; i++) {
                if (!predicate.test(this.keys[i])) {
                    return false;
                }
            }

            return true;
        }

        @Override
        public boolean forAnyKeyApplies(Predicate<K> predicate) {
            for (int i = 0; i < this.keys.length; i++) {
                if (predicate.test(this.keys[i])) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public <KT, VT> ImmutableMap<KT, VT> assertElementType(Class<KT> keyType, Class<VT> valueType) {
            @SuppressWarnings("unchecked")
            ImmutableMap<KT, VT> result = (ImmutableMap<KT, VT>) this;
            
            if (keyType.equals(Object.class) && valueType.equals(Object.class)) {
                return result;
            }
            
            for (int i = 0; i < this.keys.length; i++) {
                K key = this.keys[i];

                if (!keyType.isAssignableFrom(key.getClass())) {
                    throw new ClassCastException("Key " + key + " is not compatible with expected type " + keyType);
                }

                V value = this.values[i];

                if (value != null && !valueType.isAssignableFrom(value.getClass())) {
                    throw new ClassCastException("Value " + value + " is not compatible with expected type " + valueType);
                }
            }

            return result;
        }
    }

    static class MapBackedMap<K, V> extends AbstractImmutableMap<K, V> {
        private final Map<K, V> delegate;

        MapBackedMap(Map<K, V> delegate) {
            this.delegate = delegate;
        }

        @Override
        public int size() {
            return delegate.size();
        }

        @Override
        public boolean isEmpty() {
            return delegate.isEmpty();
        }

        @Override
        public boolean containsValue(Object value) {
            return delegate.containsValue(value);
        }

        @Override
        public boolean containsKey(Object key) {
            return delegate.containsKey(key);

        }

        @Override
        public V get(Object key) {
            return delegate.get(key);
        }

        @Override
        public ImmutableSet<K> keySet() {
            return ImmutableSet.of(delegate.keySet());
        }

        @Override
        public UnmodifiableCollection<V> values() {
            return UnmodifiableCollection.of(delegate.values());
        }

        @Override
        public ImmutableSet<Entry<K, V>> entrySet() {
            return new ImmutableSetImpl.SetBackedSet<>(delegate.entrySet());
        }

        @Override
        public boolean equals(Object o) {
            return delegate.equals(o);
        }

        @Override
        public int hashCode() {
            return delegate.hashCode();
        }

        @Override
        public String toString() {
            return delegate.toString();
        }

        @Override
        public ImmutableMap<K, V> with(K key, V value) {
            if (Objects.equals(delegate.get(key), value)) {
                return this;
            } else {
                Map<K, V> map = new LinkedHashMap<>(this);
                map.put(key, value);
                return new MapBackedMap<>(map);
            }
        }

        static class Builder<K, V> extends InternalBuilder<K, V> {
            private HashMap<K, V> delegate;

            Builder(int expectedCapacity) {
                this.delegate = new HashMap<>(expectedCapacity);
            }

            Builder(Map<K, V> map) {
                this.delegate = new HashMap<>(map);
            }

            public Builder<K, V> with(K key, V value) {
                this.delegate.put(key, value);
                return this;
            }

            @Override
            ImmutableMap<K, V> build() {
                return new MapBackedMap<>(this.delegate);
            }

            @Override
            <K2 extends K, V2 extends V> InternalBuilder<K, V> with(Map<K2, V2> map) {
                this.delegate.putAll(map);
                return this;
            }

            @Override
            int size() {
                return delegate.size();
            }

            @Override
            boolean remove(K e) {
                return delegate.remove(e) != null;
            }

            @Override
            boolean contains(K e) {
                return delegate.containsKey(e);
            }

            @Override
            V get(K k) {
                return delegate.get(k);
            }

            @Override
            Set<K> keySet() {
                return delegate.keySet();
            }

            @Override
            <V2> ImmutableMap<K, V2> build(Function<V, V2> valueMappingFunction) {
                HashMap<K, V2> result = new HashMap<>(delegate.size());

                for (Map.Entry<K, V> entry : this.delegate.entrySet()) {
                    result.put(entry.getKey(), valueMappingFunction.apply(entry.getValue()));
                }
                return new MapBackedMap<>(result);
            }

        }

        @Override
        public void forEach(BiConsumer<? super K, ? super V> action) {
            delegate.forEach(action);
        }

        @Override
        public ImmutableMap<K, V> matching(Predicate<K> predicate) {
            HashMap<K, V> result = new HashMap<>(delegate.size());

            delegate.forEach((k, v) -> {
                if (predicate.test(k)) {
                    result.put(k, v);
                }
            });

            return new MapBackedMap<>(result);
        }
        
        @Override
        public ImmutableMap<K, V> matching(BiPredicate<K, V> predicate) {
            HashMap<K, V> result = new HashMap<>(delegate.size());

            delegate.forEach((k, v) -> {
                if (predicate.test(k, v)) {
                    result.put(k, v);
                }
            });

            return new MapBackedMap<>(result);
        }

        @Override
        public boolean forAllKeysApplies(Predicate<K> predicate) {
            return delegate.keySet().stream().allMatch(predicate);
        }

        @Override
        public boolean forAnyKeyApplies(Predicate<K> predicate) {
            return delegate.keySet().stream().anyMatch(predicate);
        }

    }

    static class WithoutMap<K, V> extends AbstractImmutableMap<K, V> {
        private final Map<K, V> delegate;
        private final K withoutKey;

        WithoutMap(Map<K, V> delegate, K withoutKey) {
            this.delegate = delegate;
            this.withoutKey = withoutKey;
        }

        @Override
        public int size() {
            return delegate.size() - 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean containsKey(Object key) {
            if (this.withoutKey.equals(key)) {
                return false;
            } else {
                return this.delegate.containsKey(key);
            }
        }

        @Override
        public boolean containsValue(Object value) {
            for (Map.Entry<K, V> entry : delegate.entrySet()) {
                if (this.withoutKey.equals(entry.getKey())) {
                    continue;
                }

                if (Objects.equals(value, entry.getValue())) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public V get(Object key) {
            if (this.withoutKey.equals(key)) {
                return null;
            } else {
                return this.delegate.get(key);
            }
        }

        @Override
        public ImmutableSet<K> keySet() {
            return ImmutableSet.of(delegate.keySet()).without(withoutKey);
        }

        @Override
        public UnmodifiableCollection<V> values() {
            return UnmodifiableCollection.of(entrySet().stream().map(e -> e.getValue()).collect(Collectors.toSet()));
        }

        @Override
        public ImmutableSet<Entry<K, V>> entrySet() {
            Set<Entry<K, V>> delegateSet = delegate.entrySet();

            return new ImmutableSetImpl.SetBackedSet<>(new AbstractSet<Entry<K, V>>() {

                @Override
                public int size() {
                    return WithoutMap.this.size();
                }

                @Override
                public boolean isEmpty() {
                    return WithoutMap.this.isEmpty();
                }

                @Override
                public boolean contains(Object o) {
                    return WithoutMap.this.containsKey(o);
                }

                @Override
                public Iterator<Entry<K, V>> iterator() {
                    Iterator<Entry<K, V>> delegateIter = delegateSet.iterator();

                    return new Iterator<Entry<K, V>>() {

                        private Entry<K, V> next;
                        private boolean initialized;

                        @Override
                        public boolean hasNext() {
                            init();
                            return next != null;
                        }

                        @Override
                        public Entry<K, V> next() {
                            init();
                            initialized = false;
                            return next;
                        }

                        private void init() {
                            if (!initialized) {
                                next = null;
                                while (delegateIter.hasNext()) {
                                    next = delegateIter.next();

                                    if (!withoutKey.equals(next.getKey())) {
                                        break;
                                    } else {
                                        next = null;
                                    }
                                }

                                initialized = true;
                            }
                        }

                    };
                }

                @Override
                public boolean add(Entry<K, V> e) {
                    throw new UnsupportedOperationException();
                }

                @Override
                public boolean remove(Object o) {
                    throw new UnsupportedOperationException();
                }

                @Override
                public boolean addAll(Collection<? extends Entry<K, V>> c) {
                    throw new UnsupportedOperationException();
                }

                @Override
                public boolean retainAll(Collection<?> c) {
                    throw new UnsupportedOperationException();
                }

                @Override
                public boolean removeAll(Collection<?> c) {
                    throw new UnsupportedOperationException();
                }

                @Override
                public void clear() {
                    throw new UnsupportedOperationException();
                }

            });
        }

        @Override
        public ImmutableMap<K, V> with(K key, V value) {
            if (Objects.equals(delegate.get(key), value)) {
                return this;
            } else {
                ImmutableMap.Builder<K, V> builder = new ImmutableMap.Builder<>(this).with(key, value);
                return builder.build();
            }
        }

        @Override
        public ImmutableMap<K, V> matching(Predicate<K> predicate) {
            if (delegate instanceof ImmutableMap) {
                return ((ImmutableMap<K, V>) delegate).matching((k) -> predicate.test(k) && !k.equals(withoutKey));
            } else {
                ImmutableMap.Builder<K, V> builder = new ImmutableMap.Builder<>(delegate.size());

                delegate.forEach((k, v) -> {
                    if (!k.equals(withoutKey) && predicate.test(k)) {
                        builder.with(k, v);
                    }
                });

                return builder.build();
            }
        }
        

        @Override
        public ImmutableMap<K, V> matching(BiPredicate<K, V> predicate) {
            if (delegate instanceof ImmutableMap) {
                return ((ImmutableMap<K, V>) delegate).matching((k, v) -> predicate.test(k, v) && !k.equals(withoutKey));
            } else {
                ImmutableMap.Builder<K, V> builder = new ImmutableMap.Builder<>(delegate.size());

                delegate.forEach((k, v) -> {
                    if (!k.equals(withoutKey) && predicate.test(k, v)) {
                        builder.with(k, v);
                    }
                });

                return builder.build();
            }
        }

        @Override
        public boolean forAllKeysApplies(Predicate<K> predicate) {
            return delegate.keySet().stream().filter((k) -> !k.equals(this.withoutKey)).allMatch(predicate);
        }

        @Override
        public boolean forAnyKeyApplies(Predicate<K> predicate) {
            return delegate.keySet().stream().filter((k) -> !k.equals(this.withoutKey)).anyMatch(predicate);
        }
    }

    static class WithMap<K, V> extends AbstractImmutableMap<K, V> {

        private final ImmutableMap<K, V> base;
        private final K additional;
        private final Map.Entry<K, V> additionalEntry;
        private final int size;

        WithMap(ImmutableMap<K, V> base, K additional, V additionalValue) {
            this.base = base;
            this.additional = additional;
            this.additionalEntry = new AbstractMap.SimpleEntry<K, V>(additional, additionalValue);
            this.size = base.size() + 1;
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public V get(Object key) {
            if (additional.equals(key)) {
                return additionalEntry.getValue();
            } else {
                return base.get(key);
            }
        }

        @Override
        public boolean containsKey(Object o) {
            return Objects.equals(o, additional) || this.base.containsKey(o);
        }

        @Override
        public int hashCode() {
            return base.hashCode() + additional.hashCode();
        }

        @Override
        public ImmutableMap<K, V> with(K key, V value) {
            int size = size();

            if (size == 0) {
                return new SingleElementMap<K, V>(key, value);
            } else if (Objects.equals(get(key), value)) {
                return this;
            } else {
                return new Builder<>(this).with(key, value).build();
            }
        }

        @Override
        public ImmutableSet<Entry<K, V>> entrySet() {
            return ((ImmutableSet<Entry<K, V>>) base.entrySet()).with(additionalEntry);
        }

        @Override
        public void forEach(BiConsumer<? super K, ? super V> action) {
            base.forEach(action);
            action.accept(additional, additionalEntry.getValue());
        }

        @Override
        public ImmutableMap<K, V> matching(Predicate<K> predicate) {
            if (predicate.test(additional)) {
                ImmutableMap<K, V> matchingBase = base.matching(predicate);

                if (matchingBase == base) {
                    return this;
                } else {
                    return new WithMap<>(matchingBase, additional, additionalEntry.getValue());
                }
            } else {
                return base.matching(predicate);
            }

        }

        @Override
        public ImmutableMap<K, V> matching(BiPredicate<K,V> predicate) {
            if (predicate.test(additional, additionalEntry.getValue())) {
                ImmutableMap<K, V> matchingBase = base.matching(predicate);

                if (matchingBase == base) {
                    return this;
                } else {
                    return new WithMap<>(matchingBase, additional, additionalEntry.getValue());
                }
            } else {
                return base.matching(predicate);
            }

        }

        @Override
        public boolean forAllKeysApplies(Predicate<K> predicate) {
            return predicate.test(additional) && base.forAllKeysApplies(predicate);
        }

        @Override
        public boolean forAnyKeyApplies(Predicate<K> predicate) {
            return predicate.test(additional) || base.forAnyKeyApplies(predicate);
        }

        @Override
        public <KT, VT> ImmutableMap<KT, VT> assertElementType(Class<KT> keyType, Class<VT> valueType) {
            base.assertElementType(keyType, valueType);

            if (!keyType.isAssignableFrom(additional.getClass())) {
                throw new ClassCastException("Key " + additional + " is not compatible with expected type " + keyType);
            }

            if (additionalEntry.getValue() != null && !valueType.isAssignableFrom(additionalEntry.getValue().getClass())) {
                throw new ClassCastException("Value " + additionalEntry.getValue() + " is not compatible with expected type " + valueType);
            }

            @SuppressWarnings("unchecked")
            ImmutableMap<KT, VT> result = (ImmutableMap<KT, VT>) this;
            return result;
        }
    }

    static final Map<?, ?> EMPTY_MAP = new AbstractImmutableMap<Object, Object>() {

        @Override
        public ImmutableSet<Entry<Object, Object>> entrySet() {
            return ImmutableSet.empty();
        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public boolean containsValue(Object value) {
            return false;
        }

        @Override
        public boolean containsKey(Object key) {
            return false;
        }

        @Override
        public Object get(Object key) {
            return null;
        }

        @Override
        public ImmutableSet<Object> keySet() {
            return ImmutableSet.empty();
        }

        @Override
        public UnmodifiableCollection<Object> values() {
            return ImmutableSet.empty();
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Map)) {
                return false;
            }

            Map<?, ?> otherMap = (Map<?, ?>) o;

            return otherMap.size() == 0;
        }

        @Override
        public String toString() {
            return "{}";
        }

        @Override
        public ImmutableMap<Object, Object> with(Object key, Object value) {
            return ImmutableMap.of(key, value);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <K2 extends Object, V2 extends Object> ImmutableMap<Object, Object> with(ImmutableMap<K2, V2> other) {
            return (ImmutableMap<Object, Object>) other;
        }

        @Override
        public ImmutableMap<Object, Object> matching(Predicate<Object> predicate) {
            return this;
        }

        @Override
        public ImmutableMap<Object, Object> matching(BiPredicate<Object, Object> predicate) {
            return this;
        }
        
        @Override
        public <VT> ImmutableMap<Object, VT> mapValues(Function<Object, VT> valueMappingFunction) {
            return empty();
        }

        @Override
        public boolean forAllKeysApplies(Predicate<Object> predicate) {
            return true;
        }

        @Override
        public boolean forAnyKeyApplies(Predicate<Object> predicate) {
            return false;
        }

        @Override
        public <KT, VT> ImmutableMap<KT, VT> assertElementType(Class<KT> keyType, Class<VT> valueType) {
            return empty();
        }
    };

    abstract static class AbstractImmutableMap<K, V> extends AbstractMap<K, V> implements ImmutableMap<K, V> {

        @Deprecated
        @Override
        public V put(K key, V value) {
            throw new UnsupportedOperationException();
        }

        @Deprecated
        @Override
        public V remove(Object key) {
            throw new UnsupportedOperationException();
        }

        @Deprecated
        @Override
        public void putAll(Map<? extends K, ? extends V> m) {
            throw new UnsupportedOperationException();
        }

        @Deprecated
        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }

        @Override
        public UnmodifiableCollection<V> values() {
            return UnmodifiableCollection.of(super.values());
        }

        public ImmutableMap<K, V> without(K key) {
            if (containsKey(key)) {
                if (size() == 1) {
                    return empty();
                } else {
                    return new WithoutMap<K, V>(this, key);
                }
            } else {
                return this;
            }
        }

        @Override
        public ImmutableMap<K, V> withComputed(K key, Function<V, V> f) {
            V oldValue = this.get(key);
            V newValue = f.apply(oldValue);

            if (Objects.equals(oldValue, newValue)) {
                return this;
            } else {
                return with(key, newValue);
            }
        }

        @Override
        public  <K2 extends K, V2 extends V> ImmutableMap<K, V> with(ImmutableMap<K2, V2> other) {
            if (this.size() == 0) {
                @SuppressWarnings("unchecked")
                ImmutableMap<K, V> result = (ImmutableMap<K, V>) other;
                return result;
            } else if (other.size() == 0) {
                return this;
            } else {
                Map<K, V> map = new LinkedHashMap<>(this);
                map.putAll(other);
                return new MapBackedMap<>(map);
            }
        }

        @Override
        public ImmutableMap<K, V> intersection(ImmutableSet<? extends K> keys) {
            int otherSize = keys.size();

            if (otherSize == 0) {
                return empty();
            } else if (otherSize == 1) {
                K otherKey = keys.only();

                if (containsKey(otherKey)) {
                    return new SingleElementMap<>(otherKey, get(otherKey));
                } else {
                    return empty();
                }
            } else {
                Builder<K, V> builder = new Builder<>();

                for (K key : keys) {
                    if (containsKey(key)) {
                        builder.with(key, get(key));
                    }
                }

                return builder.build();
            }

        }

        @Override
        public boolean containsAny(ImmutableSet<? extends K> keys) {
            int otherSize = keys.size();

            if (otherSize == 0) {
                return false;
            } else if (otherSize == 1) {
                return containsKey(keys.only());
            } else {
                for (K key : keys) {
                    if (containsKey(key)) {
                        return true;
                    }
                }

                return false;
            }

        }

        @Override
        public ImmutableSet<K> keySet() {
            return ImmutableSet.of(super.keySet());
        }

        @Override
        public ImmutableList<V> values(BiPredicate<K, V> predicate) {
            ImmutableList.Builder<V> result = new ImmutableList.Builder<>(size());

            forEach((k, v) -> {
                if (predicate.test(k, v)) {
                    result.with(v);
                }
            });

            return result.build();
        }

        @Override
        public ImmutableList<V> valuesForKeys(ImmutableSet<? extends K> keys) {
            if (keys.size() < this.size()) {
                ImmutableList.Builder<V> result = new ImmutableList.Builder<>(keys.size());

                keys.forEach((k) -> {
                    V v = get(k);

                    if (v != null) {
                        result.with(v);
                    }
                });

                return result.build();
            } else {
                return values((k, v) -> keys.contains(k));
            }
        }

        @Override
        public <KT, VT> ImmutableMap<KT, VT> map(Function<K, KT> keyMappingFunction, Function<V, VT> valueMappingFunction) {
            return ImmutableMapImpl.map(this, keyMappingFunction, valueMappingFunction);
        }

        @Override
        public <VT> ImmutableMap<K, VT> mapValues(Function<V, VT> valueMappingFunction) {
            ImmutableMap.Builder<K, VT> builder = new ImmutableMap.Builder<>(size());

            for (Map.Entry<K, V> entry : this.entrySet()) {
                VT newValue = valueMappingFunction.apply(entry.getValue());

                if (newValue != null) {
                    builder.put(entry.getKey(), newValue);
                }
            }

            return builder.build();
        }

        @Override
        public <KT> ImmutableMap<KT, V> ensureKeyType(Class<KT> keyType, Function<Object, KT> conversionFunction) {
            if (forAllKeysApplies((k) -> keyType.isAssignableFrom(k.getClass()))) {
                @SuppressWarnings("unchecked")
                ImmutableMap<KT, V> result = (ImmutableMap<KT, V>) this;
                return result;
            } else {
                return map((k) -> conversionFunction.apply(k), (Function<V, V>) (v) -> v);
            }
        }

        static <K, V> Map.Entry<K, V> entry(K key, V value) {
            return new Map.Entry<K, V>() {

                @Override
                public K getKey() {
                    return key;
                }

                @Override
                public V getValue() {
                    return value;
                }

                @Override
                public V setValue(V value) {
                    throw new UnsupportedOperationException();
                }
            };
        }

        @Override
        public <KT, VT> ImmutableMap<KT, VT> assertElementType(Class<KT> keyType, Class<VT> valueType) {
            @SuppressWarnings("unchecked")
            ImmutableMap<KT, VT> result = (ImmutableMap<KT, VT>) this;
            
            if (keyType.equals(Object.class) && valueType.equals(Object.class)) {
                return result;
            }
            
            for (Map.Entry<K, V> entry : entrySet()) {
                K key = entry.getKey();
                V value = entry.getValue();

                if (!keyType.isAssignableFrom(key.getClass())) {
                    throw new ClassCastException("Key " + key + " is not compatible with expected type " + keyType);
                }

                if (value != null && !valueType.isAssignableFrom(value.getClass())) {
                    throw new ClassCastException("Value " + value + " is not compatible with expected type " + valueType);
                }
            }

            return result;
        }
    }

}
