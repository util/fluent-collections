/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public interface UnmodifiableList<V> extends List<V>, UnmodifiableCollection<V> {

    static <V> UnmodifiableList<V> of(List<V> list) {
        if (list instanceof UnmodifiableList) {
            return (UnmodifiableList<V>) list;
        }

        return new UnmodifiableList<V>() {

            @Override
            public int size() {
                return list.size();
            }

            @Override
            public boolean isEmpty() {
                return list.isEmpty();
            }

            @Override
            public boolean contains(Object o) {
                return list.contains(o);
            }

            @Override
            public UnmodifiableIterator<V> iterator() {
                return UnmodifiableIterator.of(list.iterator());
            }

            @Override
            public Object[] toArray() {
                return list.toArray();
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return list.toArray(a);
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return list.containsAll(c);
            }

            @Override
            public V get(int index) {
                return list.get(index);
            }

            @Override
            public int indexOf(Object o) {
                return list.indexOf(o);
            }

            @Override
            public int lastIndexOf(Object o) {
                return list.lastIndexOf(o);
            }

            @Override
            public ListIterator<V> listIterator() {
                return list.listIterator();
            }

            @Override
            public ListIterator<V> listIterator(int index) {
                return list.listIterator(index);
            }

            @Override
            public List<V> subList(int fromIndex, int toIndex) {
                return UnmodifiableList.of(list.subList(fromIndex, toIndex));
            }
        };
    };

    @Deprecated
    @Override
    default boolean addAll(int index, Collection<? extends V> c) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default void replaceAll(UnaryOperator<V> operator) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default void sort(Comparator<? super V> c) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default V set(int index, V element) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default void add(int index, V element) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean removeIf(Predicate<? super V> filter) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean add(V e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean addAll(Collection<? extends V> c) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default void clear() {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default V remove(int index) {
        throw new UnsupportedOperationException();
    }
}
