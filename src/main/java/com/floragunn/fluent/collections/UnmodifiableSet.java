/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.Collection;
import java.util.Set;

public interface UnmodifiableSet<V> extends Set<V>, UnmodifiableCollection<V> {

    static <V> UnmodifiableSet<V> of(Set<V> set) {
        if (set instanceof UnmodifiableSet) {
            return (UnmodifiableSet<V>) set;
        }

        return new UnmodifiableSet<V>() {

            @Override
            public int size() {
                return set.size();
            }

            @Override
            public boolean isEmpty() {
                return set.isEmpty();
            }

            @Override
            public boolean contains(Object o) {
                return set.contains(o);
            }

            @Override
            public UnmodifiableIterator<V> iterator() {
                return UnmodifiableIterator.of(set.iterator());
            }

            @Override
            public Object[] toArray() {
                return set.toArray();
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return set.toArray(a);
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return set.containsAll(c);
            }
        };
    }

    @Deprecated
    @Override
    default boolean add(V e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean addAll(Collection<? extends V> c) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    default void clear() {
        throw new UnsupportedOperationException();
    }

}
