/*
 * Copyright 2022 floragunn GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.floragunn.fluent.collections;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import com.floragunn.fluent.collections.ImmutableMapImpl.HashArrayBackedMap;
import com.floragunn.fluent.collections.ImmutableMapImpl.InternalBuilder;
import com.floragunn.fluent.collections.ImmutableMapImpl.MapBackedMap;

public interface ImmutableMap<K, V> extends UnmodifiableMap<K, V> {

    static <K, V> ImmutableMap<K, V> of(K k1, V v1) {
        return ImmutableMapImpl.of(k1, v1);
    }

    static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2) {
        return ImmutableMapImpl.of(k1, v1, k2, v2);
    }

    static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3) {
        return ImmutableMapImpl.of(k1, v1, k2, v2, k3, v3);
    }

    static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        return ImmutableMapImpl.of(k1, v1, k2, v2, k3, v3, k4, v4);
    }

    static <K, V> ImmutableMap<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return ImmutableMapImpl.of(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5);
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1) {
        return ImmutableMapImpl.ofNonNull(k1, v1);
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2) {
        return ImmutableMapImpl.ofNonNull(k1, v1, k2, v2);
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2, K k3, V v3) {
        return ImmutableMapImpl.ofNonNull(k1, v1, k2, v2, k3, v3);
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        return ImmutableMapImpl.ofNonNull(k1, v1, k2, v2, k3, v3, k4, v4);
    }

    static <K, V> ImmutableMap<K, V> ofNonNull(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return ImmutableMapImpl.ofNonNull(k1, v1, k2, v2, k3, v3, k4, v4, k5, v5);
    }

    static <K, V> ImmutableMap<K, V> of(Map<? extends K, ? extends V> map) {
        return ImmutableMapImpl.of(map);
    }

    static <K, V> ImmutableMap<K, V> of(Map<K, V> map, K k1, V v1) {
        return ImmutableMapImpl.of(map, k1, v1);
    }

    static <K, V> ImmutableMap<K, V> of(Map<K, V> map, K k1, V v1, K k2, V v2) {
        return ImmutableMapImpl.of(map, k1, v1, k2, v2);
    }

    static <K, V> ImmutableMap<K, V> without(Map<K, V> map, K key) {
        return ImmutableMapImpl.without(map, key);
    }

    static <C, K, V> ImmutableMap<K, V> map(Collection<C> collection, Function<C, Map.Entry<K, V>> mappingFunction) {
        return ImmutableMapImpl.map(collection, mappingFunction);
    }

    static <KS, VS, KT, VT> ImmutableMap<KT, VT> map(Map<KS, VS> source, Function<KS, KT> keyMappingFunction, Function<VS, VT> valueMappingFunction) {
        return ImmutableMapImpl.map(source, keyMappingFunction, valueMappingFunction);
    }

    static <K, V> Map.Entry<K, V> entry(K k1, V v1) {
        return new AbstractMap.SimpleImmutableEntry<>(k1, v1);
    }

    static <K, V> ImmutableMap<K, V> empty() {
        return ImmutableMapImpl.empty();
    }

    ImmutableMap<K, V> without(K key);

    ImmutableMap<K, V> with(K key, V value);

    <K2 extends K, V2 extends V> ImmutableMap<K, V> with(ImmutableMap<K2, V2> other);

    ImmutableMap<K, V> withComputed(K key, Function<V, V> f);

    ImmutableMap<K, V> matching(Predicate<K> predicate);

    ImmutableMap<K, V> matching(BiPredicate<K,V> predicate);

    ImmutableMap<K, V> intersection(ImmutableSet<? extends K> keys);

    boolean containsAny(ImmutableSet<? extends K> keys);

    ImmutableSet<K> keySet();
    
    ImmutableSet<Map.Entry<K, V>> entrySet();

    ImmutableList<V> values(BiPredicate<K, V> predicate);

    ImmutableList<V> valuesForKeys(ImmutableSet<? extends K> keys);

    <KT, VT> ImmutableMap<KT, VT> map(Function<K, KT> keyMappingFunction, Function<V, VT> valueMappingFunction);

    <VT> ImmutableMap<K, VT> mapValues(Function<V, VT> valueMappingFunction);
    
    boolean forAllKeysApplies(Predicate<K> predicate);

    boolean forAnyKeyApplies(Predicate<K> predicate);

    <KT, VT> ImmutableMap<KT, VT> assertElementType(Class<KT> keyType, Class<VT> valueType);

    <KT> ImmutableMap<KT, V> ensureKeyType(Class<KT> keyType, Function<Object, KT> conversionFunction);
        
    static class Builder<K, V> {
        private InternalBuilder<K, V> internalBuilder;
        private Function<K, V> defaultSupplier;

        public Builder() {
            internalBuilder = new HashArrayBackedMap.Builder<K, V>(16);
        }

        public Builder(int expectedNumberOfElements) {
            if (expectedNumberOfElements <= 25) {
                internalBuilder = new HashArrayBackedMap.Builder<K, V>(16);
            } else if (expectedNumberOfElements <= 100) {
                internalBuilder = new HashArrayBackedMap.Builder<K, V>(64);
            } else if (expectedNumberOfElements <= 400) {
                internalBuilder = new HashArrayBackedMap.Builder<K, V>(256);
            } else {
                internalBuilder = new MapBackedMap.Builder<K, V>(expectedNumberOfElements);
            }
        }

        public Builder(Map<K, V> initialContent) {
            if (initialContent instanceof HashArrayBackedMap) {
                internalBuilder = new HashArrayBackedMap.Builder<K, V>((HashArrayBackedMap<K, V>) initialContent);
            } else {
                if (initialContent.size() <= 25) {
                    internalBuilder = new HashArrayBackedMap.Builder<K, V>(16);

                    for (Map.Entry<K, V> entry : initialContent.entrySet()) {
                        internalBuilder = internalBuilder.with(entry.getKey(), entry.getValue());
                    }
                } else if (initialContent.size() <= 100) {
                    internalBuilder = new HashArrayBackedMap.Builder<K, V>(64);

                    for (Map.Entry<K, V> entry : initialContent.entrySet()) {
                        internalBuilder = internalBuilder.with(entry.getKey(), entry.getValue());
                    }
                } else if (initialContent.size() <= 400) {
                    internalBuilder = new HashArrayBackedMap.Builder<K, V>(256);

                    for (Map.Entry<K, V> entry : initialContent.entrySet()) {
                        internalBuilder = internalBuilder.with(entry.getKey(), entry.getValue());
                    }
                } else {
                    internalBuilder = new MapBackedMap.Builder<K, V>(initialContent);
                }
            }
        }
        
        public Builder<K, V> defaultValue(Function<K, V> defaultSupplier) {
            this.defaultSupplier = defaultSupplier;
            return this;
        }

        public Builder<K, V> with(K key, V value) {
            if (key == null) {
                throw new IllegalArgumentException("null keys are not allowed");
            }
            internalBuilder = internalBuilder.with(key, value);
            return this;
        }

        public Builder<K, V> with(Map<K, V> map) {
            if (!map.isEmpty()) {
                internalBuilder = internalBuilder.with(map);                
            }
            
            return this;
        }

        public void put(K key, V value) {
            if (key == null) {
                throw new IllegalArgumentException("null keys are not allowed");
            }

            internalBuilder = internalBuilder.with(key, value);
        }

        public void putAll(Map<K, V> map) {
            if (!map.isEmpty()) {
                internalBuilder = internalBuilder.with(map);                
            }
        }

        public boolean remove(K e) {
            return internalBuilder.remove(e);
        }

        public boolean contains(K e) {
            return internalBuilder.contains(e);
        }

        public boolean containsAnyKey(Iterable<K> keys) {
            return internalBuilder.containsAnyKey(keys);
        }
        
        public V get(K k) {
            V result = internalBuilder.get(k);

            if (result == null && defaultSupplier != null) {
                result = defaultSupplier.apply(k);
                if (result != null) {
                    put(k, result);
                }
            }

            return result;
        }

        public ImmutableMap<K, V> build() {
            return internalBuilder.build();
        }

        public <V2> ImmutableMap<K, V2> build(Function<V, V2> valueMappingFunction) {
            return internalBuilder.build(valueMappingFunction);
        }

        public int size() {
            return internalBuilder.size();
        }

        public Set<K> keySet() {
            return internalBuilder.keySet();
        }
    }

  
}
